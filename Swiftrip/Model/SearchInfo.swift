//
//  SearchInfo.swift
//  Swiftrip
//
//  Created by maki on 2017-04-13.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation

// budget
public let budget_max: Float = 1000
public let budget_min: Float = 100
public let budget_default: Float = 300

class SearchInfo: NSObject, NSCoding {

    var oneWay: Bool {
        willSet {
            var tf_returnDateIsEnabled: Bool!
            var icon: String!

            if !newValue { // Round trip
                if oneWay {
                    tf_returnDateIsEnabled = true
                    icon = "ic_airplanes"
                }

            } else { // One way
                if !oneWay {
                    tf_returnDateIsEnabled = false
                    icon = "ic_airplane"
                }
            }

            // fade-out
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn,
                           animations: {
                            ticketController.filterCell.btn_compare.alpha = 0

                            // changing return date if destination has been defined
                            if self.destination?.airportCode != nil {
                                if tf_returnDateIsEnabled {
                                    self.active(objc: ticketController.filterCell.lab_caption_return)
                                    self.active(objc: ticketController.filterCell.tf_returnDate)
                                } else {
                                    self.inactive(objc: ticketController.filterCell.lab_caption_return)
                                    self.inactive(objc: ticketController.filterCell.tf_returnDate)
                                }
                            }},

                           completion: { (_) in

                            // switch image
                            ticketController.filterCell.btn_compare.setImage(UIImage(named: icon), for: .normal)

                            // fade-in
                            UIView.animate(withDuration: 0.2, animations: {
                                ticketController.filterCell.btn_compare.alpha = 1
                            })
            })
        }
    }

    // these options are not used in InspirationSearchAPI, extentionSearchAPI
    lazy var options: [UIView] = [ticketController.filterCell.lab_caption_departure,
                                  ticketController.filterCell.lab_caption_return,
                                  ticketController.filterCell.tf_depatureDate,
                                  ticketController.filterCell.tf_returnDate,
                                  ticketController.filterCell.btn_stop_direct,
                                  ticketController.filterCell.btn_stop_1stop,
                                  ticketController.filterCell.btn_stop_2stop,
//                                  ticketController.filterCell.img_adult,
//                                  ticketController.filterCell.img_child,
//                                  ticketController.filterCell.img_infant,
//                                  ticketController.filterCell.lab_caption_passenger,
//                                  ticketController.filterCell.pic_passenger,
//                                  ticketController.filterCell.lab_caption_class,
//                                  ticketController.filterCell.pic_class
    ]

    var origin: Airport? {
        didSet {
            ticketController.filterCell.btn_originCode.setTitle(origin?.airportCode, for: [])
            ticketController.filterCell.lab_originCity.text = origin?.cityName
            ticketController.filterCell.validate()
        }
    }
    
    var destination: Airport? {
        didSet {
            ticketController.filterCell.btn_destinationCode.setTitle(destination?.airportCode, for: [])
            ticketController.filterCell.lab_destinationCity.text = destination?.cityName

            if destination != nil && oldValue == nil {

                self.options.forEach({
                    self.active(objc: $0)
                })

                if self.oneWay {
                    self.inactive(objc: ticketController.filterCell.tf_returnDate)
                    self.inactive(objc: ticketController.filterCell.lab_caption_return)
                }
                ticketController.filterCell.btn_stop_direct.isChecked = true
                ticketController.filterCell.btn_stop_1stop.isChecked = true
                ticketController.filterCell.btn_stop_2stop.isChecked = true

            } else if destination == nil && oldValue != nil { // for just in case

                self.options.forEach({
                    self.inactive(objc: $0)
                })
            }

            ticketController.filterCell.validate()
        }
    }

    var departDate: Date? {
        didSet {
            let tf: UITextField = ticketController.filterCell.tf_depatureDate

            // fadeInOut
            UIView.animate(withDuration: 0.1, animations: { tf.alpha = 0 }, completion: { (Bool) in
                tf.text = DateUtils.dateToString(date: self.departDate)
                UIView.animate(withDuration: 0.1, animations: {
                    tf.alpha = 1
                })
            })
        }
    }

    var returnDate: Date? {
        didSet {
            let tf: UITextField = ticketController.filterCell.tf_returnDate

            if let _departDate = departDate {
                if returnDate?.compare(_departDate) == .orderedAscending {
                    ticketController.filterCell.searchButtonIsActive = false
                } else {
                    ticketController.filterCell.searchButtonIsActive = true
                }
            }

            // fade out
            UIView.animate(withDuration: 0.1, animations: {
                tf.alpha = 0

            }, completion: { (Bool) in

                // fadeIn (if it's not nil)
                if let _returnDate: Date = self.returnDate {
                    tf.text = DateUtils.dateToString(date: _returnDate)
                } else {
                    tf.text = nil
                }

                if !searchInfo.oneWay {
                    UIView.animate(withDuration: 0.1, animations: {
                        tf.alpha = 1
                    })
                }
            })
        }
    }

    var stop: Int

    func active(objc: UIView) {
        objc.alpha = 1
        objc.isUserInteractionEnabled = true
    }

    func inactive(objc: UIView) {
        objc.alpha = 0
        objc.isUserInteractionEnabled = false
    }

    var lang: String
    var adult: Int?
    var child: Int?
    var infant: Int?
    var cabin_class: Int?
    var minTripDuration: NSInteger?
    var maxTripDuration: NSInteger?
    var budget: NSNumber? {
        didSet {
            if let _budget: NSNumber = budget {

                if _budget.floatValue == budget_max {
                    budget = nil
                }

                ticketController.filterCell.lab_budget.text = _budget.stringValue
                ticketController.filterCell.slider_budget.setValue(_budget.floatValue, animated: false)
            }
        }
    }
    var currency: Currency
    var location: UserLocation?

    init(origin: Airport? = nil, destination: Airport? = nil,
         departDate: Date? = nil, returnDate: Date? = nil,
         stop: Int = 2, oneWay: Bool = false, lang: String = "en",
         adult: Int? = 1, child: Int? = nil, infant: Int? = nil, cabin_class: Int? = nil,
         minTripDuration: NSInteger? = nil, maxTripDuration: NSInteger? = nil,
         budget:NSNumber? = budget_default as NSNumber, currency: Currency = Currency.USD, location: UserLocation? = nil) {
        
        self.origin = origin
        self.destination = destination
        self.departDate = departDate
        self.returnDate = returnDate
        self.stop = stop
        self.oneWay = oneWay
        self.lang = lang
        self.adult = adult
        self.child = child
        self.infant = infant
        self.cabin_class = cabin_class
        self.minTripDuration = minTripDuration
        self.maxTripDuration = maxTripDuration
        self.budget = budget
        self.currency = currency
        self.location = location
    }
    
    required init(coder decoder: NSCoder) {
        self.origin = decoder.decodeObject(forKey: "searchInfo_origin") as? Airport
        self.destination = decoder.decodeObject(forKey: "searchInfo_destination") as? Airport
        self.departDate = decoder.decodeObject(forKey: "searchInfo_departDate") as? Date
        self.returnDate = decoder.decodeObject(forKey: "searchInfo_returnDate") as? Date
        self.stop = decoder.decodeInteger(forKey: "searchInfo_stop")
        self.oneWay = decoder.decodeBool(forKey: "searchInfo_oneWay")
        self.lang = decoder.decodeObject(forKey: "searchInfo_lang") as! String
        self.adult = decoder.decodeObject(forKey: "searchInfo_adult") as? Int
        self.child = decoder.decodeObject(forKey: "searchInfo_child") as? Int
        self.infant = decoder.decodeObject(forKey: "searchInfo_infant") as? Int
        self.cabin_class = decoder.decodeObject(forKey: "searchInfo_trip_class") as? Int
        self.minTripDuration = decoder.decodeObject(forKey: "searchInfo_minTripDuration") as? NSInteger
        self.maxTripDuration = decoder.decodeObject(forKey: "searchInfo_maxTripDuration") as? NSInteger
        self.budget = decoder.decodeObject(forKey: "searchInfo_budget") as? NSNumber
        self.currency = convertCurrency(from: decoder.decodeObject(forKey: "searchInfo_currency") as! String)
        if let data = UserDefaults.standard.data(forKey: "searchInfo_searchInfo_location"),
           let _location: UserLocation = NSKeyedUnarchiver.unarchiveObject(with: data) as? UserLocation {
            self.location = _location
        }
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(origin, forKey: "searchInfo_origin")
        coder.encode(destination, forKey: "searchInfo_destination")
        coder.encode(departDate, forKey: "searchInfo_departDate")
        coder.encode(returnDate, forKey: "searchInfo_returnDate")
        coder.encode(stop, forKey: "searchInfo_stop")
        coder.encode(oneWay, forKey: "searchInfo_oneWay")
        coder.encode(lang, forKey: "searchInfo_lang")
        coder.encode(adult, forKey: "searchInfo_adult")
        coder.encode(child, forKey: "searchInfo_child")
        coder.encode(infant, forKey: "searchInfo_infant")
        coder.encode(cabin_class, forKey: "searchInfo_trip_class")
        coder.encode(minTripDuration, forKey: "searchInfo_minTripDuration")
        coder.encode(maxTripDuration, forKey: "searchInfo_maxTripDuration")
        coder.encode(budget, forKey: "searchInfo_budget")
        coder.encode(currency.rawValue, forKey: "searchInfo_currency")
        if let _location = location {
           let data = NSKeyedArchiver.archivedData(withRootObject: _location)
            UserDefaults.standard.set(data, forKey: "searchInfo_location")
        }
    }

    func save() {
        let searchInfo_data: Data = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(searchInfo_data, forKey: userDefaultKey_searchInfo)
        UserDefaults.standard.synchronize()
    }
}
