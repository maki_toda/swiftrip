//
//  Route.swift
//  Practice3
//
//  Created by maki on 2017-04-14.
//  Copyright © 2017 sherry. All rights reserved.
//

import Foundation

class Route {
    
    let airline_iata: String?
    let airline_icao: String?
    let departure_airport_iata: String?
    let departure_airport_icao: String?
    let arrival_airport_iata: String?
    let arrival_airport_icao: String?
    let codeshare: Bool
    let transfers: NSInteger
    let planes: [String]
    
    init(airline_iata: String?,
         airline_icao: String?,
         departure_airport_iata: String?,
         departure_airport_icao: String?,
         arrival_airport_iata: String?,
         arrival_airport_icao: String?,
         codeshare: Bool,
         transfers: NSInteger,
         planes: [String]) {
        
        self.airline_iata = airline_iata
        self.airline_icao = airline_icao
        self.departure_airport_iata = departure_airport_iata
        self.departure_airport_icao = departure_airport_icao
        self.arrival_airport_iata = arrival_airport_iata
        self.arrival_airport_icao = arrival_airport_icao
        self.codeshare = codeshare
        self.transfers = transfers
        self.planes = planes
    }
    
//    {"airline_iata":"2B","airline_icao":null,"departure_airport_iata":"AER","departure_airport_icao":null,"arrival_airport_iata":"DME","arrival_airport_icao":null,"codeshare":false,"transfers":0,"planes":["CR2"]},
    
    // IATA of airline.
    // ICAO of airline.
    // IATA of airport of departure.
    // ICAO of airport of departure.
    // IATA of airport of arrival.
    // ICAO of airport of arrival.
    // it shows whether the flight performs the same company that sells the ticket.
    // the number of direct.
    // IATA of airplane.
}
