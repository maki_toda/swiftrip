//
//  UserLocation.swift
//  Swiftrip
//
//  Created by maki on 2017-04-11.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation
import CoreLocation

class UserLocation: NSObject, NSCoding {
    var airportCode: String  // "YVR"
    var airportName: String? // "Vancouver International Airport"
    var cityName: String     // "Vancouver"
    var countryName: String  // "Canada"
    var location: CLLocation // "-123.114:49.2612"
    
    init(airportCode: String, airportName: String?, cityName: String, countryName: String, location: String) {
        self.airportCode = airportCode
        self.airportName = airportName
        self.cityName    = cityName
        self.countryName = countryName
        self.location = { () -> CLLocation in
            let array = location.components(separatedBy: ":")
            let location = CLLocation(latitude: atof(array[0]), longitude: atof(array[1]))
            return location
        }()
    }
    
    required init(coder decoder: NSCoder) {
        self.airportCode = decoder.decodeObject(forKey: "airportCode") as! String
        self.airportName = decoder.decodeObject(forKey: "airportName") as? String
        self.cityName = decoder.decodeObject(forKey: "cityName") as! String
        self.countryName = decoder.decodeObject(forKey: "countryName") as! String
        self.location = decoder.decodeObject(forKey: "location") as! CLLocation
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(airportCode, forKey: "airportCode")
        coder.encode(airportName, forKey: "airportName")
        coder.encode(cityName, forKey: "cityName")
        coder.encode(countryName, forKey: "countryName")
        coder.encode(location, forKey: "location")
    }
}
