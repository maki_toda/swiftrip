//
//  Airport.swift
//  Swiftrip
//
//  Created by maki on 2017-04-13.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation
import CoreLocation
import SwiftyJSON

class Airport: NSObject, NSCoding {
    var airportCode: String
    var airportName: String?
    var location: CLLocation?
    var placeName: String?
    var countryCode: String?
    var countryName: String?    // Canada
    var cityCode: String?
    var cityName: String?       // Vancouver
    var timeZone: String?
    var terminal: String?   // 1
    var photoURL: URL?
    
    init(airportCode: String,
         airportName: String? = nil,
         lat: Double? = nil,
         lon: Double? = nil,
         placeName: String? = nil,
         countryCode: String? = nil,
         cityCode: String? = nil,
         timeZone: String? = nil,
         terminal: String? = nil,
         photoURL: URL? = nil) {

        if let _placeName = placeName {
            let (cityName, countryName) = Airport.separateCityAndCountry(cityAndCountry: _placeName) // ex. "Prince George"
            self.placeName   = _placeName
            self.cityName    = cityName
            self.countryName = countryName
        }

        if let _lat : Double = lat, let _lon: Double = lon {
            self.location = CLLocation.init(latitude : CLLocationDegrees.init(_lat), longitude: CLLocationDegrees.init(_lon))
        }

        self.airportCode = airportCode
        self.airportName = airportName
        self.countryCode = countryCode
        self.cityCode    = cityCode
        self.timeZone    = timeZone
        self.terminal    = terminal
        self.photoURL    = photoURL
    }
    
    init(airportCode: String, terminal: String) {

        let airport: Airport? = Airport.getAirportFromUserDefaults(airportCode: airportCode)

        self.airportCode = airportCode
        self.airportName = airport?.airportName
        self.location    = airport?.location
        self.placeName   = airport?.placeName
        self.countryCode = airport?.countryCode
        self.countryName = airport?.countryName
        self.cityCode    = airport?.cityCode
        self.cityName    = airport?.cityName
        self.timeZone    = airport?.timeZone
        self.terminal    = terminal
        self.photoURL    = airport?.photoURL
    }

    init(airportCode: String, airportName: String, cityName: String, countryName: String) {
        self.airportCode = airportCode
        self.airportName = airportName
        self.cityName    = cityName
        self.countryName = countryName

        let airport: Airport? = Airport.getAirportFromUserDefaults(airportCode: airportCode)

        self.location    = airport?.location
        self.placeName   = airport?.placeName
        self.countryCode = airport?.countryCode
        self.cityCode    = airport?.cityCode
        self.timeZone    = airport?.timeZone
        self.terminal    = nil
        self.photoURL    = airport?.photoURL
    }

    required init(coder decoder: NSCoder) {
        self.airportCode = decoder.decodeObject(forKey: "airportCode") as! String
        self.airportName = decoder.decodeObject(forKey: "airportName") as? String
        self.location    = decoder.decodeObject(forKey: "location") as? CLLocation
        self.placeName   = decoder.decodeObject(forKey: "placeName") as? String
        self.countryCode = decoder.decodeObject(forKey: "countryCode") as? String
        self.countryName = decoder.decodeObject(forKey: "countryName") as? String
        self.cityCode    = decoder.decodeObject(forKey: "cityCode") as? String
        self.cityName    = decoder.decodeObject(forKey: "cityName") as? String
        self.timeZone    = decoder.decodeObject(forKey: "timeZone") as? String
        self.terminal    = decoder.decodeObject(forKey: "terminal") as? String
        self.photoURL    = decoder.decodeObject(forKey: "photoURL") as? URL
    }

    func encode(with coder: NSCoder) {
        coder.encode(airportCode, forKey: "airportCode")
        coder.encode(airportName, forKey: "airportName")
        coder.encode(location,    forKey: "location")
        coder.encode(placeName,   forKey: "placeName")
        coder.encode(countryCode, forKey: "countryCode")
        coder.encode(countryName, forKey: "countryName")
        coder.encode(cityCode,    forKey: "cityCode")
        coder.encode(cityName,    forKey: "cityName")
        coder.encode(timeZone,    forKey: "timeZone")
        coder.encode(terminal,    forKey: "terminal")
        coder.encode(photoURL,    forKey: "photoURL")
    }

    static func separateCityAndCountry(cityAndCountry: String) -> (String, String?) {
        // ex. cityAndCountry "Prince George, BC, Canada"
        let comma: String = ","
        if cityAndCountry.contains(comma) {
            let array: [String] = cityAndCountry.components(separatedBy: comma)
            let city: String = array[0]
            let country: String? = array.last
            return (city, country) // ex. "Prince George", "Canada"
        }
        return (cityAndCountry, nil)
    }

    static func fetchAirportAPIFromAirportCode(vc: UIViewController, airportCode: String, completion: ((_ airport: Airport) -> Void)?) {
        
        fetchAPI(vc: vc, executeApi: citiesAndAirportsAPI(keyword: airportCode), completion: { json_citiesAndAirports -> Void in
            
            for_jsonArray: for json in json_citiesAndAirports.array! {
                
                if let airportName = json["airport_name"].string {
                    
                    if airportCode == json["iata"].string || airportCode == json["city_iata"].string {

                        let airport: Airport = Airport(airportCode: json["iata"].stringValue,
                                                       airportName: airportName,
                                                       lat: json["location"]["lat"].double,
                                                       lon: json["location"]["lon"].double,
                                                       placeName: json["name"].string,
                                                       countryCode: json["country_iata"].string,
                                                       cityCode: json["city_iata"].string,
                                                       timeZone: nil,
                                                       terminal: nil,
                                                       photoURL: nil)

                        completion!(airport)
                        break for_jsonArray
                    }
                }
            }
        })
    }
    
    /*
     Get a photo of each city from Flickr
     1. Search photo on Flickr by city name.
     2. Set URL of the image
     */
    static func photoOfCity(vc: UIViewController, cityName: String, completion: ((_ photoURL: URL) -> Void)?) {
        
        // Request to Flickr
        fetchAPI(vc: vc, executeApi: flickrAPI(keyword: cityName), completion: { (json: JSON) -> Void in
            
            if let jsonPhoto: [String : JSON] = json["photos"]["photo"][0].dictionary {
                
                // Generate URL
                let photoURL: String = "https://farm\(jsonPhoto["farm"]!.int!).staticflickr.com/\(jsonPhoto["server"]!.string!)/\(jsonPhoto["id"]!.string!)_\(jsonPhoto["secret"]!.string!)_m.jpg"

                completion!(URL(string: photoURL)!)
            }
        })
    }
    
    static func getAirportFromUserDefaults(airportCode: String) -> Airport? {

        guard let _airports = airports else { return nil }

        for airport: Airport in _airports {
            if airport.airportCode == airportCode {
                return airport
            }
        }
        return nil
    }
}
