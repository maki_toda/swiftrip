//
//  RecordButton.swift
//  Swiftrip
//
//  Created by maki on 2017-06-28.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation

class RecordButton: UIButton {

    enum Mode {
        case recording
        case stop
        case send
        case enable
    }

    var mode: Mode = .stop {

        didSet {
            switch mode {
            case .recording:
                self.isEnabled = true
                UIView.animate(withDuration: 0.2, animations: {
                    self.alpha = 1.0
                    self.setImage(self.img_microphone, for: [])
                    self.backgroundColor = .link
                    self.tintColor = .white
                })

                self.animationView.alpha = 1
                self.animationView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                UIView.animate(withDuration: 0.8, delay: 0, options: [.repeat], animations: {
                    self.animationView.transform = CGAffineTransform.identity.scaledBy(x: 1.6, y: 1.6)
                    self.animationView.alpha = 0
                }, completion: nil)

            case .stop:
                self.isEnabled = true
                UIView.animate(withDuration: 0.2, animations: {
                    self.setImage(self.img_microphone, for: [])
                    self.backgroundColor = .white
                    self.tintColor = .link
                    self.animationView.alpha = 0
                    self.animationView.layer.removeAllAnimations()
                })

            case .send:
                UIView.animate(withDuration: 0.2, animations: {
                    self.setImage(self.img_done, for: [])
                    self.backgroundColor = .white
                    self.tintColor = .link
                    self.animationView.alpha = 0
                    self.animationView.layer.removeAllAnimations()
                })

            case .enable:
                self.isEnabled = false
                UIView.animate(withDuration: 0.2, animations: {
                    self.alpha = 0.2
                    self.setImage(self.img_microphone, for: [])
                    self.backgroundColor = .white
                    self.tintColor = .gray
                    self.animationView.alpha = 0
                    self.animationView.layer.removeAllAnimations()
                })
            }
        }
    }

    let img_microphone: UIImage = UIImage(named: "ic_mic")!.withRenderingMode(.alwaysTemplate)

    let img_done: UIImage = UIImage(named: "ic_done")!.withRenderingMode(.alwaysTemplate)

    let animationView: UIView = {
        let o: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            o.backgroundColor = .link
            o.layer.masksToBounds = false
            o.isUserInteractionEnabled = false
            o.alpha = 0
        return o
    }()

    convenience init() {
        let size: CGFloat = 80

        self.init(frame: CGRect(x: 0, y: 0, width: size, height: size))
        self.imageView?.contentMode = .scaleAspectFit
        self.layer.cornerRadius = size / 2
        self.layer.shadowRadius = 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 0.2
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        self.alpha = 0.2
        self.setImage(self.img_microphone, for: [])
        self.backgroundColor = .white
        self.mode = .enable

        // animationView
        self.addSubview(animationView)
        self.animationView.layout(centerX: self.centerXAnchor, centerY: self.centerYAnchor, widthConstant: size, heightConstant: size)
        self.animationView.layer.cornerRadius = size / 2
        self.sendSubview(toBack: self.animationView)
    }
}
