//
//  Extension.swift
//  Swiftrip
//
//  Created by maki on 2017-05-20.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class TicketView: UIView {
    
    class func button(text: String, size: CGFloat) -> UIButton {
        let o: UIButton = UIButton(type: .custom)
            o.setTitle(text, for: .normal)
            o.titleLabel?.font = UIFont.systemFont(ofSize: size)
            o.titleLabel?.textAlignment = .center
            o.setTitleColor(.primary, for: .normal)
        return o
    }
    
    class func booking() -> UIButton {
        let o: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            o.setTitle("Booking", for: .normal)
            o.titleLabel?.font = UIFont.systemFont(ofSize: 16)
            o.titleLabel?.textAlignment = .center
            o.setTitleColor(.primary, for: .normal)
            let backImage = UIImage(named: "ic_keyboard_arrow_right")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            o.setImage(backImage, for: .normal)
            o.imageView?.tintColor = .primary
            o.imageView?.contentMode = .scaleAspectFit
            o.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            o.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            o.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        return o
    }
    
    class func share() -> UIButton {
        let o: UIButton = UIButton(type: UIButtonType.custom)
            o.setImage(UIImage(named: "ic_share")!, for: .normal)
            o.alpha = 0.2
        return o
    }
    
    class func flightIcon(imgName: String) -> UIImageView {
        let o: UIImageView = UIImageView(image: UIImage(named: imgName))
            o.frame = CGRect(x: 0, y: 0, width: 16, height: 16)
            o.alpha = 0.8
            o.contentMode = .scaleAspectFit
            o.clipsToBounds = true
        return o
    }
    
    class func remainingSeatIcon() -> UIImageView {
        let o: UIImageView = flightIcon(imgName: "ic_seat")
            o.alpha = 0.2
            o.contentMode = .scaleAspectFit
            o.clipsToBounds = true
        return o
    }
    
    class func airlineLogo() -> UIImageView {
        let o = UIImageView()
            o.contentMode = .scaleAspectFit
            o.clipsToBounds = true
        return o
    }
    
    class func circle() -> UIView {
        let oval: UIBezierPath = UIBezierPath(ovalIn: CGRect(x: -4, y: -4, width: 8, height: 8))
        let shapeLayer: CAShapeLayer = CAShapeLayer()
            shapeLayer.path = oval.cgPath
            shapeLayer.fillColor     = UIColor.white.cgColor
            shapeLayer.strokeColor   = UIColor.primary.cgColor
            shapeLayer.lineWidth     = 1.0
        let o: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 8))
            o.layer.addSublayer(shapeLayer)
        return o
    }
    
    class func line() -> UIImageView {
        let o: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            o.backgroundColor = .primary
            o.alpha = 0.2
        return o
    }
    
    class func separater() -> UIImageView {
        let o: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            o.backgroundColor = .superLightGray
        return o
    }
}
