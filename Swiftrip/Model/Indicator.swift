//
//  Indicator.swift
//  Swiftrip
//
//  Created by maki on 2017-06-17.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation

class Indicator: UIView {

    let spinner: UIActivityIndicatorView = {
        let o: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            o.hidesWhenStopped = true
        return o
    }()

    var hasBeenShown: Bool = false {
        willSet {
            if !hasBeenShown && newValue {
                show()

            } else if hasBeenShown && !newValue {
                hide()
            }
        }
    }

    convenience init() {
        let size: CGFloat = 60
        self.init(frame: CGRect(x: 0, y: 0, width: size, height: size))
        self.backgroundColor = UIColor.white
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 4
        self.layer.cornerRadius = size / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        self.isHidden = true

        self.addSubview(self.spinner)
        self.spinner.layout(centerX: self.centerXAnchor, centerY: self.centerYAnchor)
    }

    func show() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1

        }, completion: { (Bool) in
            self.isHidden = false
            self.spinner.startAnimating()
        })
    }

    func hide() {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0

        }, completion: { (Bool) in
            self.isHidden = true
            self.spinner.stopAnimating()
        })
    }
}
