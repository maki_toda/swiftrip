//
//  API.swift
//  Swiftrip
//
//  Created by maki on 2017-04-09.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation
import Alamofire

class API: NSObject {
    let url: URL
    let method: HTTPMethod
    let parameters: Parameters
    let token: String?
    
    init(_ url: String, method: HTTPMethod = .get, parameters: Parameters = [ : ], token: String? = nil) {
        self.url        = URL(string: url)!
        self.method     = method
        self.parameters = parameters
        self.token      = token
    }
}

enum API_NAME {
    case inspirationSearch
    case extensiveSearch
    case lowFareSearch
    case affiliateSearch
}
