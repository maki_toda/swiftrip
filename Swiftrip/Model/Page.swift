//
//  Page.swift
//  Swiftrip
//
//  Created by maki on 2017-06-21.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation

struct Page {
    let title: String
    let message: String
}
