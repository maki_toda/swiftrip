//
//  Message.swift
//  Swiftrip
//
//  Created by maki on 2017-05-30.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import SwiftGifOrigin  // GIF anime

class Message {
    var text: NSString
    var sender: Sender
    
    init(text: NSString, sender: Sender) {
        self.text = text
        self.sender = sender
    }
}

class Sender {

    enum SenderType {
        case user
        case AI
    }

    var senderType: SenderType
    var name: String?
    var profileImage: UIImage?

    init(sender: SenderType, name: String? = nil, profileImage: UIImage? = nil) {
        switch sender {
        case .user:
            self.senderType = .user
            self.name = name
            self.profileImage = profileImage
        case .AI:
            self.senderType = .AI
            self.name = "AI"
            self.profileImage = UIImage.gif(name: "icon_AI")! // GIF anime
        }
    }
}
