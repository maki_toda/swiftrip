//
//  Ticket.swift
//  Swiftrip
//
//  Created by maki on 2017-04-10.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation

// Hierarchy of classes -----------------------
//
//   Ticket
//   └ Itenerary
//      └ Bound
//         ├ Flight
//         │ └ Fare
//         │    └ Price
//         └ BookingInfo
//
// --------------------------------------------

class Ticket {
    let apiName: API_NAME
    var itineraries: [Itinerary]
    let deep_link: String?               // "https:\/\/track.connect.travelaudience.com\/dlv\/verify\/?params=.eJwdx8FugyAAgOF3IfFWi0jcTG9taZcswrJOse6yWIWpWFgUg3Xpu2_p5c_3_4Jp6MEGNNb-jB7eQuicW1etvXXCritzhRdjFEz2BSTZDiK_rKfejjDwq6bt60Hof7ZaltrCMEDPfhD7YQSNFr4rb9DDR1vqUfQeJrx7jTn6ninJRq6OlGc8ZgudaFfHJ84_JKmWolPRW8qbJH-fWaosezm1bI-aYtmpJO0bln5e6VK4Ij_M9Dyf860XRpg8AlaglPKrrcEGBehpBYyUYng8wAG4_wGH3Etn%3A1d0GKN%3AA9p_tSxuc1hEYdVDELodSODHWjk"
    let merchant: String?                // "WX"
    var travel_class: String?            // "ECONOMY"
    let fare_family: String?             // "VALUE"
    let cabin_code: String?              // "Y"
    let fare: Fare?
    let airline: String?                 // "WX"
    var img: URL?
    
    init(apiName: API_NAME,
         itineraries: [Itinerary],
         deep_link: String?,
         merchant: String?,
         travel_class: String?,
         fare_family: String?,
         cabin_code: String?,
         fare: Fare?,
         airline: String?,
         img: URL?
        ) {
        self.apiName = apiName
        self.itineraries = itineraries
        self.deep_link = deep_link
        self.merchant = merchant
        self.travel_class = travel_class
        self.fare_family = fare_family
        self.cabin_code = cabin_code
        self.fare = fare
        self.airline = airline
        self.img = img
    }
}

class Itinerary {
    let outbound: Bound
    let inbound: Bound?
    
    init(outbound: Bound, inbound: Bound?) {
        self.outbound = outbound
        self.inbound = inbound
    }
}

class Bound {
    let flights: [Flight]
    let duration: String?
    
    init(flights: [Flight], duration: String?) {
        self.flights = flights
        self.duration = duration
    }
}

class Flight {
    let origin: Airport                 // "DUB"
    let destination: Airport            // "DUB"
    let departs_at: Date?               // "2017-08-25T08:45"
    let arrives_at: Date?               // "2017-08-25T10:15"
    let booking_info: BookingInfo?
    let operating_airline: String?      // “WX”
    let marketing_airline: String?      // “WX”
    let aircraft: String?               // "AR8"
    let flight_number: String?          // "105"
    
    init(origin: Airport,
         destination: Airport,
         departs_at: Date?,
         arrives_at: Date?,
         booking_info: BookingInfo?,
         operating_airline: String?,
         marketing_airline: String?,
         aircraft: String?,
         flight_number: String?) {
        self.origin = origin
        self.destination = destination
        self.departs_at = departs_at
        self.arrives_at = arrives_at
        self.booking_info = booking_info
        self.operating_airline = operating_airline
        self.marketing_airline = marketing_airline
        self.aircraft = aircraft
        self.flight_number = flight_number
    }
}

class Fare: NSObject {
    let price_per_adult: Price?
    let price_per_child: Price?
    let price_per_infant: Price?
    let total_price: NSNumber?
    let service_fees: NSNumber?
    let creditcard_fees: NSNumber?
    
    init(price_per_adult: Price?,
         price_per_child: Price?,
         price_per_infant: Price?,
         total_price: NSNumber?,
         service_fees: NSNumber?,
         creditcard_fees: NSNumber?) {
        self.price_per_adult = price_per_adult
        self.price_per_child = price_per_child
        self.price_per_infant = price_per_infant
        self.total_price = total_price
        self.service_fees = service_fees
        self.creditcard_fees = creditcard_fees
    }
}

class Price: NSObject {
    let tax: NSNumber?
    let total_price: NSNumber?
    
    override init() {
        self.tax = nil
        self.total_price = nil
    }
    
    init(tax: NSNumber?, total_price: NSNumber?) {
        self.tax = tax
        self.total_price = total_price
    }
}

class BookingInfo: NSObject {
    let seats_remaining: NSInteger?      // 9
    let fare_basis: String?              // "R9PRWGB"
    let booking_code: String?            // "R"
    let cabin_code: String?              // "M"
    let travel_class: String?            // "ECONOMY"
    let fare_family: String?             // "VALUE"
    
    init(seats_remaining: NSInteger?,
         fare_basis: String?,
         booking_code: String?,
         cabin_code: String?,
         travel_class: String?,
         fare_family: String?) {
        self.seats_remaining = seats_remaining
        self.fare_basis = fare_basis
        self.booking_code = booking_code
        self.cabin_code = cabin_code
        self.travel_class = travel_class
        self.fare_family = fare_family
    }
}
