//
//  ResultCell.swift
//  Swiftrip
//
//  Created by maki on 2017-05-23.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import AlamofireImage
import SwiftyJSON

class ResultCell: BaseTableViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    static let sectionHeaderID: String  = "sectionHeaderID"
    static let sectionFooterID: String  = "sectionFooterID"

    var tickets: [Ticket] = []
    var cellShown: [Bool] = []
    var collectionView: UICollectionView!
    var revealvc: SWRevealViewController!
    let noResultTextView: UITextView = {
        let o: UITextView = UITextView(fontSize: 16, text: "No matched ticket")
            o.textColor = .lightGray
            o.textAlignment = .center
            o.backgroundColor = .clear
            o.isUserInteractionEnabled = false
        return o
    }()
    
    override func setupView() {
        super.setupView()
        self.backgroundColor = .superLightGray
        self.setupCollectionView()
    }
    
    func setupCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
            layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 16)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing      = 0
        
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(collectionViewClicked(_:)))
        
        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 1, height: 1), collectionViewLayout: layout)
        self.collectionView.delegate   = self
        self.collectionView.dataSource = self
        self.collectionView.register(TicketCell.self, forCellWithReuseIdentifier: TicketCell.cellId)
        self.collectionView.register(TicketCellHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: ResultCell.sectionHeaderID)
        self.collectionView.register(TicketCellHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: ResultCell.sectionFooterID)
        self.collectionView.backgroundColor = .superLightGray
        self.collectionView.addGestureRecognizer(gesture)
        self.contentView.addSubview(collectionView)
        self.collectionView.layout(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor)

        self.addSubview(self.noResultTextView)
        self.noResultTextView.layout(centerX: self.centerXAnchor, centerY: self.centerYAnchor,
                                     widthConstant: DeviceSize.screenWidth(),
                                     heightConstant: estimateFrame(self.noResultTextView.text, fontSize: 16,
                                                                   size: CGSize(width: DeviceSize.screenWidth(), height: 0)).height)
    }

    
    // MARK: - collectionView delegate -----------------------------------------------------
    
    // section
    func numberOfSections(in collectionView: UICollectionView) -> Int {

        if self.tickets.count == 0 { // No result
            return 0
        }

        if searchInfo.destination == nil { // inspirationSearchAPI
            return self.tickets.count
        }

        // lowFareSearchAPI and affiliateSearchAPI
        return 1
    }
    
    // item
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.tickets.count <= 0 {
            self.noResultTextView.isHidden = false
            return 0
        } else if searchInfo.destination == nil {
            self.noResultTextView.isHidden = true
            return 1
        } else {
            self.noResultTextView.isHidden = true
            return tickets.count
        }
    }
    
    // cell
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var target: Int
        var withFlightDetail: Bool
        var hideRoundCorner: Bool
        
        switch tickets[indexPath.section].apiName {
        case API_NAME.inspirationSearch:
            target = indexPath.section
            withFlightDetail = false
            hideRoundCorner = false
            
        case API_NAME.extensiveSearch:
            target = indexPath.row
            withFlightDetail = false
            hideRoundCorner = false
            
        case API_NAME.lowFareSearch,
             API_NAME.affiliateSearch:
            target = indexPath.row
            withFlightDetail = true
            hideRoundCorner = true
        }
        
        let cell: TicketCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: TicketCell.cellId, for: indexPath) as! TicketCell

            cell.setupCell(ticket: self.tickets[target], withFlightDetail: withFlightDetail)
            
            // z-position
            cell.layer.zPosition = CGFloat(target)
            
            // set actions of buttons
            cell.btn_booking.tag = target
            cell.btn_booking.addTarget(self, action: #selector(self.bookingClicked(_:)), for: .touchUpInside)
            // cell.btn_share.addTarget(self, action: #selector(self.shareClicked(_:)), for: .touchUpInside)

        if hideRoundCorner && target != 0 {
            cell.baseLayoutBar.isHidden = true
            cell.separatorLine.isHidden = false
        }

        return cell
    }
    
    // Header & Footer cell
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        // Header
        if kind == UICollectionElementKindSectionHeader {
            let header: TicketCellHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ResultCell.sectionHeaderID, for: indexPath) as! TicketCellHeader
            
            if tickets.count > 0 {
                // place name
                let flights: [Flight] = tickets[indexPath.section].itineraries[0].outbound.flights
                header.lab_city.text = flights[flights.count - 1].destination.placeName

                // image
                if let url = tickets[indexPath.section].img {
                    header.imgView.af_setImage(withURL: url, imageTransition: .crossDissolve(0.2)) // using Alamofire image framework
                }
            }

            header.bringSubview(toFront: header.lab_city)
            header.layer.zPosition = CGFloat(-1)
            return header
            
        // Footer
        } else {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: ResultCell.sectionFooterID, for: indexPath)
            footer.backgroundColor = .white
            return footer
        }
    }
    
    // Header size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 180)
    }
    
    // Footer
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
    //        return CGSize(width: view.frame.width, height: 0)
    //    }

    // share button clicked
    @objc private func shareClicked(_ sender: AnyObject) {
        
        sender.setTitleColor(.primary, for: UIControlState.normal)
        
        let dialog: UIAlertController = UIAlertController(title: "Share this ticket", message: "Which SNS do you want to share?", preferredStyle:  .actionSheet)
        
        // TODO: Set SNS share link
        let action_google: UIAlertAction = UIAlertAction(title: "Google+", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) -> Void in
            print("Google+")
        })
        
        let action_twitter: UIAlertAction = UIAlertAction(title: "Twitter", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) -> Void in
            print("Twitter")
        })
        
        let action_facebook: UIAlertAction = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) -> Void in
            print("Facebook")
        })
        
        let action_cancel: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
            (action: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        dialog.addAction(action_facebook)
        dialog.addAction(action_google)
        dialog.addAction(action_twitter)
        dialog.addAction(action_cancel)
        dialog.show(vc: ticketController!, dismiss: false)
    }
    
    // booking button clicked
    @objc private func bookingClicked(_ sender: AnyObject) {
        guard let deeplink: String = tickets[sender.tag].deep_link else { return }
        guard let url: URL = URL(string: deeplink) else { return }
        UIApplication.shared.open(url)
    }
    
    // MARK: - when cell or link or collectionView was clicked, shrink filter pane ----------
    
    // cell selected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        ticketController.expandedCell = .Result
    }
    
    // collectionView clicked
    func collectionViewClicked(_ sender:UITapGestureRecognizer) {
        print("collectionView was clicked")

//        // if sidebar has been opened, close it
//        if self.revealvc.frontViewPosition != FrontViewPosition.left {
//            self.revealvc.revealToggle(animated: true)
//
//        } else {
            ticketController.expandedCell = .Result
//        }
    }

    /// 画面内の Supplementary View を取得
    fileprivate func viewedSupplementaryElements(for kinds: [String]) -> [UICollectionViewLayoutAttributes] {
        var viewRect: CGRect = CGRect()
            viewRect.origin = collectionView.contentOffset
            viewRect.size = UIScreen.main.bounds.size

        var result: [UICollectionViewLayoutAttributes] = [UICollectionViewLayoutAttributes]()

        if let layoutAttributes: [UICollectionViewLayoutAttributes] = collectionView.collectionViewLayout.layoutAttributesForElements(in: viewRect) {
            for attribute: UICollectionViewLayoutAttributes in layoutAttributes {
                for kind: String in kinds where attribute.representedElementKind == kind {
                    result.append(attribute)
                }
            }
        }

        return result
    }

    fileprivate func invalidateLayout() {
        // ここで UICollectionViewFlowLayoutInvalidationContext を利用して二つの対策を施す
        // 1. UICollectionViewFlowLayout internal error 対策
        // 2. Self Sizing Cells (UICollectionViewFlowLayoutAutomaticSize) を利用しているとき、最初の reloadData() でヘッダー／フッターの座標がずれてしまうので、レイアウトを失効させる。

        let viewedSupplementaries: [UICollectionViewLayoutAttributes] = viewedSupplementaryElements(for: [UICollectionElementKindSectionHeader, UICollectionElementKindSectionFooter])
        let context = UICollectionViewFlowLayoutInvalidationContext()

        for attribute: UICollectionViewLayoutAttributes in viewedSupplementaries {
            context.invalidateSupplementaryElements(ofKind: attribute.representedElementKind!, at: [attribute.indexPath])
        }

        self.collectionView.collectionViewLayout.invalidateLayout(with: context)
    }

    func reloadData() {
        // to avoid a bug that the header layout is lost its layout.
        self.collectionView.reloadData()
        self.invalidateLayout()
    }
}





// MARK: - fetch API ---------------------------------------------------------------------

extension ResultCell {
    
    func fetchTickets() {

        // show indicator
        ticketController.indicator.hasBeenShown = true
        
        // clear
        self.tickets.removeAll()
        // self.cellShown = []
        
        let hasInbound: Bool = {
            if searchInfo.oneWay {
                return false
            } else {
                return true
            }
        }()

        func fetchLowFareSearchAndAffiliateAPI() {
            
            let group: DispatchGroup = DispatchGroup()
            let queue_fetchLowFareSearchAPI   = DispatchQueue.global(qos: .default),
                queue_fetchAffiliateSearchAPI = DispatchQueue.global(qos: .default),
                queue_fetchPhotoAPI           = DispatchQueue.global(qos: .default)
            
            group.enter()
            queue_fetchLowFareSearchAPI.async(group: group) {
                print("get lowFareSearchAPI")
                if let api: API = lowFareSearchAPI() {
                    fetchAPI(vc: ticketController!, executeApi: api, completion: { json -> Void in
                        self.initTickets(json, API_NAME.lowFareSearch, hasInbound: hasInbound)
                        group.leave()
                    })

                } else { group.leave() }
            }

            group.enter()
            queue_fetchAffiliateSearchAPI.async(group: group) {
                print("get affiliateSearchAPI")
                if let api: API = affiliateSearchAPI() {
                    fetchAPI(vc: ticketController!, executeApi: api, completion: { json -> Void in
                        self.initTickets(json, API_NAME.affiliateSearch, hasInbound: hasInbound)
                        group.leave()
                    })
                } else { group.leave() }
            }

            group.enter()
            var destinationPhotoURL: URL?
            var destinationPlaceName: String?
            queue_fetchPhotoAPI.async(group: group) { // get photo of a destination (always only one destination)
                print("get photo of \(searchInfo.destination?.airportCode ?? "")")
                
                if let airport = Airport.getAirportFromUserDefaults(airportCode: searchInfo.destination!.airportCode) {
                    destinationPhotoURL  = airport.photoURL
                    destinationPlaceName = airport.placeName
                    group.leave()
                    
                } else {
                    
                    // Request airportInfo
                    Airport.fetchAirportAPIFromAirportCode(vc: ticketController!, airportCode: searchInfo.destination!.airportCode, completion: { airport -> Void in
                        
                        // search photoURL of city
                        Airport.photoOfCity(vc: ticketController!, cityName: airport.cityName!, completion: { photoURL in
                            
                            // save to UserDefaults (before saving, check UserDefaults again because there is time lag)
                            if Airport.getAirportFromUserDefaults(airportCode: airport.airportCode) == nil {
                                destinationPhotoURL = photoURL
                                destinationPlaceName = airport.placeName
                                airport.photoURL = photoURL
                                airports?.append(airport)
                                let encodedData = NSKeyedArchiver.archivedData(withRootObject: airports!)
                                UserDefaults.standard.set(encodedData, forKey: userDefaultKey_airports)
                            }
                            group.leave()
                        })
                    })
                }
            }
            
            group.notify(queue: DispatchQueue.main) {

                self.handleVoiceSearchView(ticketCount: self.tickets.count)

                print("total \(self.tickets.count) tickets downloaded")
                
                // show dialog
                self.showDialogOfNumberOfTickets(self.tickets.count)

                if self.tickets.count == 0 {

                    self.reloadData()

                } else {

                    ticketController.indicator.hide()

                    ticketController.expandedCell = .Result

                    // set a photo
                    if let _ : URL = destinationPhotoURL {
                        self.tickets[0].img = destinationPhotoURL
                        let flights = self.tickets[0].itineraries[0].outbound.flights
                        flights[flights.count - 1].destination.placeName = destinationPlaceName
                    }
                    
                    // sort
                    self.tickets.sort { $0.fare!.total_price!.floatValue < $1.fare!.total_price!.floatValue }

                    // reload
                    print("reloaded collectionView")
                    self.reloadData()
                    self.collectionView.contentOffset = .zero // scroll to top
                    
                    setAirportAdditionalInfo(hasInbound: hasInbound)
                }
            }
            
            func setAirportAdditionalInfo(hasInbound: Bool) {
                
                /*
                 show airport name, photo after showing cells
                 */
                
                let group: DispatchGroup = DispatchGroup()
                let queue1 = DispatchQueue.global(qos: .default)
                
                func setAirportNameToTicketAndSaveToUserDefault(airport: Airport, ticketAirport: Airport) {
                    
                    ticketAirport.airportName = airport.airportName
                    ticketAirport.placeName = airport.placeName
                    
                    // save to UserDefaults (before saving, check UserDefaults again because there is time lag)
                    if Airport.getAirportFromUserDefaults(airportCode: airport.airportCode) == nil {
                        airports?.append(airport)
                        let encodedData = NSKeyedArchiver.archivedData(withRootObject: airports!)
                        UserDefaults.standard.set(encodedData, forKey: userDefaultKey_airports)
                    }
                }
                
                // outbound
                for ticket in self.tickets {
                    
                    for itinerary in ticket.itineraries {
                        
                        for flight in itinerary.outbound.flights {
                            
                            // checking the airport name in UserDefaults
                            if let airport = Airport.getAirportFromUserDefaults(airportCode: flight.destination.airportCode) {
                                flight.destination.airportName = airport.airportName
                                print("UserDefaults: \(airport.airportName ?? "")")
                                
                            } else { // if it doesn't exist in UserDefaults, fetching an API of airportName
                                
                                Airport.fetchAirportAPIFromAirportCode(vc: ticketController!, airportCode: flight.destination.airportCode, completion: { airport -> Void in
                                    
                                    setAirportNameToTicketAndSaveToUserDefault(airport: airport, ticketAirport: flight.destination)
                                })
                            }
                        }
                        
                        if let inbound: Bound = itinerary.inbound {
                            
                            for flight in inbound.flights {
                                
                                // search from userDefaults
                                if let airport = Airport.getAirportFromUserDefaults(airportCode: flight.origin.airportCode) {
                                    flight.origin.airportName = airport.airportName
                                    flight.origin.placeName   = airport.placeName
                                    
                                // search from API
                                } else {
                                    Airport.fetchAirportAPIFromAirportCode(vc: ticketController!, airportCode: flight.origin.airportCode, completion: { airport -> Void in
                                        
                                        setAirportNameToTicketAndSaveToUserDefault(airport: airport, ticketAirport: flight.origin)
                                    })
                                }
                                
                                if let airport = Airport.getAirportFromUserDefaults(airportCode: flight.destination.airportCode) {
                                    flight.destination.airportName = airport.airportName
                                    flight.destination.placeName = airport.placeName
                                    
                                } else {
                                    Airport.fetchAirportAPIFromAirportCode(vc: ticketController!, airportCode: flight.destination.airportCode, completion: {airport -> Void in
                                        
                                        setAirportNameToTicketAndSaveToUserDefault(airport: airport, ticketAirport: flight.destination)
                                    })
                                }
                            }
                        }
                    }
                }
                self.reloadData()
            }
        }
        
        func fetchExtensiveSearchAPI() {
            
            fetchAPI(vc: ticketController!, executeApi: extensiveSearchAPI(), completion: { json -> Void in

                ticketController.indicator.hasBeenShown = false

                if json["results"].exists() {
                    self.initTickets(json, API_NAME.extensiveSearch, hasInbound: hasInbound)

                } else if json["status"].int == 400 {
                    self.reloadData()

                    // show alert
                    let title: String = {
                        if let _title: String = json["message"].string {
                            return _title
                        }
                        return "No result"
                    }()

                    let message: String = {
                        if let _message: String = json["more_info"].string {
                            return _message
                        }
                        return ""
                    }()
                    let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
                    alert.show(vc: ticketController!, dismiss: true)

                } else {
                    self.showDialogOfNumberOfTickets(0)
                    self.reloadData()
                }
            })
        }

        func fetchInspirationSearchAPI() {

            fetchAPI(vc: ticketController!, executeApi: inspirationSearchAPI(), completion: { json -> Void in
                
                ticketController.indicator.hasBeenShown = false

                if json["results"].exists() {
                    
                    self.initTickets(json, API_NAME.inspirationSearch, hasInbound: hasInbound)

                } else {
                    self.showDialogOfNumberOfTickets(0)
                    self.reloadData()
                }
            })
        }

        // fetch to "lowFareSearchAPI" & "affiliateSearchAPI" of Amadeus
        if searchInfo.destination != nil && searchInfo.departDate != nil {
            
            fetchLowFareSearchAndAffiliateAPI()
            
        // fetch to "extensiveSearchAPI" of Amadeus
        } else if searchInfo.destination != nil {
            
            fetchExtensiveSearchAPI()
            
        // fetch to "inspirationSearchAPI" of Amadeu
        } else {
            
            fetchInspirationSearchAPI()
            
            // Request to "CheapestSearchAPI" of TravelPayouts
            //                fetchAPI(executeApi: CheapestTicketsAPI(currency: searchInfo.currency, origin: searchInfo.origin!, destination: nil, depart_date: searchInfo.departDate, return_date: searchInfo.returnDate),
            //                         completion: { json -> Void in
            //                            self.initTickets(json, API_NAME.inspirationSearchTickets)
            //                })
        }
    }
    
    private func initTickets(_ json: JSON, _ apiName: API_NAME, hasInbound: Bool) {
        print(json)
        
        guard let results: [JSON] = json["results"].array else { return }
        
        if results.count == 0 {
            return
        }
        print("\(results.count) tickets of \(apiName)")

        switch apiName {
        case API_NAME.inspirationSearch,
             API_NAME.extensiveSearch:
            
            var arrDestinationCode: [String] = [String]()
            
            for result in results {
                
                // create an array of destinationCode to request Flickr photos after showing tickets on screen
                arrDestinationCode.append(result["destination"].string!)
                
                // set tickets
                let ticket: Ticket = initTicketForNoDestination(result, hasInbound: hasInbound)
                
                self.tickets.append(ticket)
            }
            
            // show airport name, place name, photo ===========================================
            
            func setAirportToTickets(index: Int, airport: Airport) {
                self.tickets[index].itineraries[0].outbound.flights[0].destination.airportName = airport.airportName
                self.tickets[index].itineraries[0].inbound?.flights[0].origin.airportName      = airport.airportName
                self.tickets[index].itineraries[0].outbound.flights[0].destination.placeName   = airport.placeName
                self.tickets[index].itineraries[0].inbound?.flights[0].origin.placeName        = airport.placeName
                self.tickets[index].itineraries[0].outbound.flights[0].destination.photoURL    = airport.photoURL
                self.tickets[index].itineraries[0].inbound?.flights[0].origin.photoURL         = airport.photoURL
                self.tickets[index].img                                                        = airport.photoURL
            }
            
            let group_firstShow : DispatchGroup = DispatchGroup()
            let group_secondShow: DispatchGroup = DispatchGroup()
            let queue1: DispatchQueue = DispatchQueue.global(qos: .default)
            let numberOfFirstShow: Int = 1 // number of the showing cell at first time
            for _ in 0..<numberOfFirstShow {
                group_firstShow.enter()
            }
            group_secondShow.enter()
            
            for (index, destinationCode) in arrDestinationCode.enumerated() {
                
                queue1.async(group: group_firstShow) {
                    
                    // check UserDefaults
                    if let matchedAirport: Airport = Airport.getAirportFromUserDefaults(airportCode: destinationCode) {
                        
                        setAirportToTickets(index: index, airport: matchedAirport)
                        print("load from local: \(matchedAirport.airportName ?? "")")
                        if index < numberOfFirstShow { group_firstShow.leave() }
                        if index == arrDestinationCode.count - 1 { group_secondShow.leave() }
                        
                    // request placeName, photoURL
                    } else {
                        Airport.fetchAirportAPIFromAirportCode(vc: ticketController!, airportCode: destinationCode, completion: { airport -> Void in
                            
                            getPhotoURL(airportOfUserDefaults: airport, ticketIndex: index, countTickets: arrDestinationCode.count)
                        })
                    }
                }
            }
            
            func getPhotoURL(airportOfUserDefaults: Airport, ticketIndex: Int, countTickets: Int) {
                
                // search photo of cityName
                guard let placeName = airportOfUserDefaults.placeName else { return }
                let (cityName, _)  = Airport.separateCityAndCountry(cityAndCountry: placeName)
                
                Airport.photoOfCity(vc: ticketController!, cityName: cityName, completion: { photoURL in
                    
                    // save to UserDefaults (before saving, check UserDefaults again because there is time lag)
                    if Airport.getAirportFromUserDefaults(airportCode: airportOfUserDefaults.airportCode)?.photoURL == nil {
                        print(photoURL)
                        
                        airportOfUserDefaults.photoURL = photoURL
                        airports?.append(airportOfUserDefaults)
                        
                        setAirportToTickets(index: ticketIndex, airport: airportOfUserDefaults)
                        
                        print("count: \(numberOfFirstShow)")
                        
                        if ticketIndex < numberOfFirstShow { group_firstShow.leave() }
                        if ticketIndex == countTickets - 1 { group_secondShow.leave() }
                    }
                })
            }
            
            group_firstShow.notify(queue: DispatchQueue.main) {
                // self.setAlert(count: self.tickets.count)
                print("ResultCell collectionView reload")

                ticketController.expandedCell = .Result

                self.handleVoiceSearchView(ticketCount: self.tickets.count)

                self.showDialogOfNumberOfTickets(self.tickets.count)
                
                self.reloadData()
            }
            
            group_secondShow.notify(queue: DispatchQueue.global()) {
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: airports!)
                UserDefaults.standard.set(encodedData, forKey: userDefaultKey_airports)
                UserDefaults.standard.synchronize()
            }
            
            
        case API_NAME.lowFareSearch,
             API_NAME.affiliateSearch:
            
            /*
             In case of lowFareSearchAPI and affiliateSearchAPI,
             the destination has been already decided, so get just one image from Flickr before excuting initTicket().
             But each flight has multiple airportNames. so use for loop.
             */
            
            for value in results {
                
                if apiName == API_NAME.affiliateSearch && searchInfo.stop == 0 {
                    if value["outbound"]["flights"].array?.count != 1 {
                        continue // skip multiple flights if user selected direct flight
                    }
                }
                
                let ticket: Ticket = self.initTicket(value: value, apiName: apiName, hasInbound: hasInbound)
                tickets.append(ticket)
                
                // TODO: cell shown
                // self.cellShown.append(false)
            }
        }
    }
    
    private func initTicket(value: JSON, apiName: API_NAME, hasInbound: Bool) -> Ticket {
        
        return Ticket(
            apiName: apiName,
            itineraries: initItineraries(value: value, hasInbound: hasInbound),
            deep_link: initDeepLink(value, apiName),
            merchant: value["merchant"].string,
            travel_class: value["travel_class"].string?.lowercased(),
            fare_family: value["fare_family"].string,
            cabin_code: value["cabin_code"].string,
            fare: Fare(price_per_adult : self.initPrice(value["fare"], "price_per_adult"),
                       price_per_child : self.initPrice(value["fare"], "price_per_child"),
                       price_per_infant: self.initPrice(value["fare"], "price_per_infant"),
                       total_price     : value["fare"]["total_price"].numberValue,
                       service_fees    : value["fare"]["service_fees"].numberValue,
                       creditcard_fees : value["fare"]["creditcard_fees"].numberValue),
            airline: value["airline"].string,
            img: nil)
    }
    
    private func initDeepLink(_ json: JSON, _ apiName: API_NAME) -> String {
        
        switch apiName {
        case API_NAME.affiliateSearch: // affiliateSearchAPI has deeplink
            return json["deep_link"].string!
            
        default: // extensiveSearch
            var param : [String] = ["marker=\(Setting.travelPayouts_marker)"]
            if let origin: String = searchInfo.origin?.airportCode {
                param.append("origin_iata=\(origin)")
            }
            if let destination: String = searchInfo.destination?.airportCode {
                param.append("destination_iata=\(destination)")
            }
            if let depart_date: Date = searchInfo.departDate { // ex. 2016-12-01
                param.append("depart_date=\(depart_date.convertToString(dateFormat: .YYYY_MM_DD))")
            }
            if let return_date: Date = searchInfo.returnDate { // ex. 2016-12-15
                param.append("return_date=\(return_date.convertToString(dateFormat: .YYYY_MM_DD))")
            }
            if searchInfo.oneWay {
                param.append("oneway=1")
            } else {
                param.append("oneway=0")
            }
            if let adults: Int = searchInfo.adult {
                param.append("adults=\(adults)")
            }
            if let children: Int = searchInfo.child {
                param.append("children=\(children)")
            }
            if let infants: Int = searchInfo.infant {
                param.append("infants=\(infants)")
            }
            if let cabin_class: Int = searchInfo.cabin_class {
                param.append("trip_class=\(cabin_class)") // Economy: 0, Business: 1, First: 2
            }
            // Initiate search automatically (true: the search is loaded, false: form is filled out but search does not start)
            param.append("with_request=false")
            let deeplink: String = "http://jetradar.com/searches/new?\(param.joined(separator: "&"))"
            return deeplink
        }
    }
    
    private func initPrice(_ json: JSON, _ passenger: String) -> Price? {
        if let tax  : NSNumber = json[passenger]["tax"].number,
           let total: NSNumber = json[passenger]["total_fare"].number {
            return Price(tax: tax, total_price: total)
        }
        return nil
    }
    
    private func initItineraries(value: JSON, hasInbound: Bool) -> [Itinerary] {
        var itineraries: [Itinerary] = []
        
        // MEMO: count of itineraries is usualy only 1
        
        func initItinerary(itinerary: JSON) -> Itinerary {
            
            var dic: [String: Bound] = [:]
            for bound in ["outbound", "inbound"] {
                
                let duration: String? = {
                    if let _duration: String = itinerary[bound]["duration"].string {
                        return _duration
                    } else { return nil }
                }()
                
                if let _bound: [JSON] = itinerary[bound]["flights"].array {
                    dic[bound] = Bound(flights: self.initFlights(_bound), duration: duration)
                    
                } else {
                    dic[bound] = nil
                }
            }
            return Itinerary(outbound: dic["outbound"]!, inbound: dic["inbound"])
        }
        
        // LowFareSesarchAPI
        if let _itineraries: [JSON] = value["itineraries"].array {
            for itinerary in _itineraries {
                itineraries.append(initItinerary(itinerary: itinerary))
            }
            
        // affiliateSearchAPI
        } else {
            itineraries.append(initItinerary(itinerary: value))
        }
        return itineraries
    }
    
    private func initFlights(_ json: [JSON]) -> [Flight] {
        
        var flights: [Flight] = [Flight]()
        for flightValue: JSON in json {

            let originAirport     : Airport = Airport(airportCode: flightValue["origin"]["airport"].stringValue,
                                                      terminal:    flightValue["origin"]["terminal"].stringValue)
            let destinationAirport: Airport = Airport(airportCode: flightValue["destination"]["airport"].stringValue,
                                                      terminal:    flightValue["destination"]["terminal"].stringValue)
            let departDate: Date = DateUtils.dateFromStringWithTime(string: flightValue["departs_at"].stringValue)
            let arriveDate: Date? = {
                if let _arriveDate: String = flightValue["arrives_at"].string {
                    return DateUtils.dateFromStringWithTime(string: _arriveDate)
                } else { return nil }
            }()
            
            flights.append(Flight(origin           : originAirport,
                                  destination      : destinationAirport,
                                  departs_at       : departDate,
                                  arrives_at       : arriveDate,
                                  booking_info     : initBookingInfo(json: flightValue["booking_info"]),
                                  operating_airline: flightValue["operating_airline"].string,
                                  marketing_airline: flightValue["marketing_airline"].string,
                                  aircraft         : flightValue["aircraft"].string,
                                  flight_number    : flightValue["flight_number"].string))
        }
        return flights
    }
    
    private func initBookingInfo(json: JSON) -> BookingInfo {
        
        return BookingInfo(seats_remaining: json["seats_remaining"].int,
                           fare_basis     : json["fare_basis"].string,
                           booking_code   : json["booking_code"].string,
                           cabin_code     : json["cabin_code"].string,
                           travel_class   : json["travel_class"].string?.lowercased(),
                           fare_family    : json["fare_family"].string
        )
    }

    func handleVoiceSearchView(ticketCount: Int) {

        guard let voiceSearchView: VoiceSearchCollectionView = ticketController.voiceSearchView else {
            print("Error: No VoiceSearchController founded")
            return
        }

        if voiceSearchView.voiceSearchViewHasShown {

            // continue voiceSearchView
            if ticketCount == 0 {
                let text: String = "No tickets were matched. Try again with another condition."

                // speak
                voiceSearchView.speak(text)

                // show message balloon
                voiceSearchView.messages.append(Message(text: text as NSString, sender: voiceSearchView.ai))
                voiceSearchView.insertMessage()

                // accept recording
                voiceSearchView.apiAI_isDone = false

            // close voiceSearchView
            } else {

                // speak
                if ticketCount == 1 {
                    voiceSearchView.speak("\(ticketCount) ticket was matched")
                } else {
                    voiceSearchView.speak("\(ticketCount) tickets were matched")
                }
                
                voiceSearchView.voiceSearchViewHasShown = false
            }
        }
    }

    func showDialogOfNumberOfTickets(_ count: Int) {
        ticketController.indicator.hide()

        let message: String = {
            if count == 0 {
                return "Sorry, no result found"
            } else {
                return "\(count) results"
            }
        }()

        let alert: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        alert.show(vc: ticketController!)
    }
}
