//
//  SearchAirport.swift
//  Swiftrip
//
//  Created by maki on 2017-05-23.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import CoreLocation

class SearchAirport: UITableViewController {

    var airportsList: [Airport] = []
    let searchController: UISearchController = UISearchController(searchResultsController: nil)
    var originOrDestination: String?
    var revealvc: SWRevealViewController?
    let indicator: Indicator = Indicator()
    var workItem: DispatchWorkItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .superLightGray

//        // if this was opened from sidebar
//        if let parent = presentingViewController {
//            revealvc = parent as? SWRevealViewController
//
//            let closeItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_close"), style: .done, target: self, action: #selector(close))
//            self.navigationItem.rightBarButtonItem = closeItem
//        }

        // setup navigation
        let backItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_arrow_left"), style: .plain, target: self, action: #selector(clickedBackBarButton))
        self.navigationController?.navigationBar.backgroundColor = .primary
        self.navigationItem.leftBarButtonItems?.append(backItem)
        self.navigationItem.leftBarButtonItem?.title = nil
        self.navigationItem.title = "Search Airport"

        // status bar
        UIApplication.shared.statusBarView?.backgroundColor = .primary
        
        // setup searchController
        definesPresentationContext = true
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        
        // setup tableView
        self.tableView.register(SearchAirportCell.self, forCellReuseIdentifier: SearchAirportCell.cellID)
        self.tableView.register(NoResultCell.self, forCellReuseIdentifier: NoResultCell.cellID)
        self.tableView.tableHeaderView = self.searchController.searchBar
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 80
        self.tableView.rowHeight = UITableViewAutomaticDimension

        // setup indicator
        self.view.addSubview(self.indicator)
        self.indicator.frame = CGRect(x: (DeviceSize.screenWidth() - self.indicator.frame.width) / 2,
                                      y: 0, // this will be set when keyboard was shown
                                      width: self.indicator.frame.width, height: self.indicator.frame.height)

        self.addNotification() // calculating y-position of indicator
    }
    
    // tableView section
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // tableView row
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.airportsList.count <= 0 { return 1 }
        return self.airportsList.count
    }

    // cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // default or no result
        if self.airportsList.count <= 0 {
            let cell: NoResultCell = tableView.dequeueReusableCell(withIdentifier: NoResultCell.cellID, for: indexPath) as! NoResultCell

            if self.searchController.searchBar.text == "" {
                cell.textView.text = "Please input airport name or\nairport code or country."
            } else {
                cell.textView.text = "no matched airport"
            }
            return cell
        
        // show list
        } else {
            let cell: SearchAirportCell = tableView.dequeueReusableCell(withIdentifier: SearchAirportCell.cellID, for: indexPath) as! SearchAirportCell
                cell.lab_airportCode.text = self.airportsList[indexPath.row].airportCode
                cell.lab_airportName.text = self.airportsList[indexPath.row].airportName
                cell.lab_place.text       = self.airportsList[indexPath.row].countryName
            return cell
        }
    }
    
    // selected cell
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if self.airportsList.count > 0 {

            let selectedAirport: Airport = self.airportsList[indexPath.row]

            // save
            searchInfo.save()

            if let target: String = self.originOrDestination {

                switch target {
                case "origin":
                    searchInfo.origin = selectedAirport
                default:
                    searchInfo.destination = selectedAirport
                }

                // go back to previous screen
                self.navigationController?.popViewController(animated: true)

//            // if from sidebar
//            } else if let _revealvc: SWRevealViewController = revealvc {
//
//                searchInfo.origin = selectedAirport
//
//                // set airport name to Sidebar
//                let sidebar: SidebarMenuController = _revealvc.rearViewController as! SidebarMenuController
//                    sidebar.tableView.reloadData()
//
//                // close
//                self.navigationController?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func clickedBackBarButton() {
        self.navigationController?.popViewController(animated: true)
    }

    func close() {
        self.removeNotification()
        self.dismiss(animated: true, completion: nil)
    }

    func addNotification() { // notification for showing keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }

    @objc fileprivate func handleKeyboardShow(_ notification: Notification) {
        guard let userInfo: [AnyHashable : Any] = notification.userInfo else { return }
        let keyboardRect: CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        self.indicator.frame.origin.y = (DeviceSize.screenHeight() - DeviceSize.statusBarHeight - DeviceSize.navBarHeight - keyboardRect.height - (self.indicator.frame.height / 2)) / 2
    }

    func removeNotification() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
}

// MARK: - UISearchResultsUpdating Delegate

extension SearchAirport: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {

        guard let keyword: String = searchController.searchBar.text else { return }
        print(keyword)

        // canceling queue before creating new one
        self.workItem?.cancel()

        // clearing if text is empty
        if searchController.searchBar.text == "" {
            self.indicator.hasBeenShown = false
            self.airportsList.removeAll()
            self.tableView.reloadData()
            return
        }

        self.workItem = DispatchWorkItem {
            self.indicator.hasBeenShown = true

            // fetching airportAPI
            fetchAPI(vc: self, executeApi: citiesAndAirportsAPI(keyword: keyword), completion: { json_citiesAndAirports -> Void in

                var tempAirportList: [Airport] = []

                for json in json_citiesAndAirports.arrayValue {

                    if let airportName: String = json["airport_name"].string {
                        tempAirportList.append(Airport(airportCode  : json["iata"].stringValue,
                                                       airportName  : airportName,
                                                       lat          : json["location"]["lat"].doubleValue,
                                                       lon          : json["location"]["lon"].doubleValue,
                                                       placeName    : json["name"].stringValue,
                                                       countryCode  : json["country_iata"].string,
                                                       cityCode     : json["city_iata"].string))
                    }
                }

                // overwriting airportList
                self.airportsList = tempAirportList

                self.indicator.hasBeenShown = false
                self.tableView.reloadData()
            })
        }

        // excuting queue
        self.workItem?.perform()
    }
}

// MARK: - Cells

class NoResultCell: BaseTableViewCell {
    
    static let cellID: String = "NoResultCell"
    let textView: UITextView = {
        let o: UITextView = UITextView(fontSize: 16)
            o.textColor = .lightGray
            o.textAlignment = .center
        return o
    }()
    
    override func setupView() {
        super.setupView()
        self.contentView.addSubview(textView)
        self.textView.layout(top: self.contentView.topAnchor, topConstant: 40,
                             left: self.contentView.leftAnchor,
                             bottom: self.contentView.bottomAnchor, bottomConstant: 40,
                             right: self.contentView.rightAnchor,
                             heightConstant: 100)
    }
}

class SearchAirportCell: BaseTableViewCell {

    static let cellID: String = "SearchAirportCell"
    let lab_airportCode: UILabel = {
        let o: UILabel = UILabel(fontSize: 16, .lightGray, textAlign: .left)
        return o
    }()
    let lab_airportName: UILabel = {
        let o: UILabel = UILabel(fontSize: 14, .black, textAlign: .left)
            o.numberOfLines = 0
            o.lineBreakMode = NSLineBreakMode.byWordWrapping
        return o
    }()
    let lab_place: UILabel = {
        let o: UILabel = UILabel(fontSize: 12, .lightGray, textAlign: .left)
        o.numberOfLines = 0
        o.lineBreakMode = NSLineBreakMode.byWordWrapping
        return o
    }()
    let separator: UIView = {
        let o: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            o.backgroundColor = UIColor.superLightGray
        return o
    }()

    override func setupView() {
        super.setupView()

        // layout
        self.contentView.addSubview(lab_airportCode)
        self.contentView.addSubview(lab_airportName)
        self.contentView.addSubview(lab_place)
        self.contentView.addSubview(separator)
        self.lab_airportCode.layout(top: self.contentView.topAnchor, topConstant: 8,
                                    left: self.contentView.leftAnchor, leftConstant: 16)
        self.lab_airportName.layout(top: self.lab_airportCode.topAnchor, topConstant: 8,
                                    left: self.contentView.leftAnchor, leftConstant: 68,
                                    bottom: self.lab_place.topAnchor, bottomConstant: 6,
                                    right: self.contentView.rightAnchor, rightConstant: 8)
        self.lab_place.layout(top: lab_airportCode.bottomAnchor,
                              left: self.lab_airportName.leftAnchor, leftConstant: -3,
                              bottom: self.contentView.bottomAnchor, bottomConstant: 10,
                              right: self.contentView.rightAnchor, rightConstant: 8)
        self.separator.layout(left: self.contentView.leftAnchor,
                              bottom: self.contentView.bottomAnchor,
                              right: self.contentView.rightAnchor,
                              heightConstant: 1)
    }
}
