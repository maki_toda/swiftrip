//
//  FlightList+simple.swift
//  Swiftrip
//
//  Created by maki on 2017-04-20.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import SwiftyJSON

extension FlightList {
    
    func initTicketForNoDestination(_ result: JSON, currency: String, hasInbound: Bool) -> Ticket {
        
         /*
         
         Example result
         
         {
            "airline" : "VY",
            "destination" : "BCN",
            "departure_date" : "2017-05-03",
            "price" : "63.90",
            "return_date" : "2017-05-04"
         }
         
         */
        
        return Ticket(apiName: API_NAME.flightInspirationSearchTickets,
                      request_id: nil,
                      currency: currency,
                      itineraries: initItineraryForNoDestination(result, hasInbound: hasInbound),
                      deep_link: deeplink(result),
                      merchant: nil,
                      travel_class: nil,
                      fare_family: nil,
                      cabin_code: nil,
                      fare: Fare(price_per_adult: nil,
                                 price_per_child: nil,
                                 price_per_infant: nil,
                                 currency: currency,
                                 total_price: Double(result["price"].string!),
                                 service_fees: nil,
                                 creditcard_fees: nil),
                      airline: result["airline"].string,
                      img: nil)
    }
    
    private func deeplink(_ result: JSON) -> String {
        var param : [String] = []
        param.append("marker=\(Config.marker)")
        if let origin_iata: String = searchInfo?.origin {
            param.append("origin_iata=\(origin_iata)")
        }
        param.append("destination_iata=\(result["destination"].string!)")
        param.append("depart_date=\(result["departure_date"].string!)")
        if let return_date: String = result["return_date"].string { // ex. 2016-12-15
            param.append("return_date=\(return_date)")
        }
        if let oneway: Bool = searchInfo?.oneWay {
            if oneway {
                param.append("oneway=1")
            } else {
                param.append("oneway=0")
            }
        }
        if let adults: Int = searchInfo?.adult {
            param.append("adults=\(adults)")
        }
        if let children: Int = searchInfo?.child {
            param.append("children=\(children)")
        }
        if let infants: Int = searchInfo?.infant {
            param.append("infants=\(infants)")
        }
        if let trip_class: Int = searchInfo?.trip_class {
            param.append("trip_class=\(trip_class)") // Economy: 0, Business: 1, First: 2
        }
        // Initiate search automatically (true: the search is loaded, false: form is filled out but search does not start)
        param.append("with_request=false")
        let url: String = "http://jetradar.com/searches/new?\(param.joined(separator: "&"))"
        return url
    }
    
    private func initItineraryForNoDestination(_ result: JSON, hasInbound: Bool) -> [Itinerary] {
        let flights: [Flight] = initFlightsForNoDestination(result, hasInbound: hasInbound)
        var itineraries: [Itinerary] = [Itinerary]()
        let outbound: Bound = Bound(flights: [flights[0]], duration: nil)
        var inbound: Bound? = nil
        if hasInbound {
            inbound = Bound(flights: [flights[1]], duration: nil)
        }
        itineraries.append(Itinerary(outbound: outbound, inbound: inbound))
        return itineraries
    }
    
    private func initFlightsForNoDestination(_ result: JSON, hasInbound: Bool) -> [Flight] {
        var departure_date: String
        var origin_code: String
        var destination_code: String
        var flights: [Flight] = [Flight]()
        
        for index: Int in 0...1 {
            
            // flights[0]: departure
            if index == 0 {
                departure_date = result["departure_date"].string!
                origin_code = (searchInfo?.origin!)!
                destination_code = result["destination"].string!
                
            // flights[1]: return
            } else {
                if hasInbound {
                    departure_date = result["return_date"].string!
                    origin_code = result["destination"].string!
                    destination_code = (searchInfo?.origin!)!
                } else {
                    continue
                }
            }
            
            flights.append(Flight(origin: Airport(code: origin_code,
                                                  name: nil,
                                                  location: nil,
                                                  placeName: nil,
                                                  country_code: nil,
                                                  country: nil,
                                                  city_code: nil,
                                                  city: nil,
                                                  time_zone: nil,
                                                  terminal: nil),
                                  destination: Airport(code: destination_code,
                                                       name: nil,
                                                       location: nil,
                                                       placeName: nil,
                                                       country_code: nil,
                                                       country: nil,
                                                       city_code: nil,
                                                       city: nil,
                                                       time_zone: nil,
                                                       terminal: nil),
                                  departs_at: DateUtils.dateFromStringWithoutTime(string:departure_date),
                                  arrives_at: nil,
                                  booking_info: nil,
                                  operating_airline: result["airline"].string,
                                  marketing_airline: result["airline"].string,
                                  aircraft: nil,
                                  flight_number: nil))
        }
        return flights
    }
}
