//
//  SidebarMenuController.swift
//  Swiftrip
//
//  Created by maki on 2017-06-04.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class SidebarMenuController: UITableViewController {

    let menu: [String] = ["Home Airport", "Currency", "FAQ"] //, "Terms of Service", "Privacy Policy"]
    let identifier: String = "SidebarMenuCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.register(SidebarMenuCell.self, forCellReuseIdentifier: identifier)
        self.tableView.alwaysBounceVertical = false
        self.tableView.separatorStyle = .none
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SidebarMenuCell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! SidebarMenuCell
            cell.textLabel?.text = menu[indexPath.row]

        if indexPath.row == 0 {
            cell.subLabel.text = searchInfo.origin?.airportCode

        } else if indexPath.row == 1 {
            cell.subLabel.text = searchInfo.currency.rawValue
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let revealController = self.revealViewController() {
            print("clicked")
            var root: UIViewController

            switch indexPath.row {
            case 0:
                root = SearchAirport()
            case 1:
                root = CurrencyController()
            case 2:
                root = HelpController()

                // TODO: add another menu link pattern
            default:
                return
            }

            // close sidebar
            revealController.revealToggle(self)

            // show controller
            let nvc: UINavigationController = UINavigationController(rootViewController: root)
            revealController.show(nvc, sender: self)
        }
    }
}

class SidebarMenuCell: BaseTableViewCell {

    let subLabel: UILabel = {
        let o: UILabel = UILabel()
        o.font = UIFont(name: fontName, size: 16)
        o.textColor = UIColor.lightGray
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()

    override func setupView() {
        self.selectionStyle = .none
        self.textLabel?.textColor = UIColor.darkGray
        self.textLabel?.font = UIFont(name: fontName, size: 16)
        self.contentView.addSubview(self.subLabel)
        self.subLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -80).isActive = true
        self.subLabel.centerYAnchor.constraint(equalTo: self.textLabel!.centerYAnchor).isActive = true
    }
}
