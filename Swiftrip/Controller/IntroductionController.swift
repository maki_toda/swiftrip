//
//  IntroductionController.swift
//  Swiftrip
//
//  Created by maki on 2017-04-22.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class IntroductionController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    let cellId    : String = "cellId"
    let lastCellId: String = "lastCellId"
    let pages: [Page] = {
        let page1: Page = Page(title: "Hi, I'm Qoo who is AI.", message: "Let me help you to search flight tickets.")
        let page2: Page = Page(title: "Tell me your trip plan.", message: "I can recognize your voice and I can speak aswell.")
        let page3: Page = Page(title: "I'll show you matched flights.", message: "Let's swip and start it!")
        return [page1, page2, page3]
    }()

    var pageControlBottomAnchor: NSLayoutConstraint?
    var skipButtonTopAnchor    : NSLayoutConstraint?
    var aiImageCenterYAnchor   : NSLayoutConstraint?

    lazy var collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 0
        let o: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            o.backgroundColor = .clear
            o.dataSource = self
            o.delegate = self
            o.isPagingEnabled = true
        return o
    }()

    lazy var pageControl: UIPageControl = {
        let o: UIPageControl = UIPageControl()
            o.pageIndicatorTintColor = .lightGray
            o.currentPageIndicatorTintColor = UIColor(r: 247, g: 154, b: 27, a: 1.0)
            o.numberOfPages = self.pages.count + 1
        return o
    }()

    let skipButton: UIButton = {
        let o: UIButton = UIButton(fontSize: 16, .white, text: "Skip")
            o.addTarget(self, action: #selector(dissmiss), for: .touchUpInside)
        return o
    }()

    let aiImage: AIImage = AIImage(size: 120)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .primary

        self.collectionView.register(IntroductionCell.self, forCellWithReuseIdentifier: cellId)
        self.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: lastCellId)

        self.setupView()
    }

    fileprivate func setupView() {
        self.view.addSubview(self.collectionView)
        self.view.addSubview(self.pageControl)
        self.view.addSubview(self.skipButton)
        self.view.addSubview(self.aiImage)

        // layout
        self.collectionView.anchorToTop(self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor)
        
        self.aiImageCenterYAnchor = self.aiImage.anchor(centerX: self.view.centerXAnchor, centerY: self.view.centerYAnchor, centerYConstant: -60, widthConstant: 120, heightConstant: 120)[1]

        self.pageControlBottomAnchor = self.pageControl.anchor(left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: view.rightAnchor, widthConstant: 0, heightConstant: 40)[1]

        self.skipButtonTopAnchor = self.skipButton.anchor(top: view.topAnchor, topConstant: 16,
                                                          left: self.view.leftAnchor,
                                                          widthConstant: 60, heightConstant: 50).first

        // ai moves in screen from bottom
        self.aiImage.frame.origin.y = -300
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.aiImage.frame.origin.y = 0
        }, completion: nil)
    }

    // scrolling
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        let pageNumber: Int = Int(targetContentOffset.pointee.x / view.frame.width)

        pageControl.currentPage = pageNumber

        // second page
        if pageNumber == 1 {
            // AI moves to left
            self.aiImageCenterYAnchor?.constant = -170

        // last page
        } else if pageNumber == pages.count {

            // AI moves out of screen
            self.aiImageCenterYAnchor?.constant = -(DeviceSize.screenHeight())
            self.pageControlBottomAnchor?.constant = 40
            self.skipButtonTopAnchor?.constant = -40

        } else {
            // back on regular pages
            self.aiImageCenterYAnchor?.constant = -60
            self.pageControlBottomAnchor?.constant = 0
            self.skipButtonTopAnchor?.constant = 16
        }

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()

            // fade out
            if pageNumber == self.pages.count {
                self.collectionView.alpha = 0
            }

        }, completion: { (_) -> Void in
            if pageNumber == self.pages.count {
                // desable
                self.aiImage.isHidden = true
                self.skipButton.isHidden = true
                self.collectionView.isHidden = true

                // dissmiss
                self.dissmiss()
            }
        })
    }

    // item
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pages.count + 1
    }

    // cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        // last page doesn't use IntroductionCell because it's empty cell
        if indexPath.item == pages.count {
            let lastCell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: lastCellId, for: indexPath)
            return lastCell
        }

        let cell: IntroductionCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! IntroductionCell
        cell.page = pages[indexPath.item]
        if indexPath.item == 1 {
            cell.contentView.addSubview(cell.boy)
            cell.contentView.addSubview(cell.balloon_location)
            cell.contentView.addSubview(cell.balloon_date)
            cell.contentView.addSubview(cell.balloon_money)
            cell.boy.anchorToTop(bottom: cell.textView.topAnchor, right: cell.rightAnchor)

            cell.balloon_location.layout(top: cell.boy.topAnchor, topConstant: -90, left: cell.leftAnchor, leftConstant: -20, widthConstant: 120)
            cell.balloon_money.layout(top: cell.boy.topAnchor, topConstant: -40, left: cell.leftAnchor, leftConstant: 80, widthConstant: 80)
            cell.balloon_date.layout(left: cell.leftAnchor, leftConstant: 10, bottom: cell.boy.bottomAnchor, bottomConstant: -20, widthConstant: 100)

            // balloon fade in
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
                cell.balloon_location.alpha = 0.9
                UIView.animate(withDuration: 0.5, delay: 1, options: .curveEaseOut, animations: {
                    cell.balloon_money.alpha = 0.7
                    UIView.animate(withDuration: 0.5, delay: 1.5, options: .curveEaseOut, animations: {
                        cell.balloon_date.alpha = 0.7
                    }, completion: nil)
                }, completion: nil)
            }, completion: nil)
        }
        return cell
    }

    // size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }

    @objc fileprivate func dissmiss() {

        // save
        skipIntroduction = true
        let searchInfo_data: Data = NSKeyedArchiver.archivedData(withRootObject: skipIntroduction)
        UserDefaults.standard.set(searchInfo_data, forKey: userDefaultKey_skipIntroduction)
        UserDefaults.standard.synchronize()

        // show VoiceSearchView
        ticketController.voiceSearchView.voiceSearchViewHasShown = true

        // dismiss IntroductionView
        self.dismiss(animated: false, completion: nil)
    }
}

class IntroductionCell: BaseCollectionViewCell {

    var page: Page? {
        didSet {
            guard let page = page else { return }

            let color: UIColor = UIColor(white: 0.2, alpha: 1)

            let attributedText: NSMutableAttributedString = NSMutableAttributedString(string: page.title, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium), NSForegroundColorAttributeName: color])
                attributedText.append(NSAttributedString(string: "\n\n\(page.message)", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: color]))

            let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
                paragraphStyle.alignment = .center

            let length: Int = attributedText.string.characters.count
            attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: length))

            self.textView.attributedText = attributedText
        }
    }

    let boy: UIImageView = {
        let width: CGFloat = 120
        let o: UIImageView = UIImageView(image: UIImage(named: "boy"))
            o.frame = CGRect(x: 0, y: 0, width: width, height: width * 1.7)
            o.contentMode = .scaleAspectFill
        return o
    }()

    let balloon_location: UIImageView = {
        let o: UIImageView = UIImageView(image: UIImage(named: "balloon_location"))
            o.contentMode = .scaleAspectFit
            o.alpha = 0
        return o
    }()

    let balloon_money: UIImageView = {
        let o: UIImageView = UIImageView(image: UIImage(named: "balloon_money"))
            o.contentMode = .scaleAspectFit
            o.alpha = 0
        return o
    }()

    let balloon_date: UIImageView = {
        let o: UIImageView = UIImageView(image: UIImage(named: "balloon_date"))
            o.contentMode = .scaleAspectFit
            o.alpha = 0
        return o
    }()

    let textView: UITextView = {
        let o = UITextView()
            o.isEditable = false
            o.isSelectable = false
            o.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
        return o
    }()

    var imageView = UIView()

    override func setupView() {
        self.contentView.addSubview(imageView)
        self.contentView.addSubview(textView)

        self.textView.layout(left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor)
        self.textView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3).isActive = true
    }
}
