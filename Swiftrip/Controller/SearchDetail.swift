//
//  SearchDetail.swift
//  Swiftrip
//
//  Created by maki on 2017-04-22.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class SearchDetail: UIViewController {
    
    public var searchInfo: SearchInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }    
    
    func setupView(){
        self.view.backgroundColor = Utility.color_primary
        self.view.addSubview(fromLabel)
        self.view.addSubview(toLabel)
        self.view.addSubview(depatureAirportLabel)
        self.view.addSubview(arrivalAirportLabel)
        self.view.addSubview(depatureCityLabel)
        self.view.addSubview(arrivalCityLabel)
        self.view.addSubview(segment1Controll)
        self.view.addSubview(depatureLabel)
        self.view.addSubview(arrivalLabel)
        self.view.addSubview(depatureDateLabel)
        self.view.addSubview(arrivalDateLabel)
        self.view.addSubview(passengerLabel)
        self.view.addSubview(passengerNumLabel)
        self.view.addSubview(cavinClassLabel)
        self.view.addSubview(segment2Controll)
        self.view.addSubview(switchBtn)
        self.view.addSubview(searchBtn)
        
        let elements = ["from" : fromLabel,
                        "to" : toLabel,
                        "depAir" : depatureAirportLabel,
                        "arrAir" : arrivalAirportLabel,
                        "depCity" : depatureCityLabel,
                        "arrCity" : arrivalCityLabel,
                        "depDate" : depatureDateLabel,
                        "arrDate" : arrivalDateLabel,
                        "dep" : depatureLabel,
                        "arr" : arrivalLabel,
                        "seg1" : segment1Controll,
                        "seg2" : segment2Controll,
                        "pass" : passengerLabel,
                        "passNum" : passengerNumLabel,
                        "cavin" : cavinClassLabel,
                        "switch" : switchBtn,
                        "search" : searchBtn]
        
        elements.forEach{ $1.translatesAutoresizingMaskIntoConstraints = false}
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[from]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-90-[from]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-220-[to]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-90-[to]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[depAir]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-130-[depAir]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-220-[arrAir]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-130-[arrAir]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[depCity]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-170-[depCity]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-220-[arrCity]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-170-[arrCity]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-85-[seg1]-85-|", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-220-[seg1]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-65-[seg2]-65-|", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-455-[seg2]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[dep]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-270-[dep]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-220-[arr]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-270-[arr]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[depDate]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-300-[depDate]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-220-[arrDate]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-300-[arrDate]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[pass]|", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-345-[pass]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[passNum]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-375-[passNum]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[cavin]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-415-[cavin]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-300-[switch]", options: .alignAllTop, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-90-[switch]", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-100-[search]-100-|", options: .alignAllLeft, metrics: nil, views: elements))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-515-[search]", options: .alignAllLeft, metrics: nil, views: elements))
    }
    
    let fromLabel: UILabel = {
        let label = UILabel()
        label.text = "from"
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 50).isActive = true
        return label
    }()
    
    let toLabel: UILabel = {
        let label = UILabel()
        label.text = "to"
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 50).isActive = true
        return label
    }()
    
    let depatureAirportLabel: UILabel = {
        let label = UILabel()
        label.text = "YVR"
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 38)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return label
    }()
    
    let arrivalAirportLabel: UILabel = {
        let label = UILabel()
        label.text = "HND"
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 38)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return label
    }()
    
    let depatureCityLabel: UILabel = {
        let label = UILabel()
        label.text = "Vancouver"
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return label
    }()
    
    let arrivalCityLabel: UILabel = {
        let label = UILabel()
        label.text = "Tokyo"
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return label
    }()
    
    let segment1Controll: UISegmentedControl = {
        let segment = UISegmentedControl()
        segment.backgroundColor = UIColor.darkGray
        segment.tintColor = .white
        segment.insertSegment(withTitle: "Round Trip", at: 0, animated: true)
        segment.insertSegment(withTitle: "One Way", at: 1, animated: true)
        segment.translatesAutoresizingMaskIntoConstraints = false
        segment.heightAnchor.constraint(equalToConstant: 30).isActive = true
        segment.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return segment
    }()
    
    let depatureLabel: UILabel = {
        let label = UILabel()
        label.text = "Depature"
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return label
    }()
    
    let arrivalLabel: UILabel = {
        let label = UILabel()
        label.text = "Return"
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return label
    }()
    
    let depatureDateLabel: UITextField = {
        let label = UITextField()
        label.placeholder = "Thu / 02 / Apr"
        label.backgroundColor = .white
        label.borderStyle = UITextBorderStyle.none
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.font = UIFont(name: (label.font?.fontName)!, size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return label
    }()
    
    let arrivalDateLabel: UITextField = {
        let label = UITextField()
        label.placeholder = "Thu / 02 / Apr"
        label.backgroundColor = .white
        label.borderStyle = UITextBorderStyle.none
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.font = UIFont(name: (label.font?.fontName)!, size: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return label
    }()
    
    let passengerLabel: UILabel = {
        let label = UILabel()
        label.text = "Passenger"
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 130).isActive = true
        return label
    }()
    
    let passengerNumLabel: UILabel = {
        let label = UILabel()
        label.text = "2 adults"
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 150).isActive = true
        return label
    }()
    
    let cavinClassLabel: UILabel = {
        let label = UILabel()
        label.text = "Cavin Class"
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.heightAnchor.constraint(equalToConstant: 30).isActive = true
        label.widthAnchor.constraint(equalToConstant: 150).isActive = true
        return label
    }()
    
    let segment2Controll: UISegmentedControl = {
        let segment2 = UISegmentedControl()
        segment2.backgroundColor = UIColor.darkGray
        segment2.tintColor = .white
        // segment2.center.x = segment2.center.x
        segment2.insertSegment(withTitle: "Economy", at: 0, animated: true)
        segment2.insertSegment(withTitle: "Business", at: 1, animated: true)
        segment2.insertSegment(withTitle: "First", at: 2, animated: true)
        segment2.translatesAutoresizingMaskIntoConstraints = false
        segment2.heightAnchor.constraint(equalToConstant: 30).isActive = true
        segment2.widthAnchor.constraint(equalToConstant: 120).isActive = true
        return segment2
    }()
    
    let switchBtn: UIButton = {
        let o = UIButton()
        o.backgroundColor = UIColor.darkGray
        o.setTitle("S", for: .normal)
        o.clipsToBounds = true
        o.addTarget(self, action: #selector(pressSwitchButton), for: .touchUpInside)
        o.heightAnchor.constraint(equalToConstant: 40).isActive = true
        o.widthAnchor.constraint(equalToConstant: 40).isActive = true
        o.layer.cornerRadius = 20.0
        return o
    }()
    
    let searchBtn: UIButton = {
        let o = UIButton()
        o.backgroundColor = UIColor.darkGray
        o.setTitle("Search", for: .normal)
        o.layer.cornerRadius = 50.0
        o.clipsToBounds = true
        o.addTarget(self, action: #selector(pressButton), for: .touchUpInside)
        o.heightAnchor.constraint(equalToConstant: 100).isActive = true
        o.widthAnchor.constraint(equalToConstant: 100).isActive = true
        return o
    }()
    
    func pressButton() {
//        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        layout.scrollDirection = .vertical
        let vc = SearchFlightController(style: .plain)
        //searchInfo = searchInfo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pressSwitchButton() {
        let vc = Search()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
