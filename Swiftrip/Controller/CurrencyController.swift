//
//  CurrencyController.swift
//  Swiftrip
//
//  Created by maki on 2017-06-06.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class CurrencyController: UITableViewController {

    let identifier = "CurrencyCell"

    override func viewDidLoad() {
        super.viewDidLoad()

        // tableView
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.allowsMultipleSelection = false
        self.tableView.register(CurrencyCell.self, forCellReuseIdentifier: self.identifier)

        // navigation
        self.navigationItem.title = "Currency"
        let closeItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_close"), style: .done, target: self, action: #selector(close))
        self.navigationItem.rightBarButtonItem = closeItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCurrency.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CurrencyCell = tableView.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath) as! CurrencyCell
        cell.currancyCode.text = arrayCurrency[indexPath.row].rawValue
        cell.currancyName.text = arrayCurrency[indexPath.row].name()

        // set checkmark
        if indexPath.row == searchInfo.currency.hashValue {
            cell.accessoryType = .checkmark
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("clicked")
        if let cell: CurrencyCell = tableView.cellForRow(at:indexPath) as? CurrencyCell {

            // set checkmark
            cell.accessoryType = .checkmark
            searchInfo.currency = convertCurrency(from: arrayCurrency[indexPath.row].rawValue)

            // save to UserDefault
            searchInfo.save()

            // reload the currency on sidebar
            if let parent: UIViewController = presentingViewController,
               let revealvc: SWRevealViewController = parent as? SWRevealViewController {
                let sidebar: SidebarMenuController = revealvc.rearViewController as! SidebarMenuController
                sidebar.tableView.reloadData()
            }
        }
        close()
    }

    func close() {
        dismiss(animated: true, completion: nil)
    }
}


class CurrencyCell: BaseTableViewCell {

    let currancyCode: UILabel = {
        let o: UILabel = UILabel()
            o.font = UIFont(name: fontName, size: 16)
            o.textColor = UIColor.darkGray
            o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()

    let currancyName: UILabel = {
        let o: UILabel = UILabel()
            o.font = UIFont(name: fontName, size: 12)
            o.textColor = UIColor.darkGray
            o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()

    override func setupView() {

        self.selectionStyle = UITableViewCellSelectionStyle.none

        self.contentView.addSubview(currancyCode)
        self.contentView.addSubview(currancyName)
        currancyCode.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 16).isActive = true
        currancyCode.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        currancyName.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 80).isActive = true
        currancyName.centerYAnchor.constraint(equalTo: self.currancyCode.centerYAnchor).isActive = true
    }
}
