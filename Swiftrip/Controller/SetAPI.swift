//
//  SetAPI.swift
//  Swiftrip
//
//  Created by maki on 2017-04-09.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

func fetchAPI(vc: UIViewController, executeApi: API?, completion: ((_ : JSON) -> Void)?) {

    guard let api: API = executeApi else { return }

    let headers: HTTPHeaders? = {
        if let token = api.token {
            return ["X-Access-Token": token]
        }
        return nil
    }()
    
    Alamofire.request(api.url, method: api.method, parameters: api.parameters, headers: headers).responseJSON { response in
        print(response.request ?? "")
        switch response.result {
        case .success(let result):
            completion!(JSON(result))

        case .failure(let error):

            // TODO: hide indicator when failure
            print(error)
            let alert: UIAlertController = UIAlertController(title: "Error", message: "Please try again in a while.", preferredStyle: .actionSheet)
            alert.show(vc: vc)
        }
    }
}

fileprivate let amadeus_endpoint               : String = "https://api.sandbox.amadeus.com/v1.2/flights"
fileprivate let travelPayouts_endpoint         : String = "http://api.travelpayouts.com"
fileprivate let travelPayouts_planes           : String = travelPayouts_endpoint + "/data/planes.json"
fileprivate let travelPayouts_countries        : String = travelPayouts_endpoint + "/data/countries.json"
fileprivate let travelPayouts_routes           : String = travelPayouts_endpoint + "/data/countries.json"
fileprivate let travelPayouts_airlinesAlliances: String = travelPayouts_endpoint + "/data/airlines_alliances.json"
fileprivate let travelPayouts_cities           : String = travelPayouts_endpoint + "/data/cities.json"
fileprivate let travelPayouts_airlines         : String = travelPayouts_endpoint + "/data/airlines.json"


// get location
func fetchUserLocationAPI(vc: UIViewController) {
    fetchAPI(vc: vc, executeApi: userLocationAPI(), completion: { (json: JSON) -> Void in
        print(json)
        // set searchInfo
        searchInfo.location = UserLocation(airportCode: json["iata"].stringValue,
                                           airportName: nil,
                                           cityName:    json["name"].stringValue,
                                           countryName: json["country_name"].stringValue,
                                           location:    json["coordinates"].stringValue)

        searchInfo.origin = Airport(airportCode: json["iata"].stringValue,
                                    airportName: nil,
                                    lat: searchInfo.location?.location.coordinate.latitude,
                                    lon: searchInfo.location?.location.coordinate.longitude,
                                    placeName: json["name"].stringValue,
                                    countryCode: nil,
                                    cityCode: nil,
                                    timeZone: nil,
                                    terminal: nil,
                                    photoURL: nil)

        // get airportName from UserDefaults
        if let airport: Airport = Airport.getAirportFromUserDefaults(airportCode: searchInfo.location!.airportCode) {
            searchInfo.location!.airportName = airport.airportName
            searchInfo.origin!.airportName   = airport.airportName

        // get airportName from API
        } else {
            fetchAirportAPI(vc: vc)
        }
    })
}

func fetchAirportAPI(vc: UIViewController) {

    guard let location: UserLocation = searchInfo.location else { return }

    Airport.fetchAirportAPIFromAirportCode(vc: vc, airportCode: location.airportCode, completion: { airport -> Void in

        // set airportName
        searchInfo.origin?.airportName = airport.airportName
        location.airportName = airport.airportName

        // set airports
        if airports != nil {
            airports!.append(airport)
        } else {
            airports = [airport]
        }

        // save
        let data: Data = NSKeyedArchiver.archivedData(withRootObject: airports!)
        UserDefaults.standard.set(data, forKey: userDefaultKey_airports)
        UserDefaults.standard.synchronize()
    })
}

func LatestPricesAPI(currency: String = "usd",
                     origin: String?,
                     destination: String?,
                     show_to_affiliates: Bool?,
                     month: String?) -> API {
    let url: String = travelPayouts_endpoint + "/v2/prices/latest"
    let parameters: Parameters = [
        "currency": currency,                           // Default value is rub. Minimum length 3.
        "origin":                                       // City IATA code. Default value is LED. Minimum length 3.
            { () -> String in
                if let v: String = origin {
                    return v
                } else { return ""}},
        "destination":                                  // City IATA code. Default value is HKT. Minimum length 3.
            { () -> String in
                if let v: String = destination {
                    return v
                } else { return ""}},
        "show_to_affiliates":                           // false - all prices, true - prices found with affiliate marker (recommended). Default value is true.
            { () -> Bool in
                if let v: Bool = show_to_affiliates {
                    return v
                } else { return false}},
        "month":                                        // First day of month in format "%Y-%m-01". Default value is 2017-05-11.
            { () -> String in
                if let v: String = month {
                    return v
                } else { return ""}}
    ]
    return API.init(url, parameters: parameters)
}

// Price calendar which returns prices for dates closest to the ones specified.
func PriceCalendarOnWeekAPI(currency: String = "usd",
                            origin: String = "",
                            destination: String = "",
                            affiliate: Bool = true,
                            depart_date: String?,
                            return_date: String?) -> API {
    let url: String = travelPayouts_endpoint + "/v2/prices/week-matrix"
    let parameters: Parameters = [
        "currency": currency,               // "usd"
        "origin": origin,                   // City IATA code. Default value is LED. Minimum length 3.
        "destination": destination ,        // City IATA code. Default value is HKT. Minimum length 3.
        "show_to_affiliates": affiliate,    // false - all prices, true - prices found with affiliate marker (recommended). Default value is true.
        "depart_date":
            { () -> String in
                if let v: String = depart_date {
                    return v
                } else { return ""}},   // format '%Y-%m-%d'. Default value is 2017-04-24.
        "return_date":
            { () -> String in
                if let v: String = return_date {
                    return v
                } else { return ""}},   // format '%Y-%m-%d'. Default value is 2017-05-08.
    ]
    return API.init(url, parameters: parameters)
}

func PricesCalendarOnMonthAPI(currency: String = "usd",
                              origin: String = "",
                              destination: String = "",
                              affiliate: Bool = true,
                              depart_date: String?,
                              return_date: String?) -> API {
    let url: String = travelPayouts_endpoint + "/v2/prices/month-matrix"
    let parameters: Parameters = [
        "currency": currency,               // "usd"
        "origin": origin,                   // City IATA code. Default value is LED. Minimum length 3.
        "destination": destination,         // City IATA code. Default value is HKT. Minimum length 3.
        "show_to_affiliates": affiliate,    // false - all prices, true - prices found with affiliate marker (recommended). Default value is true.
        "depart_date":
            { () -> String in
                if let v: String = depart_date {
                    return v
                } else { return ""}},   // format '%Y-%m-%d'. Default value is 2017-04-24.
        "return_date":
            { () -> String in
                if let o: String = return_date {
                    return o
                } else { return ""}},   // format '%Y-%m-%d'. Default value is 2017-05-08.
    ]
    return API.init(url, parameters: parameters)
}

// Special offers, in XML format. Returns newest special offers.
func SpecialOffersAPI() -> API {
    let url: String = travelPayouts_endpoint + "/v2/prices/special-offers"
    return API.init(url)
}

// The best offers on holidays from popular cities
func CheapFlightsForHolidaysAPI(currency: String = "usd") -> API {
    let url: String = travelPayouts_endpoint + "/v2/prices/holidays-by-routes"
    let parameters: Parameters = [
        "currency": currency // Currency of prices. Default value is rub. Maximum length 3. Minimum length 3.
    ]
    return API.init(url, parameters: parameters)
}

// Returns the cheapest non-stop tickets, as well as tickets with 1 or 2 stops, for the selected route for each day of the selected month.
func TicketsFromCityForEveryDayOfMonthAPI(currency: String = "usd",
                                          origin: String = "",
                                          destination: String = "",
                                          depart_date: String = "",
                                          return_date: String = "",
                                          calendar_type: String = "",
                                          trip_duration: NSInteger = 0) -> API {
    let url: String = travelPayouts_endpoint + "/v1/prices/calendar"
    let parameters: Parameters = [
        "currency": currency,               // Default value is rub. Minimum length 3.
        "origin": origin,                   // City IATA code. Default value is MOW. Minimum length 3.
        "destination": destination,         // City IATA code. Default value is LED. Minimum length 3.
        "depart_date": depart_date,         // month format '%Y-%m'.
        "return_date": return_date,         // Pay attention! If the return_date is not specified, you will get the One way flights..
        "calendar_type": calendar_type,     // Field for which the calendar is to be built. Default value is departure_date. Should be in ["departure_date", "return_date"].
        "trip_duration": trip_duration      // Trip duration in days.
    ]
    return API.init(url, parameters: parameters)
}

// Returns the cheapest non-stop tickets, as well as tickets with 1 or 2 stops, for the selected route with filters by departure and return date.
func CheapestTicketsAPI(currency: String,
                        origin: String,
                        destination: String?,
                        depart_date: String?,
                        return_date: String?,
                        page: NSInteger = 1) -> API {
    let url: String = travelPayouts_endpoint + "/v1/prices/cheap"
    let parameters: Parameters = [
        "currency": currency,               // Default value is rub. Minimum length 3.
        "origin": origin,                   // City IATA code. Default value is MOW. Minimum length 3.
        "destination":
            { () -> String in
                if let v: String = destination {
                    return v
                } else { return "-"}},  // City IATA code. Default value is HKT. Minimum length 3.
        "depart_date":
            { () -> String in
                if let v: String = depart_date {
                    return v
                } else { return ""}},   // format '%Y-%m-%d' or month format '%Y-%m'.
        "return_date":
            { () -> String in
                if let v: String = return_date {
                    return v
                } else { return ""}},   // format '%Y-%m-%d' or month format '%Y-%m'.
        "page": page                        // Default value is 1.
    ]
    return API.init(url, parameters: parameters)
}

// Non-stop tickets. Returns the cheapest non-stop tickets for the selected route with filters by departure and return date.
func DirectFlightsAPI() -> API {
    let url: String = travelPayouts_endpoint + "/v1/prices/direct"
//    Params
//    currency
//    Currency of prices. Default value is RUB. Maximum length 3. Minimum length 3.
//    origin
//    Origin. City IATA code. Default value is MOW. Maximum length 3. Minimum length 3.
//    destination
//    Destination. City IATA code. Default value is HKT. Maximum length 3. Minimum length 1.
//    depart_date
//    Depart. Date format '%Y-%m-%d' or month format '%Y-%m'.
//    return_date
//    Return. Date format '%Y-%m-%d' or month format '%Y-%m'.
    return API.init(url)
}

func CheapestTichetsGoupedByMonthAPI() -> API {
    let url: String = travelPayouts_endpoint + "/v1/prices/monthly"
//    Params
//    currency
//    Currency of prices. Default value is RUB. Maximum length 3. Minimum length 3.
//    origin
//    Origin. City IATA code. Default value is MOW. Maximum length 3. Minimum length 3.
//    destination
//    Destination. City IATA code or country code. Default value is HKT. Maximum length 3. Minimum length 1.
    return API.init(url)
}

// Returns the routes that an airline flies and sorts them by popularity.
func PopularAirlineRoutesAPI(airline_code: String,
                             limit: NSInteger = 100) -> API {
    let url: String = travelPayouts_endpoint + "/v1/airline-directions"
    let parameters = [
        "airline_code": airline_code,   // Default value is SU. Maximum length 2. Minimum length 2.
        "limit": limit                  // Records limit per page. Not more than 1. Not less than 1000. Default value is 100.
        ] as [String : Any]
    return API.init(url, parameters: parameters)
}

// Returns most popular routes for selected origin
func PopularRoutesFromCityAPI(
    currency: String = "usd",
    origin: String) -> API {
    let url: String = travelPayouts_endpoint + "/v1/city-directions"
    let parameters = [
        "currency": currency,   // City IATA code. Default value is MOW. Maximum length 3. Minimum length 3.
        "origin": origin        // Default value is RUB. Maximum length 3. Minimum length 3.
    ]
    return API.init(url, parameters: parameters)
}

// Brings the prices for the directions between the nearest to the target cities back.
func pricesForAlternativeDirectionsAPI(currency: String = "usd",
                                       origin: String = "",
                                       destination: String = "",
                                       limit: NSInteger = 20,
                                       depart_date: String = "",
                                       return_date: String = "",
                                       flexibility: String = "",
                                       distance: String = "") -> API {
    let url: String = travelPayouts_endpoint + "/v2/prices/nearest-places-matrix"
    let parameters: Parameters = [
        "currency": currency,       // "usd"
        "origin": origin,           // The IATA city code or the country code. The length - from 2 to 3 symbols.
        "destination": destination, // The IATA city code or the country code. The length - from 2 to 3 symbols.
        "limit": limit,             // the number of variants entered, from 1 to 20. Where 1 – is just the variant with the specified points of departure and the points of destination.
        "depart_date": depart_date, // Day or month of departure (yyyy-mm-dd or yyyy-mm).
        "return_date": return_date, // Day or month of return (yyyy-mm-dd or yyyy-mm).
        "flexibility": flexibility, // expansion of the range of dates upward or downward. The value may vary from 0 to 7, where 0 shall show the variants for the dates specified, 7 – all the variants found for a week prior to the specified dates and a week after.
        "distance": distance,       // the number of variants entered, from 1 to 20. Where 1 – is just the variant with the specified points of departure and the points of destination.
    ]
    return API.init(url, parameters: parameters)
}

// Brings the prices for the directions between the nearest to the target cities back.
func priceMapForPricesAPI(
    origin_iata: String,
    period: String = "",
    direct: Bool,
    one_way: Bool,
    no_visa: Bool = true,
    schengen: Bool = true,
    need_visa: Bool = true,
    lang: String,
    min_trip_duration_in_days: NSInteger,
    max_trip_duration_in_days: NSInteger) -> API {
    let url: String = "http://map.aviasales.ru/prices.json"
    let parameters: Parameters = [
        "origin_iata": origin_iata,                             // IATA code
        "period": period,                                       // Period of dates to search for (season, year, month)
        "direct": direct,                                       // Whether non-stop flights are available
        "one_way": one_way,                                     // true: one-way tickets, false: round trips
        "no_visa": no_visa,                                     // Payment not made with a Visa card
        "schengen": schengen,                                   // Can the ticket be changed
        "need_visa": need_visa,                                 // Visa required (or not)
        "locale": lang,                                         // Search language
        "min_trip_duration_in_days": min_trip_duration_in_days, // Shortest trip duration (in days)
        "max_trip_duration_in_days": max_trip_duration_in_days // Longest trip duration (in days)
    ]
    return API.init(url, parameters: parameters)
}

func userLocationAPI() -> API {
    let url = "http://www.travelpayouts.com/whereami"
    let parameters: Parameters = [
        "locale": "en", // en, in, rue, de, fr, it, pl, th
    ]
    return API.init(url, parameters: parameters)
}

func rateOfCurrencyAPI() -> API {
    let url = "http://engine.aviasales.ru/currencies/all_currencies_rates"
    return API.init(url)
}

// Get a list of all airports
func airportsAPI() -> API {
    let url = travelPayouts_endpoint + "/data/airports.json"
    return API.init(url)
}

// Get a list of cities/airports that match the user-entered string.
func citiesAndAirportsAPI(keyword: String) -> API {
    let url = "http://nano.aviasales.ru/places_en" // TODO: set lang later
    let parameters: Parameters = [
        "term": keyword, // keyword for searching airport
    ]
    return API.init(url, parameters: parameters, token: Setting.travelPayouts_token)
}


//
// Amadeus - Inspiration Search API
// https://sandbox.amadeus.com/travel-innovation-sandbox/apis/get/flights/inspiration-search
//

func inspirationSearchAPI() -> API? {

    guard let origin = searchInfo.origin?.airportCode else { return nil }

    let url: String = amadeus_endpoint + "/inspiration-search"

    var parameters: Parameters = [
        "apikey": Setting.amadeus_APIKey,
        "origin": origin
    ]
    parameters["one-way"] = "\(searchInfo.oneWay)"
    if searchInfo.stop == 0 {
        parameters["direct"] = "true"
    } else {
        parameters["direct"] = "false"
    }
    if let budget: NSNumber = searchInfo.budget {
        parameters["max_price"] = budget.stringValue
    }
    return API.init(url, parameters: parameters)
}


//
// Amadeus - Extensive Search API
// https://sandbox.amadeus.com/travel-innovation-sandbox/apis/get/flights/extensive-search
//

func extensiveSearchAPI() -> API? {

    guard let origin = searchInfo.origin?.airportCode,
          let destination = searchInfo.destination?.airportCode else { return nil }

    let url: String = amadeus_endpoint + "/extensive-search"

    // Require queries
    var parameters: Parameters = [
        "apikey"     : Setting.amadeus_APIKey,
        "origin"     : origin,
        "destination": destination,
    ]
    if searchInfo.oneWay {
        parameters["one-way"] = "true"
    } else {
        parameters["one-way"] = "false"
    }
    switch searchInfo.stop {
    case 0:  parameters["direct"] = "true"
    default: parameters["direct"] = "false"
    }
    if let budget: NSNumber = searchInfo.budget {
        parameters["max_price"] = budget.stringValue
    }
    return API.init(url, parameters: parameters)
}


//
// Amadeus - Low Fare Search API
// https://sandbox.amadeus.com/travel-innovation-sandbox/apis/get/flights/low-fare-search
//

func lowFareSearchAPI() -> API? {

    guard let origin = searchInfo.origin?.airportCode,
          let destination = searchInfo.destination?.airportCode,
          let departDate = searchInfo.departDate else { return nil }

    let url: String = amadeus_endpoint + "/low-fare-search"
    
    // Require queries
    var parameters: Parameters = ["apikey"        : Setting.amadeus_APIKey,
                                  "origin"        : origin,
                                  "destination"   : destination,
                                  "departure_date": departDate.convertToString(dateFormat: .YYYY_MM_DD),
                                  "currency"      : searchInfo.currency]
    // Optional queries
    if let _returnDate: Date = searchInfo.returnDate, !searchInfo.oneWay {
        parameters["return_date"] = _returnDate.convertToString(dateFormat: .YYYY_MM_DD)
    }
//    if let _arriveBy: String = searchInfo.arriveBy { // TODO: later
//        parameters["arrive_by"] = _arriveBy
//    }
//    if let _returnBy: String = searchInfo.returnBy { // TODO: later
//        parameters["return_by"] = _returnBy
//    }
    if let _adult : Int = searchInfo.adult  { parameters["adult"]  = _adult }
    if let _child : Int = searchInfo.child  { parameters["child"]  = _child }
    if let _infant: Int = searchInfo.infant { parameters["infant"] = _infant }
//    if let _includeAirlines: String = searchInfo.includeAirlines { parameters["include_airlines"] = include_airlines } // TODO: later
//    if let _excludeAirlines: String = searchInfo.excludeAirlines { parameters["exclude_airlines"] = exclude_airlines }
    if searchInfo.stop == 0 {
        parameters["nonstop"] = "true"
    } else {
        parameters["nonstop"] = "false"
    }
    if let _budget: NSNumber = searchInfo.budget { parameters["max_price"] = _budget.stringValue }
    if let _cabin_class: Int = searchInfo.cabin_class {
        parameters["travel_class"] = { () -> String in
            switch _cabin_class {
            case 0:
                return "ECONOMY"
            case 1:
                return "BUSINESS"
            default:
                return "FIRST"
            }
        }()
    }
    return API.init(url, parameters: parameters)
}


//
// Amadeus - Flight Affiliate Search API
// https://sandbox.amadeus.com/travel-innovation-sandbox/apis/get/flights/affiliate-search
//

func affiliateSearchAPI() -> API? {

    guard let origin = searchInfo.origin?.airportCode,
          let destination = searchInfo.destination?.airportCode,
          let departDate = searchInfo.departDate else { return nil }

    let url: String = amadeus_endpoint + "/affiliate-search"
    
    // Require queries
    var parameters: Parameters = [
        "apikey"        : Setting.amadeus_APIKey,
        "origin"        : origin,
        "destination"   : destination,
        "departure_date": departDate.convertToString(dateFormat: .YYYY_MM_DD),
        "currency"      : searchInfo.currency,
        "mobile"        : "true"
    ]

    // Optional queries
    if let returnDate: Date = searchInfo.returnDate, !searchInfo.oneWay {
        parameters["return_date"] = returnDate.convertToString(dateFormat: .YYYY_MM_DD)
    }
    if let adult : Int = searchInfo.adult  { parameters["adult"] = adult }
    if let child : Int = searchInfo.child  { parameters["child"] = child }
    if let infant: Int = searchInfo.infant { parameters["infant"] = infant }
//    if let _: String = include_airlines { parameters["include_airlines"] = include_airlines }
//    if let _: String = exclude_airlines { parameters["exclude_airlines"] = exclude_airlines }
    if let budget: NSNumber = searchInfo.budget { parameters["max_price"] = budget.stringValue }
    
    return API.init(url, parameters: parameters)
}


//
// Flickr - Photo Search API
// https://www.flickr.com/services/api/flickr.photos.search.html
//

func flickrAPI(keyword: String) -> API {
    let url = "https://api.flickr.com/services/rest/"
    let parameters: Parameters = [
        "method"        : "flickr.photos.search",
        "api_key"       : Setting.flickr_APIKey,
        "text"          : keyword,
        "sort"          : "relevance", //"interestingness-desc",
        "per_page"      : "1",
        "format"        : "json",
        "nojsoncallback": "1"
    ]
    return API.init(url, parameters: parameters)
}
