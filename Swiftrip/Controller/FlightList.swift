//
//  FlightList.swift
//  Swiftrip
//
//  Created by maki on 2017-04-22.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
//import FirebaseDatabase
import CoreLocation
import Alamofire
import AlamofireImage
import SwiftyJSON

/* Scenario ------------------------------
 
 1. setView()
 
 2. handleFetch()
 
 3. fetch userLocationAPI()
 
 4. fetch CheapestTicketsAPI()
 - photoOfCity()
 
 5. fetchRateOfCurrency()
 
 6. initTickets()
 
 7. segmentTickets()
 
 8. showResult()
 
 ----------------------------------------- */

class FlightList: UICollectionViewController, UICollectionViewDelegateFlowLayout {

//    static let ref = FIRDatabase.database().reference()
    static let sectionHeaderID: String  = "sectionHeaderID"
    static let sectionFooterID: String  = "sectionFooterID"
    static let userDefaultKey_airports: String = "airports"
    
    private var airports: [AirportOfUserDefaults] = []
    private var tickets: [Ticket] = []
    private var cellShown: [Bool] = []
    public var selectedCell: IndexPath?
    var searchInfo: SearchInfo!
    let userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Search Flights"
        self.navigationController?.hidesBarsOnSwipe = true
        
        self.getUserDefaults()
        self.setupSearchInfo()
//        self.addBackButton()
        self.setupView()
        self.handleFetch()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("didRecieveMemoryWorning")
    }
    
    func getUserDefaults() {
        
        // remove all userDefault data
        // UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        
        if let data = UserDefaults.standard.data(forKey: FlightList.userDefaultKey_airports),
           let airportsOfUserDefault: [AirportOfUserDefaults] = NSKeyedUnarchiver.unarchiveObject(with: data) as? [AirportOfUserDefaults] {
            airports = airportsOfUserDefault
        } else {
            print("UserDefaults doesn't have data")
        }
    }
    
    func setupSearchInfo() {
        let departDate: Date? = Date(timeInterval: 60*60*24*60, since: Date())
        let returnDate: Date? = Date(timeInterval: 60*60*24*70, since: Date())
        var oneWay: Bool? // for inspirationSearch
        if returnDate != nil {
            oneWay = false
        } else {
            if oneWay == nil {
                oneWay = true
            } else if oneWay == true {
                oneWay = true
            } else {
                oneWay = false
            }
        }
        searchInfo = SearchInfo(
            origin: "YVR",
            destination: nil, //"HND",
            destinationCity: nil,
            departDate: departDate, //nil,
            returnDate: returnDate,
            stop: 1,
            oneWay: oneWay!,
            lang: "en",
            adult: 1,
            child: 0,
            infant: 0,
            trip_class: 0,
            minTripDuration: nil,
            maxTripDuration: nil,
            budget: 1900,
            currency: "usd",
            location: nil)
    }
    
    func setupView() {
        
        // Set gradient layer
//        let gradientTop    = UIColor(r: 172, g: 203, b: 238, a: 1)
//        let gradientBottom = UIColor(r: 231, g: 240, b: 253, a: 1)
//        let gradientColors: [CGColor] = [gradientTop.cgColor, gradientBottom.cgColor]
//        let gradientLayer: CAGradientLayer = CAGradientLayer()
//        gradientLayer.colors = gradientColors
//        gradientLayer.frame = self.view.bounds
//        self.view.layer.insertSublayer(gradientLayer, at: 0)
        self.view.backgroundColor = UIColor(r: 240, g: 240, b: 240, a: 1)
        
        // Set collectionView
        if let cv = self.collectionView {
            cv.backgroundColor = UIColor.clear
            cv.register(TicketCell.self, forCellWithReuseIdentifier: TicketCell.cellId)
            cv.register(TicketCellHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: FlightList.sectionHeaderID)
            cv.register(TicketCellHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: FlightList.sectionFooterID)
            
            if let flowLayout = cv.collectionViewLayout as? UICollectionViewFlowLayout {
                flowLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
                cv.collectionViewLayout = flowLayout
            }
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    // Number of secion
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        if searchInfo?.destination == nil && tickets.count > 0 { // inspirationSearchAPI
            return tickets.count
        } else if tickets.count > 0 { // lowFareSearchAPI and affiliateSearchAPI
            return 1
        } else { // No result
            return 0
        }
    }
    
    // Number of items
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchInfo?.destination == nil {
            return 1
        } else {
            return tickets.count
        }
    }
    
    // Cell
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var target: Int
        var withFlightDetail: Bool
        
        switch tickets[indexPath.section].apiName {
        case API_NAME.flightInspirationSearchTickets:
            target = indexPath.section
            withFlightDetail = false
            
        case API_NAME.lowFareSearchTickets,
             API_NAME.affiliateSearchTickets:
            target = indexPath.row
            withFlightDetail = true
        }
        
        let cell: TicketCell = collectionView.dequeueReusableCell(withReuseIdentifier: TicketCell.cellId, for: indexPath) as! TicketCell
            cell.setupCell(ticket: tickets[target], withFlightDetail: withFlightDetail)
            
            // z-position
            cell.layer.zPosition = CGFloat(indexPath.row)
            cell.layer.backgroundColor = UIColor.white.cgColor
            
            // set action of button
            cell.btn_booking.tag = target
            cell.btn_booking.addTarget(self, action: #selector(self.bookingClicked(_:)), for: .touchUpInside)
            cell.btn_share.addTarget(self, action: #selector(self.shareClicked(_:)), for: .touchUpInside)
        
//        for subview in cell.contentView.subviews {
//            subview.removeFromSuperview()
//        }
        return cell
    }
    
//    func showDetail(selectedTicket: Ticket) {
//        let detail: FlightDetail = FlightDetail()
//        detail.ticket = selectedTicket
//        navigationController?.pushViewController(detail, animated: true)
//    }
    
    // Space around section
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets { return UIEdgeInsetsMake(0.0, 16.0, 16.0, 16.0) }
    
    // Space between rows of cells
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat { return 1 }
    
    // Space between columns of cells
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat { return 0 }
    
    // CGSize of collection view cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = self.view.frame.width - 32
        var height: CGFloat = 200
        
        switch tickets[indexPath.section].apiName {
            
        case API_NAME.flightInspirationSearchTickets:
            
            if (searchInfo?.oneWay)! { // Oneway simple
                height = 200
                
            } else { // Round simple
                height = 310
            }
            
        default: // lowFareSearchTickets, affiliateSearchTickets
            
            for itinerary in tickets[indexPath.row].itineraries {
                
                let outboundFlights: [Flight] = itinerary.outbound.flights
                    
                switch outboundFlights.count {
                case 1:  height = 260
                case 2:  height = 260 + 100
                default: height = 260 + 100 + 100
                }
                
                if let inboundnFlights: [Flight] = itinerary.inbound?.flights {
                    
                    switch inboundnFlights.count {
                    case 1:  height += 180
                    case 2:  height += 180 + 100
                    default: height += 180 + 100 + 100
                    }
                }
            }
        }
        print(height)
        return CGSize(width: width, height: height)
    }
    
    // Header & Footer
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        // Header
        if kind == UICollectionElementKindSectionHeader {
            let header: TicketCellHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: FlightList.sectionHeaderID, for: indexPath) as! TicketCellHeader
            
            if tickets.count > 0 {
                
                header.lab_city.text = { () -> String in
                    
                    let flights = tickets[indexPath.section].itineraries[0].outbound.flights
                    if let text = flights[flights.count - 1].destination.placeName {
                        return text
                        
                    } else { return "" }
                }()
                
                if let url = tickets[indexPath.section].img {
                    header.imgView.af_setImage(withURL: url, imageTransition: .crossDissolve(0.2)) // using Alamofire image framework
                }
            }
            
            header.bringSubview(toFront: header.lab_city)
            header.layer.zPosition = CGFloat(-1)
            return header
            
        // Footer
        } else {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: FlightList.sectionFooterID, for: indexPath)
            footer.backgroundColor = .white
            return footer
        }
    }
    
    // Header
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 180)
    }
    
    // Footer
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
    //        return CGSize(width: view.frame.width, height: 0)
    //    }
    
    @objc private func shareClicked(_ sender: AnyObject) {
        
        sender.setTitleColor(Utility.color_primary, for: UIControlState.normal)
        
        let alert: UIAlertController = UIAlertController(title: "Share this ticket", message: "Which SNS do you want to share?", preferredStyle:  UIAlertControllerStyle.actionSheet)
        
        // TODO: Set SNS share link
        let action_google: UIAlertAction = UIAlertAction(title: "Google+", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) -> Void in
            print("Google+")
        })
        
        let action_twitter: UIAlertAction = UIAlertAction(title: "Twitter", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) -> Void in
            print("Twitter")
        })
        
        let action_facebook: UIAlertAction = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) -> Void in
            print("Facebook")
        })
        
        let action_cancel: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {
            (action: UIAlertAction!) -> Void in
            print("Cancel")
        })
        
        alert.addAction(action_facebook)
        alert.addAction(action_google)
        alert.addAction(action_twitter)
        alert.addAction(action_cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func bookingClicked(_ sender: AnyObject) {
        guard let deeplink: String = tickets[sender.tag].deep_link else { return }
        guard let url: URL = URL(string: deeplink) else { return }
        UIApplication.shared.open(url)
    }
    
    
// MARK: - private methods
    
    private func returnAirportByAirportCodeFromUserDefaults(airportCode: String) -> AirportOfUserDefaults? {
        
        var matchedAirport: AirportOfUserDefaults? = nil
        for airport in self.airports {
            if airport.airportCode == airportCode {
                matchedAirport = airport
                break
            }
        }
        return matchedAirport
    }
    
    func handleFetch() {
        
        // Get user's location
        func fetchUserLocation() {
            fetchAPI(executeApi: userLocationAPI(), completion: { (json: JSON) -> Void in
                
                print(json)
                
                // set searchInfo
                let airportCode: String = json["iata"].string!
                self.searchInfo?.location = UserLocation(airportCode: airportCode,
                                                         airportName: nil,
                                                         cityName: json["name"].string!,
                                                         countryName: json["country_name"].string!,
                                                         coordinates: json["coordinates"].string!)
                
                // get airportName of user's location by airportCode
                // check user default
                if let airport = self.returnAirportByAirportCodeFromUserDefaults(airportCode: airportCode) {
                    self.searchInfo?.location?.airportName = airport.airportName
                    fetchTickets()
                    
                } else { // fetch API
                    AirportOfUserDefaults.fetchAirportAPIFromAirportCode(airportCode: (self.searchInfo?.location?.airportCode)!, completion: { airport -> Void in
                        self.searchInfo?.location?.airportName = airport.airportName
                        fetchTickets()
                        
                        // save to UserDefaults (before saving, check UserDefaults again because there is time lag)
                        if self.returnAirportByAirportCodeFromUserDefaults(airportCode: airport.airportCode) == nil {
                            self.airports.append(airport)
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.airports)
                            UserDefaults.standard.set(encodedData, forKey: FlightList.userDefaultKey_airports)
                        }
                    })
                }
            })
        }
        
        // Get JSON of tickets by using user's location
        func fetchTickets() {
            
            // clear
            self.tickets = []
            // self.cellShown = []
            
            let hasInbound: Bool = {
                if (searchInfo?.oneWay)! {
                    return false
                } else {
                    return true
                }
            }()
            
            let nonstop: Bool? = {
                if let stop = self.searchInfo?.stop {
                    if stop == 0 {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return nil
                }
            }()
            
            let returnDate: String? = {
                if searchInfo!.oneWay {
                    return nil
                } else {
                    return self.searchInfo!.returnDate!.convertToString(dateFormat: .YYYY_MM_DD)
                }
            }()
            
            // Request to "LowFareSearchAPI" & "affiliateSearchAPI" of Amadeus
            if let destinationCode: String = self.searchInfo?.destination {
                
                let group: DispatchGroup = DispatchGroup()
                let queue1 = DispatchQueue.global(qos: .default),
                    queue2 = DispatchQueue.global(qos: .default),
                    queue3 = DispatchQueue.global(qos: .default)
                
                group.enter()
                queue1.async(group: group) {
                    print("get lowFareSearchAPI")
                    fetchAPI(executeApi: lowFareSearchAPI(
                        origin: self.searchInfo!.origin!,
                        destination: destinationCode,
                        departure_date: self.searchInfo!.departDate!.convertToString(dateFormat: .YYYY_MM_DD),
                        return_date: returnDate,
                        arrive_by: nil, // TODO: set arrive_by later
                        return_by: nil,
                        adult:  "\(self.searchInfo!.adult!)",
                        child:  "\(self.searchInfo!.child!)",
                        infant: "\(self.searchInfo!.infant!)",
                        include_airlines: nil,
                        exclude_airlines: nil,
                        nonstop: nonstop,
                        max_price: self.searchInfo!.budget,
                        currency: self.searchInfo!.currency,
                        travel_class: "\(self.searchInfo!.trip_class!)"),
                             completion: { json -> Void in
                                self.initTickets(json, API_NAME.lowFareSearchTickets, hasInbound: hasInbound)
                                group.leave()
                    })
                }
                
                group.enter()
                queue2.async(group: group) {
                    print("get affiliateSearchAPI")
                    fetchAPI(executeApi: affiliateSearchAPI(
                        origin: (self.searchInfo?.origin!)!,
                        destination: destinationCode,
                        departure_date: (self.searchInfo!.departDate?.convertToString(dateFormat: .YYYY_MM_DD))!,
                        return_date: returnDate,
                        adult:  "\(self.searchInfo!.adult!)",
                        child:  "\(self.searchInfo!.child!)",
                        infant: "\(self.searchInfo!.infant!)",
                        include_airlines: nil,
                        exclude_airlines: nil,
                        max_price: self.searchInfo?.budget,
                        currency: (self.searchInfo?.currency)!),
                             completion: { json -> Void in
                                self.initTickets(json, API_NAME.affiliateSearchTickets, hasInbound: hasInbound)
                                group.leave()
                    })
                }
                
                group.enter()
                var destinationPhotoURL: URL?
                var destinationPlaceName: String?
                queue3.async(group: group) { // get photo of a destination (always only one destination)
                    print("get photo")
                    
                    if let airport = self.returnAirportByAirportCodeFromUserDefaults(airportCode: destinationCode) {
                        destinationPhotoURL = airport.photoURL
                        destinationPlaceName = airport.placeName
                        group.leave()
                        
                    } else {
                        
                        // Request airportInfo
                        AirportOfUserDefaults.fetchAirportAPIFromAirportCode(airportCode: destinationCode, completion: { airport -> Void in
                            
                            // search photoURL of city
                            AirportOfUserDefaults.photoOfCity(cityName: airport.cityName, completion: { photoURL in
                                
                                // save to UserDefaults (before saving, check UserDefaults again because there is time lag)
                                if self.returnAirportByAirportCodeFromUserDefaults(airportCode: airport.airportCode) == nil {
                                    destinationPhotoURL = photoURL
                                    destinationPlaceName = airport.placeName
                                    airport.photoURL = photoURL
                                    self.airports.append(airport)
                                    let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.airports)
                                    UserDefaults.standard.set(encodedData, forKey: FlightList.userDefaultKey_airports)
                                }
                                group.leave()
                            })
                        })
                    }
                }
                
                group.notify(queue: DispatchQueue.main) {
                    // set a photo
                    if let _ : URL = destinationPhotoURL {
                        self.tickets[0].img = destinationPhotoURL
                        let flights = self.tickets[0].itineraries[0].outbound.flights
                        flights[flights.count - 1].destination.placeName = destinationPlaceName
                    }
                    
                    // sort
                    self.tickets.sort { ($0.fare?.total_price as! Double) < ($1.fare?.total_price as! Double) }
                    
                    // show a number of tickets
                    self.setAlert(count: self.tickets.count)
                    
                    // reload
                    self.collectionView?.reloadData()
                    print("reloaded collectionView")
                    
                    setAirportAdditionalInfo(hasInbound: hasInbound)
                }
                
                func setAirportAdditionalInfo(hasInbound: Bool) {
                    
                    /*
                     show airport name, photo after showing cells
                    */
                    
                    let group: DispatchGroup = DispatchGroup()
                    let queue1 = DispatchQueue.global(qos: .default)
                    
                    func setAirportNameToTicketAndSaveToUserDefault(airport: AirportOfUserDefaults, ticketAirport: Airport) {
                        
                        ticketAirport.name = airport.airportName
                        ticketAirport.placeName = airport.placeName
                        
                        // save to UserDefaults (before saving, check UserDefaults again because there is time lag)
                        if self.returnAirportByAirportCodeFromUserDefaults(airportCode: airport.airportCode) == nil {
                            self.airports.append(airport)
                            let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.airports)
                            UserDefaults.standard.set(encodedData, forKey: FlightList.userDefaultKey_airports)
                        }
                    }
                    
                    // outbound
                    for ticket in self.tickets {
                        
                        for itinerary in ticket.itineraries {
                            
                            for flight in itinerary.outbound.flights {
                                
                                // checking the airport name in UserDefaults
                                if let airport = self.returnAirportByAirportCodeFromUserDefaults(airportCode: flight.destination.code) {
                                    flight.destination.name = airport.airportName
                                    print("read: \(String(describing: airport.airportName))")
                                    
                                } else { // if it doesn't exist in UserDefaults, fetching an API of airportName
                                    
                                    AirportOfUserDefaults.fetchAirportAPIFromAirportCode(airportCode: flight.destination.code, completion: { airport -> Void in
                                        
                                        setAirportNameToTicketAndSaveToUserDefault(airport: airport, ticketAirport: flight.destination)
                                    })
                                }
                            }
                        }
                    }
                    
                    // inbound
                    if hasInbound {
                        
                        for ticket in self.tickets {
                            
                            for itinerary in ticket.itineraries {
                                
                                for flight in itinerary.inbound!.flights {
                                    
                                    
                                    // search from userDefaults
                                    if let airport = returnAirportByAirportCodeFromUserDefaults(airportCode: flight.origin.code) {
                                        flight.origin.name = airport.airportName
                                        flight.origin.placeName = airport.placeName
                                        
                                    // search from API
                                    } else {
                                        AirportOfUserDefaults.fetchAirportAPIFromAirportCode(airportCode: flight.origin.code, completion: { airport -> Void in
                                            
                                            setAirportNameToTicketAndSaveToUserDefault(airport: airport, ticketAirport: flight.origin)
                                        })
                                    }
                                    
                                    if let airport = returnAirportByAirportCodeFromUserDefaults(airportCode: flight.destination.code) {
                                        flight.destination.name = airport.airportName
                                        flight.destination.placeName = airport.placeName
                                        
                                    } else {
                                        AirportOfUserDefaults.fetchAirportAPIFromAirportCode(airportCode: flight.destination.code, completion: {airport -> Void in
                                            
                                            setAirportNameToTicketAndSaveToUserDefault(airport: airport, ticketAirport: flight.destination)
                                        })
                                    }
                                }
                            }
                        }
                    }
                    
                    self.collectionView?.reloadData()
                }
                
            } else { // flightInspirationSearchAPI of Amadeus
                
                fetchAPI(executeApi: flightInspirationSearchAPI(origin: searchInfo!.origin!, oneway: searchInfo!.oneWay, nonstop: nonstop),
                         completion: { json -> Void in
                            self.initTickets(json, API_NAME.flightInspirationSearchTickets, hasInbound: hasInbound)
                })
                
                // Request to "CheapestSearchAPI" of TravelPayouts
                //                fetchAPI(executeApi: CheapestTicketsAPI(currency: searchInfo.currency, origin: searchInfo.origin!, destination: nil, depart_date: searchInfo.departDate, return_date: searchInfo.returnDate),
                //                         completion: { json -> Void in
                //                            self.initTickets(json, API_NAME.flightInspirationSearchTickets)
                //                })
            }
        }
        
        // TODO: Reload location when use hasn't access for a while
        if searchInfo?.location == nil {
            fetchUserLocation()
            
        } else {
            fetchTickets()
        }
    }
    
    private func initTickets(_ json: JSON, _ apiName: API_NAME, hasInbound: Bool) {
//        print(json)
        
        print("\(apiName) starts initTickets()")
        
        switch apiName {
        case API_NAME.flightInspirationSearchTickets:
            
            // show ticket cell without city name and photo
            var arrDestinationCode: [String] = [String]()
            let originAirportName: String = (self.searchInfo?.location?.airportName)!
            
            for result in json["results"].array! {
                
                // Create array of destination code to request Flickr photos after showing tickes data on view
                arrDestinationCode.append(result["destination"].string!)
                
                // Request tickets
                let ticket: Ticket = initTicketForNoDestination(result, currency: json["currency"].string!, hasInbound: hasInbound)
                
                // set airportName
                ticket.itineraries[0].outbound.flights[0].origin.name      = originAirportName
                ticket.itineraries[0].inbound?.flights[0].destination.name = originAirportName
                
                self.tickets.append(ticket)
            }
            
            // show airport name, place name, photo ===========================================
            
            func setAirportToTickets(index: Int, airport: AirportOfUserDefaults) {
                self.tickets[index].itineraries[0].outbound.flights[0].destination.name      = airport.airportName
                self.tickets[index].itineraries[0].inbound?.flights[0].origin.name           = airport.airportName
                self.tickets[index].itineraries[0].outbound.flights[0].destination.placeName = airport.placeName
                self.tickets[index].itineraries[0].inbound?.flights[0].origin.placeName      = airport.placeName
                self.tickets[index].img                                                      = airport.photoURL
            }
            
            let group:  DispatchGroup = DispatchGroup()
            let group2: DispatchGroup = DispatchGroup()
            let queue1 = DispatchQueue.global(qos: .default)
            let count: Int = 1 // count of showing cell at first reload
            for _ in 0..<count {
                group.enter()
            }
            group2.enter()
            
            for (index, destinationCode) in arrDestinationCode.enumerated() {
                
                queue1.async(group: group) {
                    
                    // check UserDefaults
                    if let matchedAirport: AirportOfUserDefaults = self.returnAirportByAirportCodeFromUserDefaults(airportCode: destinationCode) {
                        
                        setAirportToTickets(index: index, airport: matchedAirport)
                        print("load from local: \(String(describing: matchedAirport.airportName))")
                        
                        if index < count { group.leave() }
                        if index == arrDestinationCode.count - 1 { group2.leave() }
                        
                    // request placeName, photoURL
                    } else {
                        
                        AirportOfUserDefaults.fetchAirportAPIFromAirportCode(airportCode: destinationCode, completion: { airport -> Void in
                            
                            getPhotoURL(airportOfUserDefaults: airport, ticketIndex: index, countTickets: arrDestinationCode.count)
                        })
                    }
                }
            }
            
            func getPhotoURL(airportOfUserDefaults: AirportOfUserDefaults, ticketIndex: Int, countTickets: Int) {
                
                // search photo of cityName
                let (cityName, _)  = Airport.cityNameFromCityAndContry(cityAndCountry: airportOfUserDefaults.placeName)
                
                AirportOfUserDefaults.photoOfCity(cityName: cityName, completion: { photoURL in
                    
                    // save to UserDefaults (before saving, check UserDefaults again because there is time lag)
                    if self.returnAirportByAirportCodeFromUserDefaults(airportCode: airportOfUserDefaults.airportCode)?.photoURL == nil {
                        print(photoURL)
                        
                        airportOfUserDefaults.photoURL = photoURL
                        self.airports.append(airportOfUserDefaults)
                        
                        setAirportToTickets(index: ticketIndex, airport: airportOfUserDefaults)
                        
                        if ticketIndex < count { group.leave() }
                        if ticketIndex == countTickets - 1 { group2.leave() }
                    }
                })
            }
            
            group.notify(queue: DispatchQueue.main) {
                self.setAlert(count: self.tickets.count)
                self.collectionView?.reloadData()
            }
            
            group2.notify(queue: DispatchQueue.global()) {
                let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.airports)
                UserDefaults.standard.set(encodedData, forKey: FlightList.userDefaultKey_airports)
            }
            
            
        case API_NAME.lowFareSearchTickets,
             API_NAME.affiliateSearchTickets:
            
            /*
             In case of lowFareSearchAPI and affiliateSearchAPI,
             the destination has been already decided, so get just one image from Flickr before excuting initTicket().
             But each flight has multiple airportNames. so use for loop.
            */
            
            for value in json["results"].array! {
                
                if apiName == API_NAME.affiliateSearchTickets && searchInfo?.stop == 0 {
                    if value["outbound"]["flights"].array?.count != 1 {
                        continue // skip multiple flights if user selected direct flight
                    }
                }
                
                let ticket: Ticket = self.initTicket(value: value, apiName: apiName, hasInbound: hasInbound)
                tickets.append(ticket)
                
                // TODO: cell shown
                // self.cellShown.append(false)
            }
        }
    }
    
    private func initTicket(value: JSON, apiName: API_NAME, hasInbound: Bool) -> Ticket {
        
        return Ticket(
            apiName: apiName,
            request_id: value["request_id"].string,
            currency: searchInfo?.currency,
            itineraries: initItinerary(value: value, hasInbound: hasInbound),
            deep_link: initDeepLink(value),
            merchant: value["merchant"].string,
            travel_class: value["travel_class"].string?.lowercased(),
            fare_family: value["fare_family"].string,
            cabin_code: value["cabin_code"].string,
            fare: Fare(price_per_adult: self.initPrice(value["fare"], "price_per_adult"),
                       price_per_child: self.initPrice(value["fare"], "price_per_child"),
                       price_per_infant: self.initPrice(value["fare"], "price_per_infant"),
                       currency: value["fare"]["currency"],
                       total_price: Double(value["fare"]["total_price"].string!),
                       service_fees: value["fare"]["service_fees"], // TODO: set Double() for lowFareSearch
                       creditcard_fees: value["fare"]["creditcard_fees"]), // TODO: set Double() for lowFareSearch
            airline: value["airline"].string,
            img: nil)
    }
    
    private func initDeepLink(_ json: JSON) -> String {
        
        // affiliateSearchAPI has deeplink
        if let deeplink = json["deep_link"].string {
            return deeplink
            
        // lowFareSearchAPI needs generating deeplink
        } else {
            var param : [String] = []
            param.append("marker=\(Config.marker)")
            if let origin_iata: String = searchInfo?.origin {
                param.append("origin_iata=\(origin_iata)")
            }
            if let destination_iata: String = searchInfo?.destination {
                param.append("destination_iata=\(destination_iata)")
            }
            if let depart_date: Date = searchInfo?.departDate { // ex. 2016-12-01
                param.append("depart_date=\(depart_date)")
            }
            if let return_date: Date = searchInfo?.returnDate { // ex. 2016-12-15
                param.append("return_date=\(return_date)")
            }
            if let oneway: Bool = searchInfo?.oneWay {
                if oneway {
                    param.append("oneway=1")
                } else {
                    param.append("oneway=0")
                }
            }
            if let adults: Int = searchInfo?.adult {
                param.append("adults=\(adults)")
            }
            if let children: Int = searchInfo?.child {
                param.append("children=\(children)")
            }
            if let infants: Int = searchInfo?.infant {
                param.append("infants=\(infants)")
            }
            if let trip_class: Int = searchInfo?.trip_class {
                param.append("trip_class=\(trip_class)") // Economy: 0, Business: 1, First: 2
            }
            // Initiate search automatically (true: the search is loaded, false: form is filled out but search does not start)
            param.append("with_request=false")
            let deeplink: String = "http://jetradar.com/searches/new?\(param.joined(separator: "&"))"
            return deeplink
        }
    }
    
    private func initPrice(_ json: JSON, _ passenger: String) -> Price? {
        var price: Price = Price()
        if let tax: String = json[passenger]["tax"].string,
           let total: String = json[passenger]["total_fare"].string {
            price = Price(tax: tax, total_price: total)
        }
        return price
    }
    
    private func initItinerary(value: JSON, hasInbound: Bool) -> [Itinerary] {
        var itineraries: [Itinerary] = []
        
        // MEMO: count of itineraries is usualy only 1
        
        // LowFareSesarchAPI
        if let itinerariesJSON: [JSON] = value["itineraries"].array {
            
            for itinerary in itinerariesJSON {
                let outboundFlights: [Flight] = self.initFlights(itinerary["outbound"]["flights"].array!)
                let outbound: Bound = Bound(flights: outboundFlights, duration: nil)
                var inbound: Bound? = nil
                
                // round-trip
                if hasInbound {
                    inbound = Bound(flights: self.initFlights(itinerary["inbound"]["flights"].array!), duration: nil)
                }
                itineraries.append(Itinerary(outbound: outbound, inbound: inbound))
            }
            
        // affiliateSearchAPI
        } else {
            let outboundFlights: [Flight] = self.initFlights(value["outbound"]["flights"].array!)
            let outbound: Bound = Bound(flights: outboundFlights, duration: value["outbound"]["duration"].string!)
            var inbound: Bound? = nil
            
            // round-trip
            if hasInbound {
                inbound = Bound(flights: self.initFlights(value["inbound"]["flights"].array!), duration: value["inbound"]["duration"].string!)
            }
            itineraries.append(Itinerary(outbound: outbound, inbound: inbound))
        }
        return itineraries
    }
    
    private func initFlights(_ json: [JSON]) -> [Flight] {
        
        var flights: [Flight] = [Flight]()
        for flightValue: JSON in json {
            
            flights.append(Flight(origin: Airport(code: flightValue["origin"]["airport"].string!,
                                                  name: { () -> String? in
                                                    if flights.count == 0 {
                                                        if let airportName = searchInfo?.location?.airportName {
                                                            return airportName
                                                        } else {
                                                            return nil
                                                        }
                                                    } else {
                                                        return nil
                                                    }}(),
                                                  location: nil,
                                                  placeName: nil,
                                                  country_code: nil,
                                                  country: nil,
                                                  city_code: nil,
                                                  city: nil,
                                                  time_zone: nil,
                                                  terminal: nil),
                                  destination: Airport(code: flightValue["destination"]["airport"].string!,
                                                       name: nil,
                                                       location: nil,
                                                       placeName: nil,
                                                       country_code: nil,
                                                       country: nil,
                                                       city_code: nil,
                                                       city: nil,
                                                       time_zone: nil,
                                                       terminal: flightValue["destination"]["terminal"].string),
                                  departs_at: DateUtils.dateFromStringWithTime(string: flightValue["departs_at"].string!),
                                  arrives_at: DateUtils.dateFromStringWithTime(string: flightValue["arrives_at"].string!),
                                  booking_info: initBookingInfo(flightValue: flightValue),
                                  operating_airline: flightValue["operating_airline"].string!,
                                  marketing_airline: flightValue["marketing_airline"].string!,
                                  aircraft: flightValue["aircraft"].string!,
                                  flight_number: flightValue["flight_number"].string!))
        }
        return flights
    }
    
    private func initBookingInfo(flightValue: JSON) -> BookingInfo {
        
        return BookingInfo(seats_remaining: { () -> Int? in
            guard let value: Int = flightValue["booking_info"]["seats_remaining"].int else { return nil }
            return value }(),
                           fare_basis: { () -> String? in
                            guard let value: String = flightValue["booking_info"]["fare_basis"].string else { return nil }
                            return value }(),
                           booking_code: { () -> String? in
                            guard let value = flightValue["booking_info"]["booking_code"].string else { return nil }
                            return value }(),
                           cabin_code: { () -> String? in
                            guard let value = flightValue["booking_info"]["cabin_code"].string else { return nil }
                            return value }(),
                           travel_class: { () -> String? in
                            guard let value = flightValue["booking_info"]["travel_class"].string?.lowercased() else { return nil }
                            return value }(),
                           fare_family: { () -> String? in
                            guard let value = flightValue["booking_info"]["fare_family"].string else { return nil }
                            return value }()
        )
    }
    
    // determine the rate of user's currency
    private func fetchRateOfCurrency(completion: ((_ rate: Double?) -> Void)?) {
        fetchAPI(executeApi: rateOfCurrencyAPI(), completion: { (json: JSON) -> Void in
            if let rate: Double = json[(self.searchInfo?.currency)!].double {
                completion! ( rate )
            }
        })
    }
    
    func setAlert(count: Int) {
        let dialog: UIAlertController = UIAlertController(title: "\(count) results", message: nil, preferredStyle: .actionSheet)
        self.present(dialog, animated: true) { () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
