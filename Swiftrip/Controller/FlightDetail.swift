//
//  FlightDetail.swift
//  Swiftrip
//
//  Created by maki on 2017-04-22.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class FlightDetail: UIViewController {
    
    var ticket: Ticket? {
        didSet {
            navigationItem.title = ticket?.itineraries[0].outbound.flights[0].destination.name
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Utility.color_primary
        
        depatureAirportLabel.text = ticket?.itineraries[0].outbound.flights[0].origin.code
        arrivalAirportLabel.text = ticket?.itineraries[0].outbound.flights[0].destination.code
        
        setupViews()
    }
    
    func setupViews() {
        self.view.addSubview(logoImage)
        self.view.addSubview(fromLabel)
        self.view.addSubview(toLabel)
        self.view.addSubview(depatureAirportLabel)
        self.view.addSubview(arrivalAirportLabel)
    }
    
    let fromLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 50, y: 120, width: 70, height: 40))
        label.text = "from"
        label.textColor = .gray
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let toLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 240, y: 120, width: 70, height: 40))
        label.text = "to"
        label.textColor = .gray
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let depatureAirportLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 50, y: 150, width: 100, height: 60))
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 40)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let arrivalAirportLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 240, y: 150, width: 100, height: 60))
        label.textColor = .white
        label.textAlignment = .left
        label.font = UIFont(name: label.font.fontName, size: 40)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let logoImage: UIImageView = {
        let image = UIImageView(frame: CGRect(x: 10, y: 10, width: 70, height: 70))
        image.backgroundColor = UIColor.blue
        image.image = UIImage(named: "taylor_swift_blank_space")
        return image
    }()
}
