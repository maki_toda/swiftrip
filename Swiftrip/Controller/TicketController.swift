//
//  TicketController.swift
//  Swiftrip
//
//  Created by maki on 2017-05-23.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON

// common variables
var ticketController: TicketController! // be defined in viewDidLoad()

/*
 ┌── SearchFlight ─┐ - TableViewController
 │┌────────┐│
 ││   FilterCell   ││ - row 0 : FilterCell
 │├────────┤│
 ││   ResultCell   ││ - row 1 : ResultCell
 ││(CollectionView)││
 ││┌──────┐││
 │││            │││
 ││├──────┤││
 │││            │││
 ││├──────┤││
 │││            │││
 ││└──────┘││
 │└────────┘│
 └──────────┘
 */

class TicketController: UITableViewController {
    
    static let filterCellID: String = "FilterCellID"
    static let resultCellID: String = "ResultCellID"
    static let showAlertDialog: String = "showAlertDialog"
    fileprivate var filterCellHeight: CGFloat = 400  // 460 // default fliter pane is shrinked
    fileprivate var resultCellHeight: CGFloat!
    static let filterCellExpandHeight: CGFloat = 400 // 460
    static let filterCellShrinkHeight: CGFloat = 80

    public let indicator: Indicator = Indicator()
    public var filterCell: FilterCell!
    public var resultCell: ResultCell!
    var voiceSearchView: VoiceSearchCollectionView!

    enum TableCell: Int {
        case Filter = 0
        case Result = 1
    }
    
    fileprivate func getCell(_ indexPath: IndexPath) -> TableCell {
        switch indexPath.row {
        case 0:  return .Filter
        default: return .Result
        }
    }

    var expandedCell: TableCell = .Filter {
        willSet {
            switch newValue {
            case .Filter:
                print("Filter cell clicked")
                if expandedCell != .Filter {    // if searchPane hasn't been selected
                    tableView.beginUpdates()    // update to cell height
                    filterCellHeight = TicketController.filterCellExpandHeight
                    tableView.endUpdates()
                    filterCell.layoutIfNeeded()
                }

            case .Result:
                print("Result cell clicked")
                if expandedCell != .Result {    // if resultPane hasn't been selected
                    tableView.beginUpdates()    // update to cell height
                    filterCellHeight = TicketController.filterCellShrinkHeight
                    tableView.endUpdates()
                    resultCell.layoutIfNeeded()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .primary

        ticketController = self

        if !skipIntroduction {
            self.showIntoroduction()
        }
        
        // micButton on navigationBar
        let micButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_mic"), style: .plain, target: self, action: #selector(showVoiceSearchView))
        self.navigationItem.rightBarButtonItem = micButton

//        self.setupSidebar()
        self.setupTableView()

        // init VoiceSearchView
        self.voiceSearchView = VoiceSearchCollectionView()
        self.navigationController?.view.addSubview(self.voiceSearchView)
    }

    override func viewWillAppear(_ animated: Bool) {
        // MEMO: keep this method and do not call super.viewWillAppear
        //       to avoid scrolling when textfields are focused in filter.
    }

    fileprivate func setupTableView() {
        self.tableView.delegate   = self
        self.tableView.dataSource = self
        self.tableView.register(FilterCell.self, forCellReuseIdentifier: TicketController.filterCellID)
        self.tableView.register(ResultCell.self, forCellReuseIdentifier: TicketController.resultCellID)
        self.tableView.alwaysBounceVertical = false
        self.tableView.separatorStyle = .none

        // add indicator to tableView
        self.tableView.addSubview(self.indicator)

        self.indicator.frame = CGRect(x: (DeviceSize.screenWidth() - self.indicator.frame.width) / 2,
                                      y: (DeviceSize.screenHeight() - self.indicator.frame.height) / 2 - DeviceSize.statusBarHeight - DeviceSize.navBarHeight,
                                      width: self.indicator.frame.width, height: self.indicator.frame.height)
    }
    
    // MARK: - tableView delegate -----------------------------------------------------
    
    // tableView section
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // tableView row
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    // size of cell
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        switch self.getCell(indexPath) {
        case TableCell.Filter:
            return self.filterCellHeight

        case TableCell.Result:
            self.resultCellHeight = self.view.frame.height - DeviceSize.statusBarHeight - DeviceSize.navBarHeight - filterCellHeight
            return self.resultCellHeight
        }
    }
    
    // tableView cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch getCell(indexPath) {
        case TableCell.Filter:
            return self.setupFilterCell(indexPath)
            
        case TableCell.Result:
            return self.setupResultCell(indexPath)
        }
    }
    
    // Filter cell
    fileprivate func setupFilterCell(_ indexPath: IndexPath) -> UITableViewCell {

        let filterCell: FilterCell = tableView.dequeueReusableCell(withIdentifier: TicketController.filterCellID, for: indexPath) as! FilterCell
        self.filterCell = filterCell

        // get location
        if searchInfo.origin == nil {
            fetchUserLocationAPI(vc: self)
        }

        return filterCell
    }
    
    // Result cell
    fileprivate func setupResultCell(_ indexPath: IndexPath) -> UITableViewCell {

        let resultCell: ResultCell = tableView.dequeueReusableCell(withIdentifier: TicketController.resultCellID, for: indexPath) as! ResultCell
            resultCell.revealvc = self.revealViewController()
        self.resultCell = resultCell

//        if skipIntroduction  {
//            resultCell.fetchTickets()
//        }

        return resultCell
    }
    
    // didSelect
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.expandedCell = getCell(indexPath)
    }
    
    // MARK: - custom tableView -----------------------------------------------------
    
    public func showAlert(_ alert: UIAlertController) {
        self.indicator.hide()

        self.present(alert, animated: true) { () -> Void in
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    func showIntoroduction() {
        self.present(IntroductionController(), animated: true, completion: nil)
    }
    
    func showVoiceSearchView() {
        self.voiceSearchView.voiceSearchViewHasShown = true
    }
}
