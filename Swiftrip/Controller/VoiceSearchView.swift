//
//  VoiceSearchController.swift
//  Swiftrip
//
//  Created by maki on 2017-05-29.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import MediaPlayer    // volume
import AVFoundation   // record
import Speech         // speech
import CoreData
import AI             // API.ai
import Firebase       // save message
import SwiftGifOrigin // show GIF anime

class VoiceSearchCollectionView: UIVisualEffectView {

    // TODO: declare from SNS login
    let user: Sender = Sender(sender: .user) // Sender(sender: .user, name: "Maki", profileImage: UIImage(named: "userProfile"))
    let ai  : Sender = Sender(sender: .AI)
    var messages: [Message]!                                                            // be defined in defaultMessage()
    fileprivate let cellID: String = "VoiceSearchCell"
    
    // setting of api.ai
    let apiAI_actionName   : String = "searchFlight"                                    // the name of the action of intents in api.ai
    let apiAI_doneMessage  : String = "Please wait a few seconds, I'm now searching."   // to recognize starting to search flights
    var apiAI_isDone       : Bool   = false                                             // be changed to true when recieving doneMessage
    
    // speech synthesizer & recognizer
    let audioEngine       : AVAudioEngine       = AVAudioEngine()
    let speechSynthesizer : AVSpeechSynthesizer = AVSpeechSynthesizer()
    let speechRecognizer  : SFSpeechRecognizer? = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask   : SFSpeechRecognitionTask?
    var speechRecognitionTaskDelegate: SFSpeechRecognitionTaskDelegate!

    fileprivate static let defaultMessage = "Say hello (^o^)"
    let tempMsg: NSString = "…"
    var keyboardHasShown: Bool = false
    fileprivate var timer: Timer?
    fileprivate var recordingStartTime: Date?

    // send feedback to database
    var ref: DatabaseReference! // Firebase database reference
    var messageID: String!      // id for classify database

    // handle visibility
    var voiceSearchViewHasShown: Bool = false { // true: voiceSearch is active, false: inactive
        didSet {
            if voiceSearchViewHasShown {
                show()
            } else {
                close()
            }
        }
    }

    // collectionView
    let collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
        let o: UICollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            o.backgroundColor = .clear
        return o
    }()

    // sound controll
    var volumeSlider: UISlider!
    var volume: Float!
    var soundButton: UIButton = {
        let o: UIButton = UIButton(imgName: "ic_volume_off", color: .white)
            o.addTarget(self, action: #selector(toggleSoundMute), for: .touchUpInside)
        return o
    }()
    var soundButtonMuted: Bool! {
        didSet {
            if soundButtonMuted {
                soundButton.setImage(UIImage(named: "ic_volume_off"), for: [])
            } else {
                soundButton.setImage(UIImage(named: "ic_volume_up"), for: [])
            }
        }
    }
    var mutedBySoundButton: Bool = false

    let closeButton: UIButton = {
        let o: UIButton = UIButton(imgName: "ic_close", color: .white)
            o.addTarget(self, action: #selector(clickedCloseButton), for: .touchUpInside)
        return o
    }()

    let messageInputContainerView: UIView = {
        let o: UIView = UIView()
            o.backgroundColor = UIColor(r: 0, g: 150, b: 210, a: 0.3)
        return o
    }()
    
    let inputTextField: UITextView = {
        let o: UITextView = UITextView(fontSize: 18)
            o.textColor = .white
            o.backgroundColor = .clear
            o.spellCheckingType = .no
            o.autocorrectionType = .no
            o.textAlignment = .center
            o.isScrollEnabled = true
            o.isUserInteractionEnabled = false
        return o
    }()
    
    lazy var recordButton: RecordButton = {
        let o: RecordButton = RecordButton()
            o.addTarget(self, action: #selector(toggleRecordButton), for: .touchUpInside)
        return o
    }()
    
    lazy var retryButton: UIButton = {
        let o: UIButton = UIButton(fontSize: 16, .white, text: "Retry")
            o.addTarget(self, action: #selector(handleRetry), for: .touchUpInside)
            o.isEnabled = false
            o.isHidden = true
        return o
    }()

    lazy var feedbackButton: UIButton = {
        let o: UIButton = UIButton(imgName: "ic_feedback", color: .white)
            o.addTarget(self, action: #selector(openFeedbackView), for: .touchUpInside)
        return o
    }()

    // MARK: - feedback view objects

    let feedback_textView: UITextView = {
        let o: UITextView = UITextView(fontSize: 16)
            o.backgroundColor = .white
            o.layer.cornerRadius = 5
            o.clipsToBounds = true
        return o
    }()
    let feedback_button: UIButton = {
        let o: UIButton = UIButton(fontSize: 16, .link, text: "Send")
            o.addTarget(self, action: #selector(clickedSendFeedbackButton), for: .touchUpInside)
            o.backgroundColor = .primary
            o.setTitleColor(.white, for: [])
            o.layer.cornerRadius = 5
            o.clipsToBounds = true
        return o
    }()
    let foodeback_defaultText: String = "Give me your feedback\nto improve this app! :D"
    let feedback_titleLable: UILabel = {
        let o: UILabel = UILabel(fontSize: 24, .white, text: "Feedback", textAlign: .center)
        return o
    }()
    let feedback_view: UIScrollView = {
        let o: UIScrollView = UIScrollView(frame: CGRect(x: 0, y: DeviceSize.statusBarHeight + DeviceSize.navBarHeight, width: DeviceSize.screenWidth(), height: DeviceSize.screenHeight()))
            o.contentSize.height = DeviceSize.screenHeight() * 2
            o.isUserInteractionEnabled = true
            o.alpha = 0
        return o
    }()
    let feedback_thanksLabel: UITextView = {
        let o: UITextView = UITextView(fontSize: 20, text: "Your message was sent.\nThank you!")
            o.textColor = .white
            o.backgroundColor = .clear
            o.textAlignment = .center
            o.alpha = 0
        return o
    }()
    let feedback_backgroundView: UIVisualEffectView = {
        let effect: UIBlurEffect = UIBlurEffect(style: .dark)
        let o: UIVisualEffectView = UIVisualEffectView(effect: effect)
            o.alpha = 0
        return o
    }()

    init() {
        let effect: UIBlurEffect = UIBlurEffect(style: .dark)
        super.init(effect: effect)
        self.frame = CGRect(x: 0, y: 0, width: DeviceSize.screenWidth(), height: DeviceSize.screenHeight())
        self.alpha = 0
        self.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func show() {

        if self.collectionView.delegate == nil {
            self.setupDataSouse()
            self.setupView()
            self.setupSoundButton()

        } else {
            self.volumeChanged()
        }

        self.isHidden = false

        self.addNotification() // keyboard notification

        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1

        }, completion: { (Bool) in
            self.defaultMessages()
        })
    }

    fileprivate func close() {

        self.stopRecording()
        self.removeNotification()

        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0

        }, completion: { (Bool) in
            self.isHidden = true
        })
    }

    fileprivate func setupDataSouse() {

        // collectionView
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(VoiceSearchCell.self, forCellWithReuseIdentifier: cellID)
        self.addSubview(self.collectionView)

        // speech
        self.speechSynthesizer.delegate = self
        self.speechRecognitionTaskDelegate = self

        // database
        self.ref = Database.database().reference()
        self.messageID = Date().convertToString(dateFormat: .YYYY_MM_DD_T_HHmm)
    }

    public func setupView() {

        // close button
        self.addSubview(closeButton)
        closeButton.layout(top: self.topAnchor, topConstant: DeviceSize.statusBarHeight, left: self.leftAnchor, leftConstant: 4, widthConstant: 48, heightConstant: 48)

        // sound button
        self.addSubview(self.soundButton)
        self.soundButton.layout(top: self.topAnchor, topConstant: DeviceSize.statusBarHeight, right: self.rightAnchor, rightConstant: 8, widthConstant: 48, heightConstant: 48)

        // messageInput (height was fixed)
        let height: CGFloat = 200
        self.addSubview(self.messageInputContainerView)
        self.messageInputContainerView.addSubview(self.inputTextField)
        self.messageInputContainerView.addSubview(self.recordButton)
        self.messageInputContainerView.addSubview(self.retryButton)
        self.messageInputContainerView.addSubview(self.feedbackButton)

        self.messageInputContainerView.layout(left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, heightConstant: height)

        self.inputTextField.layout(top: self.messageInputContainerView.topAnchor, left: self.messageInputContainerView.leftAnchor, right: self.messageInputContainerView.rightAnchor, heightConstant: 60)

        self.recordButton.layout(centerX: self.messageInputContainerView.centerXAnchor, bottom: self.messageInputContainerView.bottomAnchor, bottomConstant: 16, widthConstant: self.recordButton.frame.width, heightConstant: self.recordButton.frame.height)
        
        self.retryButton.layout(left: self.messageInputContainerView.leftAnchor, bottom: self.messageInputContainerView.bottomAnchor, widthConstant: 80, heightConstant: self.recordButton.frame.height)

        self.feedbackButton.layout(bottom: self.messageInputContainerView.bottomAnchor, right: self.messageInputContainerView.rightAnchor, widthConstant: 80, heightConstant: self.recordButton.frame.height)

        // collectionView (height is related to messageInput's height)
        self.collectionView.layout(top: self.closeButton.bottomAnchor, topConstant: 8, left: self.leftAnchor, bottom: self.messageInputContainerView.topAnchor, right: self.rightAnchor)

        self.setupFeedbackView()
    }

    fileprivate func setupSoundButton() {

        // reference volume status
        let mpVolumeView: MPVolumeView = MPVolumeView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            mpVolumeView.isHidden = true
        for childView in mpVolumeView.subviews {
            if childView.isKind(of: UISlider.self) {
                self.volumeSlider = childView as? UISlider
                break
            }
        }

        DispatchQueue.main.async {
            self.volumeChanged()

            // call volumeChanged() when the volume was changed
            NotificationCenter.default.addObserver(self, selector: #selector(self.volumeChanged), name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
        }
    }

    @objc fileprivate func toggleSoundMute() {

        // turn on
        if self.soundButtonMuted {
            self.volumeSlider!.setValue(volume, animated: false)
            self.soundButtonMuted = false

        // turn off
        } else {
            self.mutedBySoundButton = true
            self.volume = self.volumeSlider!.value
            self.volumeSlider!.setValue(0.0, animated: false)
            self.soundButtonMuted = true
        }
    }

    @objc fileprivate func volumeChanged() {

        print("volue: \(self.volumeSlider.value)")

        if !self.mutedBySoundButton {
            self.volume = self.volumeSlider!.value
        }

        if self.volumeSlider!.value == 0 {
            self.soundButtonMuted = true
        } else {
            self.soundButtonMuted = false
        }
    }
    
    fileprivate func defaultMessages() {

        // checking of speech authorization before first speeching
        let statusSpeech = SFSpeechRecognizer.authorizationStatus()
        if statusSpeech != .authorized {
            self.requestSpeechAuthorization()

        } else {
            var text: String!
            if let userName = self.user.name {
                text = "Hi, \(userName).\nWhat can I help you?"
            } else {
                text = "Hi.\nWhat can I help you?"
            }
            self.speak(text)
            self.messages = [Message(text: text as NSString, sender: self.ai)]
            self.collectionView.reloadData()
        }
    }

    func addNotification() { // notification for showing keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func removeNotification() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc fileprivate func handleSend() { // when clickced "send" button

        // change to enableMode
        self.recordButton.mode = .enable
        self.recordButton.removeTarget(self, action: #selector(handleSend), for: .touchUpInside)
        self.recordButton.addTarget(self, action: #selector(toggleRecordButton), for: .touchUpInside)
        
        // request API
        if let text: String = self.inputTextField.text {
            
            print(text)

            self.messages.last?.text = text as NSString
            let lastCellPath: IndexPath = IndexPath(item: self.collectionView.numberOfItems(inSection: 0) - 1, section: 0)
            self.collectionView.reloadItems(at: [lastCellPath])

            // clear
            self.inputTextField.text = nil

            // desable retryButton
            self.retryButton.isEnabled = false
            self.retryButton.isHidden  = true

            // request API
            self.requestApiAI(userMessage: self.messages.last!.text as String)
        }
    }

    public func insertMessage() {

        print("insertMessage")

        self.collectionView.performBatchUpdates({

            // insert last message
            self.collectionView.insertItems(at: [IndexPath(item: self.messages.count - 1, section: 0)])

        }) { (Bool) in
            self.scrollToBottom()
        }
    }

    fileprivate func scrollToBottom() {
        if (self.collectionView.contentSize.height > self.collectionView.frame.size.height - self.collectionView.contentInset.top - self.collectionView.contentInset.bottom) {
            print("scroll to bottom of collectionView")
            let offset: CGPoint = CGPoint(x: 0, y: self.collectionView.contentSize.height - self.collectionView.frame.size.height + self.collectionView.contentInset.bottom + 5)
            self.collectionView.setContentOffset(offset, animated: true)
        }
    }
    
    @objc fileprivate func toggleRecordButton() {

        if !audioEngine.isRunning {
            // start recording
            try! self.startRecording()

        } else {
            // stop recording
            self.stopRecording()
        }
    }

    fileprivate func startRecording() throws {
        print("\nstartRecording()\n")

        self.recordButton.mode = .recording

        // refresh task
//        self.refreshTask()

        // initilize recognitionRequest
        self.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        guard let recognitionRequest = self.recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
        recognitionRequest.shouldReportPartialResults = true

        guard let node = audioEngine.inputNode else { fatalError("Unable to created an InputNode") }

        // reflesh tap
        node.removeTap(onBus: 0)

        // install tap
        node.installTap(onBus: 0, bufferSize: 1024, format: node.outputFormat(forBus: 0)) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            recognitionRequest.append(buffer)
        }

        // prepare
        audioEngine.prepare()

        // start
        do { try audioEngine.start() }
        catch { return print(error) }

        // task
        self.recognitionTask = self.speechRecognizer?.recognitionTask(with: recognitionRequest, delegate: speechRecognitionTaskDelegate)
    }

    fileprivate func stopRecording() {

        if audioEngine.isRunning {
            
            print("stop recording")

            self.recordButton.mode = .stop

            audioEngine.stop()
            recognitionRequest?.endAudio()

            // refresh task
            self.refreshTask()

            // if user spoke something
            if !self.inputTextField.text.isEmpty && self.inputTextField.text != VoiceSearchCollectionView.defaultMessage {

                // change to sendMode
                self.recordButton.mode = .send
                self.recordButton.removeTarget(self, action: #selector(self.toggleRecordButton), for: .touchUpInside)
                self.recordButton.addTarget(self, action: #selector(self.handleSend), for: .touchUpInside)
            }
        }

        // invalidate timer
        self.timer?.invalidate()
    }

    fileprivate func refreshTask() {
        if let recognitionTask = self.recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
    }

    @objc fileprivate func handleRetry() {

        self.retryButton.isEnabled = false
        self.retryButton.isHidden  = true

        self.inputTextField.text = nil

        self.timer?.invalidate()

        try! self.startRecording()
    }

    @objc fileprivate func clickedCloseButton() {
        self.voiceSearchViewHasShown = false
    }
}



extension VoiceSearchCollectionView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    // ------------------------------------------------------------------
    // MARK: - UICollectionViewDataSource
    // ------------------------------------------------------------------

    // section
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    // item
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = messages?.count {
            return count
        }
        return 0
    }

    // cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let message: Message = self.messages[indexPath.item]

        let cell: VoiceSearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! VoiceSearchCell
        cell.message = message // setup by message.didSet in VoiceSearchControllerCell

        return cell
    }

    // size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.messages.count >= indexPath.item {
            let message: Message = self.messages[indexPath.item]
            let estimatedFrame: CGRect = VoiceSearchCell().estimateFrame(string: message.text)
            return CGSize(width: self.frame.width, height: estimatedFrame.height + 36)
        }
        return CGSize(width: 0, height: 0)
    }
}



extension VoiceSearchCollectionView: AVSpeechSynthesizerDelegate, SFSpeechRecognizerDelegate, SFSpeechRecognitionTaskDelegate {
    
    // ------------------------------------------------------------------
    // MARK: - speech recognition methods
    // ------------------------------------------------------------------
    
    // Authorization of speech
    func requestSpeechAuthorization()  {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.defaultMessages()

                case .denied, .restricted, .notDetermined:

                    // showing alert
                    let alert: UIAlertController = UIAlertController(title: "Speech Recognition Authorization", message: "You haven't accepted the authorization of speech recognition. You can change it from \"Privacy\" of setting app.", preferredStyle: .alert)
                    let open : UIAlertAction = UIAlertAction(title: "Open setting app", style: .default,
                                                             handler: { (action: UIAlertAction!) -> Void in
                        // opening setting app
                        UIApplication.shared.open(URL(string: "App-Prefs:root=Privacy")!)
                    })
                    let close: UIAlertAction = UIAlertAction(title: "Close", style: .default,
                                                             handler: { (action: UIAlertAction!) -> Void in
                        // closing voiceSearchView
                        self.voiceSearchViewHasShown = false
                    })
                    alert.addAction(open)
                    alert.addAction(close)
                    alert.show(vc: ticketController, dismiss: false)
                }
            }
        }
    }

    func speechRecognitionDidDetectSpeech(_ task: SFSpeechRecognitionTask) {
        print("speechRecognitionDidDetectSpeech =====================")
    }
    
    // checking speechRecoginizer is avalable
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        
        if available {
            self.recordButton.isEnabled = true
        } else {
            self.recordButton.isEnabled = false
            self.recordButton.setTitle("Unable to use now", for: .disabled)
        }
        print(speechRecognizer)
    }

    // result of speechRecognization
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didHypothesizeTranscription transcription: SFTranscription) {

        if audioEngine.isRunning {
            let allText: String = transcription.formattedString
            self.inputTextField.text = allText
            print("\(allText)")

            self.recordingStartTime = Date()

            self.timer?.invalidate()
            self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.finishRecordingAutomatically), userInfo: nil, repeats: true)

            if !self.retryButton.isEnabled {
                self.retryButton.isEnabled = true
                self.retryButton.isHidden = false
            }

//            guard let lastTranscription = transcription.segments.last else {return}
//            let indexTo = allText.index(allText.startIndex, offsetBy: lastTranscription.substringRange.location)
//            let lastString: String = allText.substring(from: indexTo)
        }
    }

    func finishRecordingAutomatically() {

        if let recordingStartTime = self.recordingStartTime {

            let watingTime: Double = 2.0
            let span: Double = Double(Date().timeIntervalSince(recordingStartTime))

            // finish recording in 1 second, if user didn't speak
            if span >= watingTime {
                print("finish recording")
                self.timer?.invalidate()
                self.toggleRecordButton()
                self.handleSend()
                self.retryButton.isEnabled = false
                self.retryButton.isHidden = true
            }
        }
    }
    
    func speechRecognitionTaskFinishedReadingAudio(_ task: SFSpeechRecognitionTask) {
        print("speechRecognitionTaskFinishedReadingAudio\n")
    }
    
    func speechRecognitionTask(_ task: SFSpeechRecognitionTask, didFinishSuccessfully: Bool) {
        print("didFinishSuccessfully\n")

        // change color when recording time was out
        if self.recordButton.mode == .recording {
            self.recordButton.mode = .stop
        }
        if audioEngine.isRunning {
            self.stopRecording()
        }
    }

    func speechRecognitionTaskWasCancelled(_ task: SFSpeechRecognitionTask) {
        print("speechRecognitionTaskWasCancelled\n")
        
        self.audioEngine.stop()
        self.audioEngine.inputNode?.removeTap(onBus: 0)

        self.recognitionRequest = nil

        if self.recordButton.mode != .send {
            self.recordButton.mode = .stop
        }
    }
    
    // ------------------------------------------------------------------
    // speech synthesizer methods
    // ------------------------------------------------------------------
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        print("willSpeakRangeOfSpeechString")
        print(utterance.speechString) // this works!
    }

    // speeching finished
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        print("\nspeech finished\n")

        if !apiAI_isDone { // if AI was done, open TicketController instead of inserting a message.

            if messages.count == 1 {
                self.inputTextField.text = VoiceSearchCollectionView.defaultMessage
            }

            // insert temp message
            let tempMessage: Message = Message(text: self.tempMsg, sender: self.user)
            self.messages.append(tempMessage)
            self.insertMessage()

            // start recording
            try! self.startRecording()
        }
    }
    
    func requestApiAI(userMessage: String) {

        // request
        AI.sharedService.textRequest(userMessage).success(completionHandler: { (response) -> Void in
            // success
            if let speechText: String = response.result.fulfillment?.speech {
                print(speechText)
                // insert new message as collectionView row
                self.messages.append(Message(text: speechText as NSString, sender: self.ai))
                self.insertMessage()

                // speak
                self.speak(speechText)

                print(response.result.parameters ?? "none")

                guard let actionName: String = response.result.action else { return }

                // check parameters are matched
                if actionName == self.apiAI_actionName,
                    let parameters: [String: Any?] = response.result.parameters {

                    // setup searchInfo
                    self.setupSearchInfo(parameters)

                    // compare with a searching done message
                    if speechText == self.apiAI_doneMessage {
                        self.apiAI_isDone = true
                        self.showTickets()
                    }
                }
            }

        }).failure(completionHandler: { (error) -> Void in
            // error
            print(error)
            let alert: UIAlertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.show(vc: alert, dismiss: true)
            self.recordButton.mode = .enable
        })
    }

    public func speak(_ text: String) {

        let utterance: AVSpeechUtterance = AVSpeechUtterance(string: text)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US") // TODO: set language dynamically
            utterance.volume             = 1.0
            utterance.rate               = 0.5  // voice speed
            utterance.pitchMultiplier    = 1.3  // voice key tone
            utterance.preUtteranceDelay  = 0.0  // delay for speaking
            utterance.postUtteranceDelay = 0.0  // delay for finishing to speak

        self.speechSynthesizer.speak(utterance)
    }
    
    func setupSearchInfo(_ parameters: [String: Any?]) {

        print(parameters)
        
        // destination airport
        if let airport = parameters["airport"] {
            guard let airportDic = airport as? [String: Any?] else { return }

            searchInfo.destination = Airport(airportCode: airportDic["IATA"] as! String,
                                             airportName: airportDic["name"] as! String,
                                             cityName   : airportDic["city"] as! String,
                                             countryName: airportDic["country"] as! String)
        }

        // departDate
        if let _departDate: String = parameters["departDate"] as? String {
            if !_departDate.isEmpty {
                searchInfo.departDate = _departDate.convertToDate(dateFormat: .YYYY_MM_DD)
            }
        }

        // returnDate
        if let _returnDate: String = parameters["returnDate"] as? String {
            if !_returnDate.isEmpty {
                searchInfo.returnDate = _returnDate.convertToDate(dateFormat: .YYYY_MM_DD)
            }
        }

        // budget
        if let _budget = parameters["budget"] {

            guard let budgetDic = _budget as? [String: Any?] else { return }
            if let amount = budgetDic["amount"] as? NSNumber {
                if !(amount).isEqual(to: 0) {
                    searchInfo.budget = amount
                }
            }
            if let _currency: String = budgetDic["currency"] as? String {
                if !_currency.isEmpty {
                    searchInfo.currency = convertCurrency(from: _currency)
                }
            }
        }

        print("origin code: \(searchInfo.origin?.airportCode ?? "none")")
        print("origin name: \(searchInfo.origin?.airportName ?? "none")")
        print("destination code: \(searchInfo.destination?.airportCode ?? "none")")
        print("destination name: \(searchInfo.destination?.airportName ?? "none")")
        print("destination city: \(searchInfo.destination?.cityName ?? "none")")
        print("depart: \(searchInfo.departDate?.convertToString(dateFormat: .YYYY_MM_DD) ?? "none")")
        print("return: \(searchInfo.returnDate?.convertToString(dateFormat: .YYYY_MM_DD) ?? "none")")
    }

    func showTickets() {
        
        // save to UserDefault
        Utility.save_userDefault()

        // show TicketController
        ticketController.resultCell?.fetchTickets()
    }
}




// ----------------------------------------------------------
// functions for feedback view
// ----------------------------------------------------------

extension VoiceSearchCollectionView: UITextViewDelegate {

    fileprivate func setupFeedbackView() {
        self.feedback_view.addSubview(self.feedback_titleLable)
        self.feedback_view.addSubview(self.feedback_textView)
        self.feedback_view.addSubview(self.feedback_button)
        self.addSubview(self.feedback_backgroundView)
        self.addSubview(self.feedback_thanksLabel)
        self.addSubview(self.feedback_view)

        self.feedback_titleLable.layout(top: self.feedback_view.topAnchor, topConstant: 100,
                                        left: self.leftAnchor, leftConstant: 16,
                                        right: self.rightAnchor, rightConstant: 16)

        self.feedback_textView.layout(top: self.feedback_titleLable.bottomAnchor, topConstant: 16,
                                      left: self.leftAnchor, leftConstant: 16,
                                      right: self.rightAnchor, rightConstant: 16,
                                      heightConstant: 120)

        self.feedback_button.layout(top: self.feedback_textView.bottomAnchor, topConstant: 8,
                                    left: self.leftAnchor, leftConstant: 16,
                                    right: self.feedback_textView.rightAnchor,
                                    heightConstant: 48)


        self.feedback_thanksLabel.layout(centerY: self.centerYAnchor,
                                         left: self.leftAnchor, leftConstant: 16,
                                         right: self.rightAnchor, rightConstant: 16,
                                         heightConstant: 60)

        self.feedback_backgroundView.layout(top: self.topAnchor,
                                            left: self.leftAnchor,
                                            bottom: self.bottomAnchor,
                                            right: self.rightAnchor)

        // add tap actions
        let gesture_view: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(closeFeedbackView))
        self.feedback_view.addGestureRecognizer(gesture_view)
        self.feedback_textView.delegate = self
    }

    @objc fileprivate func handleKeyboardShow(_ notification: Notification) {
        self.keyboardHasShown = true
        self.feedbackTextViewToEditable()

        guard let userInfo: [AnyHashable : Any] = notification.userInfo else { return }
        let keyboardRect: CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let txtLimit: CGFloat = self.feedback_textView.frame.origin.y + self.feedback_textView.frame.height + 8 + self.feedback_button.frame.height
        let kbdLimit: CGFloat = DeviceSize.screenHeight() - keyboardRect.size.height

        if txtLimit >= kbdLimit {
            self.feedback_view.isScrollEnabled = true
            self.feedback_view.contentOffset.y = txtLimit - kbdLimit + 24 + self.feedback_button.frame.height
            self.feedback_view.isScrollEnabled = false
        }
    }

    @objc fileprivate func handleKeyboardHide(_ notification: Notification) {
        self.keyboardHasShown = false
    }

    @objc fileprivate func openFeedbackView() {

        self.stopRecording()

        self.feedbackTextViewToDefault()

        UIView.animate(withDuration: 0.2) {
            self.feedback_backgroundView.alpha = 1
            self.feedback_view.alpha = 1
        }
    }

    @objc fileprivate func closeFeedbackView() {

        // scroll
        self.feedback_view.isScrollEnabled = true
        self.feedback_view.contentOffset.y = 0
        self.feedback_view.isScrollEnabled = false

        if self.keyboardHasShown {
            // close keyboard
            self.feedback_textView.resignFirstResponder()
            self.keyboardHasShown = false

        } else {
            UIView.animate(withDuration: 0.2) {
                self.feedback_backgroundView.alpha = 0
                self.feedback_view.alpha = 0
            }

            if self.feedback_thanksLabel.alpha != 0 {
                UIView.animate(withDuration: 0.2) {
                    self.feedback_thanksLabel.alpha = 0
                }
            }
        }
    }

    @objc fileprivate func clickedSendFeedbackButton() {

        // close keyboard
        self.feedback_textView.resignFirstResponder()

        // prepare NSString (Firebase accepts only NS types)
        let feedbackText: NSString = self.feedback_textView.text as NSString

        if feedbackText.length > 200 {
            let amount: Int = 200 - feedbackText.length
            let alert: UIAlertController = UIAlertController(title: "Oops!", message: "Please reduce the amount of your message more \(amount) letters.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                alert.show(vc: ticketController!, dismiss: false)

        } else if feedbackText != "" {
            // save on Firebase
            self.ref.child("feedbacks").child(messageID).setValue(feedbackText)

            // clear
            self.feedback_textView.text = nil

            // show thanks message then dismiss it
            UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
                self.feedback_thanksLabel.alpha = 1
                self.feedback_view.alpha = 0

            }, completion: { (_) -> Void in
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.closeFeedbackView()
                }
            })
        }
    }

    fileprivate func feedbackTextViewToDefault() {
        self.feedback_textView.contentInset = UIEdgeInsets(top: 30, left: 0, bottom: -30, right: 0)
        self.feedback_textView.text = foodeback_defaultText
        self.feedback_textView.textAlignment = .center
        self.feedback_textView.isScrollEnabled = false
        self.feedback_textView.textColor = .lightGray
        self.feedback_view.isScrollEnabled = false
        self.feedback_button.isHidden = true
    }

    fileprivate func feedbackTextViewToEditable() {
        if self.feedback_textView.text == self.foodeback_defaultText {
            self.feedback_textView.text = nil
        }
        self.feedback_textView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.feedback_textView.textAlignment = .left
        self.feedback_textView.textColor = .black
        self.feedback_textView.isScrollEnabled = true
        self.feedback_textView.becomeFirstResponder() //  keyboard
        self.feedback_button.isHidden = false
    }
}
