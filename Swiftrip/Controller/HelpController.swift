//
//  HelpController.swift
//  Swiftrip
//
//  Created by maki on 2017-06-26.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import Firebase

class HelpController: UITableViewController {

    fileprivate let cellID: String = "reuseIdentifier"
    fileprivate var helps: [Help] = []
    let margin: CGFloat = 8

    override func viewDidLoad() {
        super.viewDidLoad()

        // navigation
        self.navigationItem.title = "Help"
        let closeItem: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_close"), style: .done, target: self, action: #selector(close))
        self.navigationItem.rightBarButtonItem = closeItem

        self.tableView.register(HelpControllerCell.self, forCellReuseIdentifier: cellID)
        self.tableView.separatorStyle = .none

//        self.tableView.estimatedRowHeight = 80
//        self.tableView.rowHeight = UITableViewAutomaticDimension

        // load Firebase
        let ref: DatabaseReference = Database.database().reference()
        ref.child("help").observe(DataEventType.value, with: { (snapshot) in

            if let value: Any = snapshot.value {
                print(value)

                for dic in value as! [Dictionary<String, String>] {
                    if let question = dic["q"], let answer = dic["a"] {
                        self.helps.append(Help(questtion: question as NSString, answer: answer as NSString))
                    }
                }

                self.tableView.reloadData()
            }

        }) { (error) in
            print(error.localizedDescription)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.helps.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        let cell: HelpControllerCell = tableView.dequeueReusableCell(withIdentifier: self.cellID, for: indexPath) as! HelpControllerCell

        // set texts
        cell.Help = self.helps[indexPath.row]


        let size = CGSize(width: DeviceSize.screenWidth() - (self.margin * 2), height: CGFloat.greatestFiniteMagnitude)
        let questionFrame = estimateFrame(helps[indexPath.row].questtion as String, fontSize: cell.questionFontSize, size: size)
        let answerFrame   = estimateFrame(helps[indexPath.row].answer    as String, fontSize: cell.answerFontSize,   size: size)

        // auto layout
        cell.questionTextView.layout(top: cell.topAnchor, topConstant: self.margin, left: cell.leftAnchor, leftConstant: self.margin, right: cell.rightAnchor, rightConstant: self.margin, heightConstant: questionFrame.height)
        cell.answerTextView.layout(top: cell.questionTextView.bottomAnchor, left: cell.leftAnchor, leftConstant: self.margin, right: cell.rightAnchor, rightConstant: self.margin, heightConstant: answerFrame.height + (self.margin * 3))

        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if self.helps.count > 0 {
            let size = CGSize(width: DeviceSize.screenWidth() - (self.margin * 2), height: CGFloat.greatestFiniteMagnitude)
            let questionFrame = estimateFrame(helps[indexPath.row].questtion as String, fontSize: 16, size: size)
            let answerFrame   = estimateFrame(helps[indexPath.row].answer    as String, fontSize: 12, size: size)

            return questionFrame.height + answerFrame.height + (self.margin * 3)
        }
        return 0
    }

    func close() {
        dismiss(animated: true, completion: nil)
    }
}

class HelpControllerCell: BaseTableViewCell {

    var height: CGFloat = 0.0

    var Help: Help? {

        didSet {
            self.questionTextView.text = Help?.questtion as String!
            self.questionTextView.font = UIFont(name: fontName, size: questionFontSize)

            self.answerTextView.text   = Help?.answer as String!
            self.answerTextView.font = UIFont(name: fontName, size: answerFontSize)
        }
    }

    lazy var questionFontSize: CGFloat = 16
    lazy var answerFontSize: CGFloat = 12

    let separator: UIView = {
        let o: UIView = UIView()
            o.backgroundColor = UIColor(r: 220, g: 220, b: 220, a: 1.0)
        return o
    }()

    let questionTextView: UITextView = {
        let o: UITextView = UITextView()
        o.isSelectable = false
        o.isScrollEnabled = false
        o.isEditable = false
        o.backgroundColor = .clear
        return o
    }()

    let answerTextView : UITextView = {
        let o: UITextView = UITextView()
        o.isSelectable = false
        o.isScrollEnabled = false
        o.isEditable = false
        o.textColor = .gray
        o.backgroundColor = .clear
        return o
    }()

    override func setupView() {

        self.contentView.addSubview(self.separator)
        self.contentView.addSubview(self.questionTextView)
        self.contentView.addSubview(self.answerTextView)

        self.separator.layout(left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, heightConstant: 1)
    }
}

class Help {

    let questtion: NSString
    let answer: NSString

    init(questtion: NSString, answer: NSString) {
        self.questtion = questtion
        self.answer = answer
    }
}
