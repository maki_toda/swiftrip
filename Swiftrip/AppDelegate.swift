//
//  AppDelegate.swift
//  Swiftrip
//
//  Created by maki on 2017-04-22.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import CoreData
import AI
import Firebase
import Fabric
import Crashlytics

var skipIntroduction: Bool!
var searchInfo: SearchInfo!
var airports: [Airport]?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // Firebase
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true

        // setting of API.ai
        AI.configure(Setting.apiAI_clientAccessToken)

        // remove UserDefaults
        // UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)

        // load UserDefaults
        Utility.load_introducton()
        Utility.load_searchInfo()
        Utility.load_airports()

        // root controller for navigaiton
        let root: TicketController = TicketController()

        // navigation controller
        let nvc: UINavigationController = UINavigationController(rootViewController: root)
            nvc.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white,
                                                     NSFontAttributeName: UIFont(name: fontName, size: 16)!]
            nvc.navigationBar.setBackgroundImage(UIImage(), for: .default)  // remove back image
            nvc.navigationBar.shadowImage   = UIImage()                     // remove shadow line
            nvc.navigationBar.tintColor     = .white                        // fill color of navigation images

//        // reveal controller
//        let revealController = SWRevealViewController()
//            revealController.frontViewController = ncv
//            revealController.rearViewController = SidebarMenuController()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = nvc //revealController
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {

        if !checkReachability(host_name: "google.com") {
            let alertController = UIAlertController(title: "Disconnection", message: "Try again after connecting with the internet.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Swiftrip")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

