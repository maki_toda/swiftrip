//
//  Utility.swift
//  Swiftrip
//
//  Created by maki on 2017-04-22.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import SystemConfiguration

// UserDefault keys
public let userDefaultKey_skipIntroduction : String = "introduction"
public let userDefaultKey_searchInfo       : String = "searchInfo"
public let userDefaultKey_airports         : String = "airports"

// font
public let fontName: String  = "Gotham Rounded"
public let margin  : CGFloat = 32


extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

//extension UIViewController {
//
//    public func setupSidebar() {
//
//        if let revealVC: SWRevealViewController = self.revealViewController() {
//
//            // settting menuButton on navigationBar
//            self.navigationItem.leftBarButtonItems = {
//
//                let menuButton: UIButton = UIButton.init()
//                    menuButton.setImage(UIImage.init(named: "ic_menu"), for: .normal)
//                    menuButton.addTarget(revealVC, action: #selector(revealVC.revealToggle(animated:)), for: .touchUpInside)
//                    menuButton.sizeToFit()
//
//                return [UIBarButtonItem.init(customView: menuButton)]
//            }()
//
//            // closing sidebar by tapping front viewController
//            self.view.addGestureRecognizer((revealVC.tapGestureRecognizer())!)
//        }
//    }
//}

let arrayCurrency: [Currency] = [.USD, .AUD, .CAD, .GBP, .JPY, .EUR, .RUB, .CNY]

enum Currency: String {

    case USD = "USD"
    case AUD = "AUD"
    case CAD = "CAD"
    case GBP = "GBP"
    case JPY = "JPY"
    case EUR = "EUR"
    case RUB = "RUB"
    case CNY = "CNY"

    func localeCode() -> String {
        switch self {
        case Currency.USD: return "en_US"
        case Currency.AUD: return "en_AU"
        case Currency.CAD: return "en_CA"
        case Currency.GBP: return "en_GB"
        case Currency.JPY: return "jp_JP"
        case Currency.EUR: return "it_IT"
        case Currency.RUB: return "ru_RU"
        case Currency.CNY: return "zh_CN"
        }
    }

    func name() -> String {
        switch self {
        case Currency.USD: return "US dollar"
        case Currency.AUD: return "Australian dollar"
        case Currency.CAD: return "Canadian dollar"
        case Currency.GBP: return "British pound"
        case Currency.JPY: return "Japanese yen"
        case Currency.EUR: return "EU euro"
        case Currency.RUB: return "Russian ruble"
        case Currency.CNY: return "Chinese yuan renminbiN"
        }
    }
}

func convertCurrency(from string: String) -> Currency {
    switch string {
    case Currency.USD.rawValue: return Currency.USD
    case Currency.CAD.rawValue: return Currency.CAD
    case Currency.AUD.rawValue: return Currency.AUD
    case Currency.EUR.rawValue: return Currency.EUR
    case Currency.GBP.rawValue: return Currency.GBP
    case Currency.RUB.rawValue: return Currency.RUB
    case Currency.CNY.rawValue: return Currency.CNY
    case Currency.JPY.rawValue: return Currency.JPY
    default: return Currency.USD
    }
}

extension UIView {

    func anchorToTop(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil) {

        layout(top: top, left: left, bottom: bottom, right: right)
    }

    func layout(centerX: NSLayoutXAxisAnchor? = nil, centerXConstant: CGFloat = 0,
                centerY: NSLayoutYAxisAnchor? = nil, centerYConstant: CGFloat = 0,
                top    : NSLayoutYAxisAnchor? = nil, topConstant    : CGFloat = 0,
                left   : NSLayoutXAxisAnchor? = nil, leftConstant   : CGFloat = 0,
                bottom : NSLayoutYAxisAnchor? = nil, bottomConstant : CGFloat = 0,
                right  : NSLayoutXAxisAnchor? = nil, rightConstant  : CGFloat = 0,
                widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) {

        _ = anchor(centerX: centerX, centerXConstant: centerXConstant,
                   centerY: centerY, centerYConstant: centerYConstant,
                   top    : top,     topConstant    : topConstant,
                   left   : left,    leftConstant   : leftConstant,
                   bottom : bottom,  bottomConstant : bottomConstant,
                   right  : right,   rightConstant  : rightConstant,
                   widthConstant: widthConstant, heightConstant: heightConstant)
    }

    func anchor(centerX: NSLayoutXAxisAnchor? = nil, centerXConstant: CGFloat = 0,
                centerY: NSLayoutYAxisAnchor? = nil, centerYConstant: CGFloat = 0,
                top    : NSLayoutYAxisAnchor? = nil, topConstant    : CGFloat = 0,
                left   : NSLayoutXAxisAnchor? = nil, leftConstant   : CGFloat = 0,
                bottom : NSLayoutYAxisAnchor? = nil, bottomConstant : CGFloat = 0,
                right  : NSLayoutXAxisAnchor? = nil, rightConstant  : CGFloat = 0,
                widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {

        self.translatesAutoresizingMaskIntoConstraints = false

        var anchors = [NSLayoutConstraint]()

        if let x: NSLayoutXAxisAnchor = centerX {
            anchors.append(centerXAnchor.constraint(equalTo: x, constant: centerXConstant))
        }

        if let y: NSLayoutYAxisAnchor = centerY {
            anchors.append(centerYAnchor.constraint(equalTo: y, constant: centerYConstant))
        }

        if let top: NSLayoutYAxisAnchor = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }

        if let left: NSLayoutXAxisAnchor = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }

        if let bottom: NSLayoutYAxisAnchor = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }

        if let right: NSLayoutXAxisAnchor = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }

        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }

        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }

        anchors.forEach({$0.isActive = true})

        return anchors
    }

}

extension UIImageView {

    convenience init(imgName: String, color: UIColor? = nil) {

        self.init(image: UIImage(named: imgName))

        // fill color
        if let _color: UIColor = color {
            self.image = self.image?.withRenderingMode(.alwaysTemplate)
            self.tintColor = _color
        }

        self.contentMode = .scaleAspectFit
    }
}

extension UIButton {

    convenience init(fontSize: CGFloat, _ color: UIColor = UIColor.darkGray, text: String) {
        self.init()
        self.titleLabel?.font = UIFont(name: fontName, size: fontSize)
        self.setTitleColor(color, for: .normal)
        self.setTitle(text, for: .normal)
    }

    convenience init(imgName: String, color: UIColor?) {
        self.init()
        var image: UIImage

        // fill color
        if let _color: UIColor = color {
            image = UIImage(named: imgName)!.withRenderingMode(.alwaysTemplate)
            self.tintColor = _color

        } else {
            image = UIImage(named: imgName)!
        }

        self.setImage(image, for: .normal)
    }
}

extension UILabel {

    convenience init(fontSize: CGFloat, _ color: UIColor, text: String? = nil, textAlign: NSTextAlignment) {
        self.init()
        self.font = UIFont(name: fontName, size: fontSize)
        self.textColor = color
        self.text = text
        self.textAlignment = textAlign
    }
}

extension UITextField {

    convenience init(fontSize: CGFloat, _ color: UIColor, placeholder: String? = nil, textAlign: NSTextAlignment) {
        self.init()
        self.font = UIFont(name: fontName, size: fontSize)
        self.textColor = color
        self.placeholder = placeholder
        self.textAlignment = textAlign
    }

    func addUnderline(width: CGFloat, height: CGFloat, color: UIColor) {
        let border: CALayer = CALayer()
            border.frame = CGRect(x: 0, y: height - 1, width: width, height: 1)
            border.backgroundColor = color.cgColor
        self.layer.addSublayer(border)
    }
}

extension UITextView {

    convenience init(fontSize: CGFloat, text: String? = nil) {
        self.init()
        self.font = UIFont(name: fontName, size: fontSize)
        self.text = text
    }
}

extension UISegmentedControl {

    convenience init(fontSize: CGFloat) {
        self.init()
        let font: UIFont = UIFont(name: fontName, size: fontSize)!
        self.setTitleTextAttributes([NSFontAttributeName: font], for: [])
    }
}

extension UIColor {

    static let link          : UIColor = UIColor(r:   0, g: 122, b: 255, a: 1.0)
    static let primary       : UIColor = UIColor(r:  81, g: 184, b: 212, a: 1.0)
    static let sub           : UIColor = UIColor(r:   0, g: 150, b: 210, a: 1.0)
    static let superLightGray: UIColor = UIColor(r: 240, g: 240, b: 240, a: 1.0)

    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
}

class BudgetLabel: UILabel {

    override var text: String? {
        didSet {
            // add plus sign if the budget equals max
            if let _text: String = text {
                let formatter = NumberFormatter()
                    formatter.numberStyle = .currency
                    formatter.locale = Locale(identifier: searchInfo.currency.localeCode())
                let num = formatter.number(from: _text)?.floatValue
                if num == budget_max {
                    text!.append("+")
                }
            }
        }
    }

    init(fontSize: CGFloat) {
        super.init(frame: CGRect.zero)
        self.font = UIFont(name: fontName, size: fontSize)
        self.textAlignment = .center
        self.textColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupText(_ sliderValue: Float) {
        let num: NSNumber = NSNumber.init(value: round(sliderValue / 10) * 10)

        let formatter: NumberFormatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = Locale(identifier: searchInfo.currency.localeCode())
            formatter.alwaysShowsDecimalSeparator = false
            formatter.usesGroupingSeparator = false
            formatter.positivePrefix = "\(formatter.currencySymbol!)"
            formatter.maximumFractionDigits = 0
            formatter.minimumFractionDigits = 0
        self.text = formatter.string(from: num)
    }

    func convertNumber() -> NSNumber? {
        if let _text = self.text {
            let formatter = NumberFormatter()
                formatter.numberStyle = .currency
                formatter.locale = Locale(identifier: searchInfo.currency.localeCode())
            return formatter.number(from: _text)
        }
        return nil
    }
}

class Utility {
    
    class func initSegmentedControl(fontSize: CGFloat, _ color: UIColor, segments: [String]) -> UISegmentedControl {
        let o: UISegmentedControl = UISegmentedControl(fontSize: fontSize)
            o.backgroundColor = color
            o.tintColor = .white
        for (i, segment) in segments.enumerated() {
            o.insertSegment(withTitle: segment, at: i, animated: true)
        }
        return o
    }
    
    class func initTextView(text: String, fontSize: CGFloat, color: UIColor) -> UITextView {
        let o: UITextView = UITextView(fontSize: fontSize, text: text)
            o.backgroundColor = color
            o.spellCheckingType = .no
            o.autocorrectionType = .no
            o.isScrollEnabled = false
            o.isEditable = false
            o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }

    class func save_userDefault() {

        let searchInfo_data: Data = NSKeyedArchiver.archivedData(withRootObject: searchInfo)
        UserDefaults.standard.set(searchInfo_data, forKey: userDefaultKey_searchInfo)

        if let _airports = airports {
            let data: Data = NSKeyedArchiver.archivedData(withRootObject: _airports)
            UserDefaults.standard.set(data, forKey: userDefaultKey_airports)
        }
        UserDefaults.standard.synchronize()
    }

    class func load_introducton() {
        if let data: Data = UserDefaults.standard.data(forKey: userDefaultKey_skipIntroduction) {
            skipIntroduction = NSKeyedUnarchiver.unarchiveObject(with: data) as? Bool
            
        } else {
            skipIntroduction = false
        }
    }

    class func load_searchInfo() {
        if let data: Data = UserDefaults.standard.data(forKey: userDefaultKey_searchInfo) {
            searchInfo = NSKeyedUnarchiver.unarchiveObject(with: data) as? SearchInfo

        } else { // initilize default searchInfo
            searchInfo = SearchInfo()
        }
    }

    class func load_airports() {
        if let data: Data = UserDefaults.standard.data(forKey: userDefaultKey_airports) {
            airports = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Airport]
        }
    }
}

class CheckBox: UIButton {
    
    // Images
    var checkedImage   : UIImage = UIImage(named: "ic_check_box")!
    var uncheckedImage : UIImage = UIImage(named: "ic_check_box_outline")!
    
    // Bool property
    var isChecked: Bool = true {
        didSet {
            if isChecked {
                self.setImage(checkedImage,   for: .normal)
                self.setTitleColor(UIColor.white, for: .normal)
            } else {
                self.setImage(uncheckedImage, for: .normal)
                self.setTitleColor(UIColor(r: 0, g: 0, b: 0, a: 0.25), for: .normal)
            }
        }
    }

    init(text: String) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitle(text, for: .normal)
        self.titleLabel?.font = UIFont(name: fontName, size: 14)
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0)
        self.imageView?.contentMode = .scaleAspectFit
        self.imageEdgeInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        self.clipsToBounds = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

struct DeviceSize {
    
    static let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
    static let navBarHeight:    CGFloat = UINavigationController().navigationBar.frame.size.height
    
    static func bounds() -> CGRect {
        return UIScreen.main.bounds
    }
    
    static func screenWidth() -> CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    static func screenHeight() -> CGFloat {
        return UIScreen.main.bounds.size.height
    }
}

enum DateFormat: String {
    case YYYY_MM_DD        = "yyyy-MM-dd"
    case YYYY_MM_DD_T_HHmm = "yyyy-MM-dd'T'HH:mm"
    case YYYYMMDDTHHmm     = "yyyy/MM/dd HH:mm"
    case YYYYMMDD          = "yyyy/MM/dd"
    case Symbol           // "Jun 2 Fri"
}

extension Date {
    
    func convertToString(dateFormat: DateFormat) -> String {
        let formatter: DateFormatter = DateFormatter()
        if dateFormat == DateFormat.Symbol {
            return DateUtils.dateToString(date: self)!
        }
        formatter.dateFormat = dateFormat.rawValue
        return formatter.string(from: self)
    }
}

extension String {
    
    func convertToDate(dateFormat: DateFormat) -> Date? {
        let formatter: DateFormatter = DateFormatter()
        if dateFormat != .Symbol {
            formatter.dateFormat = dateFormat.rawValue
        }
        return formatter.date(from: self)
    }
}

class DateUtils {
    
    // String → Date -------------------------------
    
    // "yyyy-MM-dd" → yyyy-MM-dd
    class func dateFromStringWithoutTime(string: String) -> Date {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: string)!
    }
    
    // "yyyy-MM-dd'T'HH:mm" → yyyy-MM-dd HH:mm:SS +0000
    class func dateFromStringWithTime(string: String) -> Date {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        return formatter.date(from: string)!
    }
    
    // Date → String -------------------------------
    
    class func stringFromDateWithTime(date: Date) -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        return formatter.string(from: date)
    }
    
    class func stringFromDateWithoutTime(date: Date) -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter.string(from: date)
    }
    
    class func getDuration(departDate: Date, arriveDate: Date) -> String {
        let span: NSInteger = NSInteger(arriveDate.timeIntervalSince(departDate))
        let m: Int = (span / 60) % 60
        let h: Int = (span / 3600)
        var duration: String = "\(h)hr"
        if m != 0 {
            duration = "\(duration) \(m)m"
        }
        return duration
    }
    
    class func durationFormat(duration: String) -> String {
        let arr: [String] = duration.components(separatedBy: ":")
        let h: String = arr[0]
        let m: String = arr[1]
        var duration: String = "\(h)hr"
        if m != "00" {
            duration = "\(duration) \(m)m"
        }
        return duration
    }
    
    class func dateToString(date: Date?) -> String? {
        if let _date = date {
            let calendar      : Calendar = Calendar.current
            let month         : Int      = calendar.component(.month,   from: _date)
            let day           : Int      = calendar.component(.day,     from: _date)
            let weekday       : Int      = calendar.component(.weekday, from: _date)
            let month_symbol  : String   = calendar.shortStandaloneMonthSymbols[month - 1]      // "Aug"
            let weekday_symbol: String   = calendar.shortStandaloneWeekdaySymbols[weekday - 1]  // "Sun"
            return "\(month_symbol) \(day) \(weekday_symbol)"                                   // "Sun 01 Aug"
        }
        return nil
    }
}

func checkReachability(host_name:String) -> Bool {
    let reachability = SCNetworkReachabilityCreateWithName(nil, host_name)!
    var flags = SCNetworkReachabilityFlags.connectionAutomatic
    if !SCNetworkReachabilityGetFlags(reachability, &flags) {
        return false
    }
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    return (isReachable && !needsConnection)
}

extension UIAlertController {

    // show alert
    func show(vc: UIViewController, dismiss: Bool = true) {

        vc.present(self, animated: true) { () -> Void in

            // dismiss the alert in 1 sec
            if dismiss {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                    vc.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}


func estimateFrame(_ string: String, fontSize: CGFloat, size: CGSize) -> CGRect {

    let attributes: [String : Any] = [NSFontAttributeName : UIFont(name: fontName, size: fontSize)!]
    let attributedString: NSAttributedString = NSAttributedString(string: string, attributes: attributes)
    var rect: CGRect = attributedString.boundingRect(with: size, options: .usesLineFragmentOrigin, context: nil)
    let line = CGFloat(rect.height / fontSize)
    // TODO: calculate lines exactory
    rect.size.height = rect.height + (line * fontSize / 1.5)

    return rect
}
