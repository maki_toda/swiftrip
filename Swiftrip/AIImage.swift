//
//  AIImage.swift
//  Swiftrip
//
//  Created by maki on 2017-06-22.
//  Copyright © 2017 maki. All rights reserved.
//

import Foundation

class AIImage: UIView {

    init(size: CGFloat) {
        super.init(frame: CGRect(x: 0, y: 0, width: size, height: size))

        let eyeLeft : UIView = setEye(x: 36)
        let eyeRight: UIView = setEye(x: 69)

        let image: UIView = UIImageView(image: UIImage(named: "AI"))
            image.frame = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
            image.contentMode = .scaleAspectFill
            image.clipsToBounds = true
            image.layer.cornerRadius = self.bounds.width / 2
            image.layer.shadowColor = UIColor.black.cgColor
            image.layer.shadowOffset = CGSize(width: 0, height: 4)
            image.layer.shadowOpacity = 0.2
            image.layer.shadowRadius = 4.0
            image.layer.masksToBounds = false

        // bounce animation
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "bounds.origin.y")
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
            animation.fromValue = 0
            animation.toValue   = -20
            animation.duration  = 1
            animation.autoreverses = true
            animation.repeatCount  = Float.infinity
            animation.isRemovedOnCompletion = false
            animation.fillMode = kCAFillModeForwards

        self.layer.add(animation, forKey: nil)
        self.addSubview(image)
        self.addSubview(eyeLeft)
        self.addSubview(eyeRight)
    }

    func setEye(x: CGFloat) -> UIView {

        // eye size setting
        let eye_open  = CGSize(width: 16, height: 38)
        let eye_close = CGSize(width: 16, height: 2)

        // radius
        let cornerRadiusAnimation: CABasicAnimation = CABasicAnimation(keyPath: "cornerRadius")
            cornerRadiusAnimation.repeatDuration = 0.1
            cornerRadiusAnimation.duration  = 0.2
            cornerRadiusAnimation.fromValue = eye_open.width / 2
            cornerRadiusAnimation.toValue   = eye_open.height / 2

        // size
        let sizeAnimation: CABasicAnimation = CABasicAnimation(keyPath: "bounds.size")
            sizeAnimation.fromValue = NSValue(cgSize: eye_open)
            sizeAnimation.toValue   = NSValue(cgSize: eye_close)
            sizeAnimation.duration  = 0.2

        // eye blink animationGroup
        let eyeAnimationGroup: CAAnimationGroup = CAAnimationGroup()
            eyeAnimationGroup.setValue("group-animation1", forKey: "animationName")
            eyeAnimationGroup.animations   = [cornerRadiusAnimation, sizeAnimation]
            eyeAnimationGroup.duration     = 3
            eyeAnimationGroup.autoreverses = true
            eyeAnimationGroup.repeatCount  = Float.infinity
            eyeAnimationGroup.isRemovedOnCompletion = false
            eyeAnimationGroup.fillMode = kCAFillModeForwards
            eyeAnimationGroup.delegate     = self as? CAAnimationDelegate

        let eye: UIView = UIView(frame: CGRect(origin: CGPoint(x: x, y: 31), size: eye_open))
            eye.layer.add(eyeAnimationGroup, forKey: "eyeAnimation")
            eye.layer.backgroundColor = UIColor(r: 0, g: 138, b: 255, a: 1).cgColor
            eye.layer.masksToBounds = false
            eye.layer.cornerRadius = eye_open.width / 2
        return eye
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
