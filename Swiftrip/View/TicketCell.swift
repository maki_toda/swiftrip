//
//  TicketCell.swift
//  Swiftrip
//
//  Created by maki on 2017-04-28.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class TicketCell: BaseCollectionViewCell {
    
    static let cellId: String = "TicketCellID"
    let cell_height         : CGFloat = 340
    let cell_height_expand  : CGFloat = 800

    var ticket: Ticket!             // this will be defined when ResultCell was initilized in TicketController
    var withFlightDetail: Bool!     // this will be defined when ResultCell was initilized in TicketController

    private var lastVerticalAnchor: NSLayoutYAxisAnchor!
    let margin: CGFloat = 16
    static let limit: Int = 7

    let baseLayoutBar: UIView = {
        var o: UIView = UIView()
            o.backgroundColor = .white
        return o
    }()

    let separatorLine: UIView = {
        let o: UIView = UIView()
            o.backgroundColor = .superLightGray
        return o
    }()

    let circles: [UIView] = {
        var o: [UIView] = []
        for i in 0...limit {
            o.append(TicketView.circle())
        }
        return o
    }()
    
    let airportNames: [UILabel] = {
        var o: [UILabel] = []
        for i in 0...limit {
            o.append(UILabel(fontSize: 12, .lightGray, textAlign: .left))
        }
        return o
    }()
    
    let lines: [UIView] = {
        var o: [UIView] = []
        for i in 0...2 {
            o.append(TicketView.line())
        }
        return o
    }()

    let lab_price         : UILabel     = UILabel(fontSize: 28, .darkGray,  textAlign: .right)
    let lab_priceDetail   : UILabel     = UILabel(fontSize: 12, .lightGray, textAlign: .right)
    var lab_outbound_stop : UILabel     = UILabel(fontSize: 12, .lightGray, textAlign: .right)
    var lab_inbound_stop  : UILabel     = UILabel(fontSize: 12, .lightGray, textAlign: .right)
    var btn_booking       : UIButton    = TicketView.booking()
    var btn_share         : UIButton    = TicketView.share()
    var img_airlineLogo   : UIImageView = TicketView.airlineLogo()
    
    override func setupView() {
        
        // Background
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .white
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.layout(top: self.topAnchor, left: self.leftAnchor)
        self.contentView.layer.shadowColor = UIColor.lightGray.cgColor
        self.contentView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.contentView.layer.shadowOpacity = 0.3
        self.contentView.layer.shadowRadius = 2.0
        self.contentView.layer.cornerRadius = 4.0
        self.contentView.layer.masksToBounds = true
        self.contentView.clipsToBounds = false
    }
    
    public func setupCell(ticket: Ticket, withFlightDetail: Bool) {

        self.ticket = ticket
        self.withFlightDetail = withFlightDetail
        
        // clear
        for subview: UIView in contentView.subviews {
            subview.removeFromSuperview()
        }

        self.header()
        self.itinerary()
        self.footer()
    }
    
    fileprivate func header() {

        // set data
        if let airlineLogo: String = self.ticket.itineraries[0].outbound.flights[0].marketing_airline {
            self.img_airlineLogo.image = UIImage(named: airlineLogo)
        }

        if let price: NSNumber = self.ticket.fare?.total_price {
            self.lab_price.text = "$\(Int(round(price as! Double)))"
        }
        
        // add subView
        self.contentView.addSubview(self.baseLayoutBar)
        self.contentView.addSubview(self.separatorLine)
        self.contentView.addSubview(img_airlineLogo)
        self.contentView.addSubview(lab_price)

        // set layout
        self.baseLayoutBar.layout(top: self.contentView.topAnchor, // base width for autosizing
                                  left: self.contentView.leftAnchor,
                                  right: self.contentView.rightAnchor,
                                  widthConstant: DeviceSize.screenWidth() - (margin * 2), heightConstant: 3)

        self.separatorLine.layout(top: self.baseLayoutBar.topAnchor,
                                  left: self.baseLayoutBar.leftAnchor,
                                  right: self.baseLayoutBar.rightAnchor,
                                  heightConstant: 1)

        self.lab_price.layout(top: self.baseLayoutBar.topAnchor, topConstant: margin,
                              right: self.baseLayoutBar.rightAnchor, rightConstant: margin)

        self.img_airlineLogo.layout(centerY: self.lab_price.centerYAnchor,
                                    left: self.baseLayoutBar.leftAnchor, leftConstant: margin,
                                    widthConstant: 80, heightConstant: 30)

        self.lastVerticalAnchor = self.lab_price.bottomAnchor
    }
    
    fileprivate func itinerary() {

        let itinerary: Itinerary = self.ticket.itineraries[0]
        
        // index
        var i: Int = 0

        // outbound stop
        self.stop(self.lab_outbound_stop, bound: itinerary.outbound)

        // outbound airport, flightDate
        for (index, flight) in itinerary.outbound.flights.enumerated() {
            if index == 0 {
                airport(i: i, airport: flight.origin)
            }
            self.flightDate(i: i, flight, depart: true)
            self.airport(i: i + 1, airport: flight.destination)
            
            if withFlightDetail {
                flightDate(i: i, flight, depart: false)
            }
            i += 1
        }
        verticalRouteline(i: 0, topAnchor: self.circles[0].centerYAnchor, bottomAnchor: self.circles[i].centerYAnchor)
        
        // inbound
        if let inbound: Bound = itinerary.inbound {

            self.separator()

            self.stop(self.lab_inbound_stop, bound: inbound)

            i += 1

            let firstCircleOfInbound: Int = i // for height of verticalRouteLine

            for (index, flight) in inbound.flights.enumerated() {
                if index == 0 {
                    airport(i: i, airport: flight.origin)
                }
                self.flightDate(i: i, flight, depart: true)
                self.airport(i: i + 1, airport: flight.destination)
                if withFlightDetail {
                    flightDate(i: i, flight, depart: false)
                }
                i += 1
            }
            self.verticalRouteline(i: 1, topAnchor: circles[firstCircleOfInbound].centerYAnchor, bottomAnchor: circles[i].centerYAnchor)
        }
    }
    
    fileprivate func verticalRouteline(i: Int, topAnchor: NSLayoutYAxisAnchor, bottomAnchor: NSLayoutYAxisAnchor) {

        self.contentView.addSubview(lines[i])
        self.contentView.sendSubview(toBack: lines[i])
        self.lines[i].layout(centerX: circles[0].centerXAnchor, top: topAnchor, bottom: bottomAnchor, widthConstant: 2)
    }
    
    fileprivate func separator() {

        let view_separater : UIImageView = TicketView.separater()
        self.contentView.addSubview(view_separater)

        view_separater.layout(top: self.lastVerticalAnchor, topConstant: margin,
                              left: self.img_airlineLogo.leftAnchor,
                              right: self.baseLayoutBar.rightAnchor, rightConstant: margin,
                              heightConstant: 1)

        self.lastVerticalAnchor = view_separater.bottomAnchor
    }
    
    fileprivate func stop(_ label_stop: UILabel, bound: Bound) {

        // set stop
        let stop: Int = bound.flights.count
        let stopText: String = {
            if stop <= 1 {
                return "Direct"
            } else {
                return "\(stop - 1) stop"
            }
        }()
        
        // set duration
        var durationText: String
        if let duration = bound.duration {
            durationText = DateUtils.durationFormat(duration: duration)
            label_stop.text = "\(stopText)・\(durationText)"
            
        } else if
            let departDate: Date = bound.flights[0].departs_at,
            let arriveDate: Date = bound.flights[bound.flights.count - 1].arrives_at {
            durationText = DateUtils.getDuration(departDate: departDate, arriveDate: arriveDate)
            label_stop.text = "\(stopText)・\(durationText)"
            
        } else {
            label_stop.text = "\(stopText)"
        }
        
        self.contentView.addSubview(label_stop)

        label_stop.layout(top: self.lastVerticalAnchor, topConstant: 8, right: lab_price.rightAnchor)

        // set lastObj
        self.lastVerticalAnchor = label_stop.bottomAnchor
    }
    
    fileprivate func airport(i: Int, airport: Airport) {
        // set data source
        let airportCode: UILabel = UILabel(fontSize: 16, .primary, textAlign: .left)

        airportCode.text = airport.airportCode
        self.airportNames[i].text = airport.airportName
        
        // add subView
        self.contentView.addSubview(self.circles[i])
        self.contentView.addSubview(airportCode)
        self.contentView.addSubview(self.airportNames[i])

        // set layout
        self.circles[i].layout(centerY: airportCode.centerYAnchor, left: self.baseLayoutBar.leftAnchor, leftConstant: 24)

        airportCode.layout(top: self.lastVerticalAnchor, topConstant: 8, left: self.circles[i].rightAnchor, leftConstant: 24)
        
        self.airportNames[i].layout(centerY: airportCode.centerYAnchor,
                                    left: self.baseLayoutBar.leftAnchor, leftConstant: 92,
                                    right: self.baseLayoutBar.rightAnchor, rightConstant: margin)
        
        // set lastObj
        self.lastVerticalAnchor = airportCode.bottomAnchor
    }
    
    fileprivate func flightDate(i: Int, _ flight: Flight, depart: Bool) {
        
        if depart {

            let departIcon: UIImageView = TicketView.flightIcon(imgName: "ic_flight_takeoff_48pt")
            let departDate: UILabel = UILabel()
                departDate.font = UIFont(name: departDate.font.fontName, size: 12)
                departDate.textColor = .darkGray

            self.contentView.addSubview(departIcon)
            self.contentView.addSubview(departDate)

            departIcon.layout(centerY: departDate.centerYAnchor,
                              right: self.airportNames[0].leftAnchor, rightConstant: 12,
                              widthConstant: 20, heightConstant: 20)

            departDate.layout(top: self.lastVerticalAnchor, topConstant: 8,
                              left: self.airportNames[0].leftAnchor)

            if withFlightDetail {

                let flightNumber: UILabel = UILabel()
                    flightNumber.font = UIFont(name: flightNumber.font.fontName, size: 11)
                    flightNumber.textColor = .lightGray

                let reminingSheet: UILabel = UILabel(fontSize: 11, .lightGray, textAlign: .left)

                departDate.text    = flight.departs_at?.convertToString(dateFormat: .YYYYMMDDTHHmm)
                reminingSheet.text = "\(String(flight.booking_info!.seats_remaining!)) seats"
                flightNumber.text  = "\(flight.operating_airline!)\(flight.flight_number!)・\(flight.booking_info!.travel_class!)"

                self.contentView.addSubview(reminingSheet)
                self.contentView.addSubview(flightNumber)

                reminingSheet.layout(centerY: departDate.centerYAnchor, right: self.lab_price.rightAnchor)
                flightNumber.layout(top: departDate.bottomAnchor, topConstant: 4, left: self.airportNames[0].leftAnchor)
                
                // set lastObj
                self.lastVerticalAnchor = flightNumber.bottomAnchor

            } else {
                departDate.text = flight.departs_at?.convertToString(dateFormat: .YYYYMMDD)

                // set lastObj
                self.lastVerticalAnchor = departIcon.bottomAnchor
            }

        // arrive
        } else {

            let arriveIcon: UIImageView = TicketView.flightIcon(imgName: "ic_flight_land_48pt")
            let arriveDate: UILabel = UILabel()
                arriveDate.font = UIFont(name: arriveDate.font.fontName, size: 12)
                arriveDate.textColor = .darkGray

            // set data source
            arriveDate.text = flight.arrives_at?.convertToString(dateFormat: .YYYYMMDDTHHmm)

            // add subView
            self.contentView.addSubview(arriveIcon)
            self.contentView.addSubview(arriveDate)

            // set layout
            arriveIcon.layout(centerY: arriveDate.centerYAnchor,
                              right: self.airportNames[0].leftAnchor, rightConstant: 12,
                              widthConstant: 20, heightConstant: 20)

            arriveDate.layout(top: self.lastVerticalAnchor, topConstant: 8,
                              left: self.airportNames[0].leftAnchor)
            
            // set lastObj
            self.lastVerticalAnchor = arriveDate.bottomAnchor
        }
    }
    
    fileprivate func footer() {
        
        if withFlightDetail {
            
            let text: String = {
                var text: [String] = []
                if let price_adult: NSNumber = self.ticket.fare?.price_per_adult?.total_price {
                    text.append("adult $\(price_adult.stringValue)")
                }
                if let price_child: NSNumber = self.ticket.fare?.price_per_child?.total_price {
                    text.append("child $\(price_child.stringValue)")
                }
                if let price_infant: NSNumber = self.ticket.fare?.price_per_infant?.total_price {
                    text.append("infant $\(price_infant.stringValue))")
                }
                return text.joined(separator: "・")
            }()

            if self.lab_priceDetail.text != "" {

                self.lab_priceDetail.text = text
                self.contentView.addSubview(self.lab_priceDetail)

                self.lab_priceDetail.layout(top: self.lastVerticalAnchor, topConstant: 16,
                                            right: self.lab_price.rightAnchor)

                self.lastVerticalAnchor = self.lab_priceDetail.bottomAnchor
            }
        }
        
        // set text of booking button
        if self.ticket.apiName != API_NAME.affiliateSearch {
            self.btn_booking.setTitle("Booking from Jetradar", for: .normal)
        }
        
        // add subView
        self.contentView.addSubview(btn_booking)
        //self.contentView.addSubview(btn_share) // TODO: set Firebase login
        
        // set layout
//        self.btn_share.centerXAnchor.constraint(equalTo: self.circles[0].centerXAnchor).isActive = true
//        self.btn_share.centerYAnchor.constraint(equalTo: self.btn_booking.centerYAnchor).isActive = true
//        self.btn_share.widthAnchor.constraint(equalToConstant: 16).isActive = true
//        self.btn_share.heightAnchor.constraint(equalToConstant: 16).isActive = true
        self.btn_booking.layout(top: self.lastVerticalAnchor, topConstant: 16,
                                bottom: self.contentView.bottomAnchor, bottomConstant: margin,
                                right: self.baseLayoutBar.rightAnchor, rightConstant: margin)
    }
}
