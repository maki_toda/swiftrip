//
//  FilterCell.swift
//  Swiftrip
//
//  Created by maki on 2017-05-23.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class FilterCell: BaseTableViewCell {

    // MEMO:    this keys is for recognizing the button of origin or destination
    //          when coming from SearchAirportController
    static let originAirportKey     : String = "originAirport"
    static let destinationAirportKey: String = "destinationAirport"
    
    var destinationAirport: Airport?
    var searchAirport: SearchAirport?
    var searchButtonIsActive: Bool = false {
        willSet {
            if !searchButtonIsActive && newValue {
                print("searchButtonIsActive is true")
                self.btn_search.isEnabled = true
                self.btn_search.alpha = 1
            }

            if searchButtonIsActive && !newValue {
                print("searchButtonIsActive is false")
                btn_search.isEnabled = false
                self.btn_search.alpha = 0.5
            }
        }
    }
    
    // data sources for pickerViews
    let data_passengerPicker: [String] = {
        var array: [String] = []
        for i in 0...20 {
            array.append(String(i))
        }
        return array
    }()
    let data_classPicker: [String] = ["Economy", "Bussiness", "First"]

    // data source for dataPicker
    var toolBar_depart: UIToolbar!
    var toolBar_return: UIToolbar!
    var datePicker_depart: UIDatePicker!
    var datePicker_return: UIDatePicker!
    
    override func setupView() {
        super.setupView()
        self.backgroundColor = .primary
        self.layer.zPosition = -1

        setupDatePicker()
//        setupPickerView()
        setUpViewObjects()
        setDelegateTextField()
        setupFromSearchInfo()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // expanding & shrinking filterCell with animation
        if #available(iOS 10, *) {
            UIView.animate(withDuration: 1) {
                self.contentView.layoutIfNeeded()
            }
        }
    }
    
    // set selected airport from SearchAirport
    func setupAirportFromSearchAirport(target: String, airport: Airport) {

        if target == "origin" {
            btn_originCode.setTitle(airport.airportCode, for: .normal)
            lab_originCity.text = airport.cityName
            searchInfo.origin = airport

        } else { // destination Airport
            btn_destinationCode.setTitle(airport.airportCode, for: .normal)
            lab_destinationCity.text = airport.cityName
            searchInfo.destination = airport
        }
    }
    
    public func setupFromSearchInfo() {

        // one way
        if searchInfo.oneWay {
            self.seg_bound.selectedSegmentIndex = 1
        } else {
            self.seg_bound.selectedSegmentIndex = 0
        }

        let options: [UIView] = [self.lab_caption_departure, self.lab_caption_return,
                                 self.tf_depatureDate, self.tf_returnDate,
                                 self.btn_stop_direct, self.btn_stop_1stop, self.btn_stop_2stop,
                                 self.img_adult, self.img_child, self.img_infant,
                                 self.lab_caption_passenger, self.pic_passenger,
                                 self.lab_caption_class, self.pic_class]

        // set each value from searchInfo
        if let origin: Airport = searchInfo.origin {
            self.btn_originCode.setTitle(origin.airportCode, for: .normal)
            self.lab_originCity.text = origin.cityName
        }

        if let destination: Airport = searchInfo.destination {
            self.btn_destinationCode.setTitle(destination.airportCode, for: .normal)
            self.lab_destinationCity.text = destination.cityName

            // if destination was defined returnDate is enable
            options.forEach({
                $0.alpha = 1
                $0.isUserInteractionEnabled = true
            })

            // returnDate (hiding if oneway is true)
            if searchInfo.oneWay { // oneway
                self.lab_caption_return.alpha = 0
                self.tf_returnDate.alpha = 0
                self.tf_returnDate.isUserInteractionEnabled = false
            }

        } else { // hiding options
            options.forEach({
                $0.alpha = 0
                $0.isUserInteractionEnabled = false
            })
        }

        // departDate
        if let departDate: Date = searchInfo.departDate {
            self.tf_depatureDate.text = DateUtils.dateToString(date: departDate)
        }

        // returnDate
        if let returnDate: Date = searchInfo.returnDate {
            self.tf_returnDate.text = DateUtils.dateToString(date: returnDate)
        }

        // budget
        if let budget: NSNumber = searchInfo.budget {
            self.lab_budget.setupText(budget.floatValue)
            self.slider_budget.setValue(budget.floatValue, animated: true)

        } else {
            self.slider_budget.setValue(budget_default, animated: true)
        }

        // stop
        switch searchInfo.stop {
        case 0:
            self.btn_stop_direct.isChecked = true
            self.btn_stop_1stop.isChecked  = false
            self.btn_stop_2stop.isChecked  = false
        case 1:
            self.btn_stop_direct.isChecked = true
            self.btn_stop_1stop.isChecked  = true
            self.btn_stop_2stop.isChecked  = false
        default:
            self.btn_stop_direct.isChecked = true
            self.btn_stop_1stop.isChecked  = true
            self.btn_stop_2stop.isChecked  = true
        }

//        // passenger
//        if let adult: Int = searchInfo.adult {
//            self.pic_passenger.selectRow(adult, inComponent: 0, animated: false)
//        }
//        if let child: Int = searchInfo.child {
//            self.pic_passenger.selectRow(child, inComponent: 1, animated: false)
//        }
//        if let infant: Int = searchInfo.infant {
//            self.pic_passenger.selectRow(infant, inComponent: 2, animated: false)
//        }
//
//        // cabin class
//        if let cabin_class: Int = searchInfo.cabin_class {
//            self.pic_class.selectRow(cabin_class, inComponent: 0, animated: false)
//        }

        self.validate()
    }

    //  -----------------------------------------------------
    static let sideMargin: CGFloat = 32
    static let widthFor1Column: CGFloat  = DeviceSize.screenWidth() - (sideMargin * 2)
    static let widthFor2Columns: CGFloat = widthFor1Column / 2
    static let widthFor3Columns: CGFloat = widthFor1Column / 3
    
    var btn_originCode          : UIButton      = UIButton(fontSize: 32, .white, text: "?")
    var btn_destinationCode     : UIButton      = UIButton(fontSize: 32, .white, text: "?")
    var btn_compare             : UIButton      = UIButton(imgName: "ic_airplanes", color: nil)
    var lab_originCity          : UILabel       = UILabel(fontSize: 12, .white, text: "Origin", textAlign: .center)
    var lab_destinationCity     : UILabel       = UILabel(fontSize: 12, .white, text: "Destination", textAlign: .center)
    var lab_caption_departure   : UILabel       = UILabel(fontSize: 11, .white, text: "Departure", textAlign: .left)
    var lab_caption_return      : UILabel       = UILabel(fontSize: 11, .white, text: "Return", textAlign: .left)
    var lab_caption_budget      : UILabel       = UILabel(fontSize: 11, .white, text: "Budget", textAlign: .left)
    var tf_depatureDate         : UITextField   = UITextField(fontSize: 18, .white, placeholder: nil, textAlign: .left)
    var tf_returnDate           : UITextField   = UITextField(fontSize: 18, .white, placeholder: nil, textAlign: .left)
    var lab_budget              : BudgetLabel   = BudgetLabel(fontSize: 20)
    var seg_bound               : UISegmentedControl = {
        let o: UISegmentedControl = Utility.initSegmentedControl(fontSize: 14, .clear, segments: ["Round Trip", "One Way"])
            o.selectedSegmentIndex = 0
        return o
    }()
    var lab_caption_passenger   : UILabel       = UILabel(fontSize: 11, .white, text: "Passenger", textAlign: .left)
    var pic_passenger           : UIPickerView    = {
        let o: UIPickerView = UIPickerView()
            o.backgroundColor = .clear
        return o
    }()
    var img_adult               : UIImageView   = UIImageView(imgName: "icon_adult",  color: UIColor.white.withAlphaComponent(0.5))
    var img_child               : UIImageView   = UIImageView(imgName: "icon_child",  color: UIColor.white.withAlphaComponent(0.5))
    var img_infant              : UIImageView   = UIImageView(imgName: "icon_infant", color: UIColor.white.withAlphaComponent(0.5))
    var lab_caption_class       : UILabel       = UILabel(fontSize: 11, .white, text: "Class", textAlign: .left)
    var pic_class               : UIPickerView    = {
        let o: UIPickerView = UIPickerView()
            o.backgroundColor = .clear
        return o
    }()
    var btn_stop_direct: CheckBox = CheckBox(text: "Direct")
    var btn_stop_1stop : CheckBox = CheckBox(text: "1 stop")
    var btn_stop_2stop : CheckBox = CheckBox(text: "2 stop")

    var btn_search: UIButton = { () -> UIButton in
        let o: UIButton = UIButton(imgName: "ic_search", color: nil)
            o.backgroundColor = .sub
            o.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
            o.layer.shadowOffset = CGSize(width: 0, height: 4)
            o.layer.shadowOpacity = 1.0
            o.layer.shadowRadius = 4.0
            o.layer.cornerRadius = 20.0
            o.layer.masksToBounds = false
            o.contentMode = .scaleAspectFit
            o.clipsToBounds = false
        return o
    }()
    
    var slider_budget: UISlider = { () -> UISlider in
        let o: UISlider = UISlider()
            o.isContinuous = true
            o.tintColor = UIColor.white
            o.thumbTintColor = UIColor.white
            o.minimumValue = budget_min
            o.maximumValue = budget_max
            o.maximumTrackTintColor = UIColor(r: 255, g: 255, b: 255, a: 0.3)
        return o
    }()

    func setUpViewObjects() {
        self.contentView.addSubview(btn_originCode)
        self.contentView.addSubview(btn_destinationCode)
        self.contentView.addSubview(btn_compare)
        self.contentView.addSubview(lab_originCity)
        self.contentView.addSubview(lab_destinationCity)
        self.contentView.addSubview(seg_bound)
        self.contentView.addSubview(lab_caption_departure)
        self.contentView.addSubview(lab_caption_return)
        self.contentView.addSubview(tf_depatureDate)
        self.contentView.addSubview(tf_returnDate)
        self.contentView.addSubview(btn_stop_direct)
        self.contentView.addSubview(btn_stop_1stop)
        self.contentView.addSubview(btn_stop_2stop)
        self.contentView.addSubview(lab_caption_budget)
        self.contentView.addSubview(lab_budget)
        self.contentView.addSubview(slider_budget)
//        self.contentView.addSubview(lab_caption_passenger)
//        self.contentView.addSubview(img_adult)
//        self.contentView.addSubview(img_child)
//        self.contentView.addSubview(img_infant)
//        self.contentView.addSubview(pic_passenger)
//        self.contentView.addSubview(lab_caption_class)
//        self.contentView.addSubview(pic_class)
        self.contentView.addSubview(btn_search)

        // MARK: - set auto layout

        // origin & destination
        self.btn_originCode.layout(top: self.topAnchor, left: self.leftAnchor, leftConstant: 32, right: self.btn_compare.leftAnchor)
        self.btn_destinationCode.layout(top: self.topAnchor, left: self.btn_compare.rightAnchor, right: self.rightAnchor, rightConstant: 32)
        self.btn_compare.layout(centerX: self.centerXAnchor, centerY: self.btn_originCode.centerYAnchor, widthConstant: 30, heightConstant: 30)
        self.lab_originCity.layout(top: self.btn_originCode.bottomAnchor, left: self.leftAnchor, leftConstant: 32, right: self.btn_compare.leftAnchor)
        self.lab_destinationCity.layout(top: self.btn_destinationCode.bottomAnchor, left: self.btn_compare.rightAnchor, right: self.rightAnchor, rightConstant: 32)

        // round trip or one way
        self.seg_bound.layout(top: self.lab_originCity.bottomAnchor, topConstant: 24, left: self.leftAnchor, leftConstant: 32, right: self.rightAnchor, rightConstant: 32, heightConstant: 32)

        // dates
        self.lab_caption_departure.layout(top: self.seg_bound.bottomAnchor, topConstant: 24, left: self.leftAnchor, leftConstant: 32)
        self.lab_caption_return.layout(top: self.seg_bound.bottomAnchor, topConstant: 24, left: self.centerXAnchor, leftConstant: 8)
        self.tf_depatureDate.layout(top: self.lab_caption_departure.bottomAnchor, left: self.lab_caption_departure.leftAnchor, right: self.centerXAnchor, rightConstant: 8, heightConstant: 32)
        self.tf_returnDate.layout(top: self.lab_caption_return.bottomAnchor, left: self.centerXAnchor, leftConstant: 8, right: self.rightAnchor, rightConstant: 32, heightConstant: 32)

        // budget
        self.lab_caption_budget.layout(top: self.lab_budget.topAnchor, left: self.leftAnchor, leftConstant: 32)
        self.lab_budget.layout(top: self.tf_depatureDate.bottomAnchor, topConstant: 16, left: self.leftAnchor, leftConstant: 32, right: self.rightAnchor, rightConstant: 32)
        self.slider_budget.layout(top: self.lab_budget.bottomAnchor, topConstant: 8, left: self.leftAnchor, leftConstant: 32, right: self.rightAnchor, rightConstant: 32)

        // stop
        self.btn_stop_direct.layout(top: self.slider_budget.bottomAnchor, topConstant: 8, left: self.leftAnchor, leftConstant: 20, widthConstant: FilterCell.widthFor3Columns, heightConstant: 36)
        self.btn_stop_1stop.layout(centerX: self.centerXAnchor, top: self.btn_stop_direct.topAnchor, widthConstant: FilterCell.widthFor3Columns, heightConstant: 36)
        self.btn_stop_2stop.layout(top: self.btn_stop_direct.topAnchor, right: self.rightAnchor, rightConstant: 20, widthConstant: FilterCell.widthFor3Columns, heightConstant: 36)

//        // passenger
//        self.lab_caption_passenger.layout(top: self.btn_stop_direct.bottomAnchor, topConstant: 12, left: self.leftAnchor, leftConstant: 32)
//        self.pic_passenger.layout(centerY: self.img_adult.centerYAnchor, left: self.lab_caption_passenger.leftAnchor, widthConstant: 156, heightConstant: 88)
//        self.img_adult.layout(top: self.lab_caption_passenger.bottomAnchor, topConstant: 20, left: self.leftAnchor, leftConstant: 28, widthConstant: 24, heightConstant: 24)
//        self.img_child.layout(centerY: self.img_adult.centerYAnchor, left: self.pic_passenger.leftAnchor, leftConstant: 52, widthConstant: 24, heightConstant: 24)
//        self.img_infant.layout(centerY: self.img_adult.centerYAnchor, left: self.pic_passenger.leftAnchor, leftConstant: 104, widthConstant: 24, heightConstant: 24)
//
//        // cabin class
//        self.lab_caption_class.layout(top: self.lab_caption_passenger.topAnchor, left: self.pic_class.leftAnchor)
//        self.pic_class.layout(centerY: self.img_adult.centerYAnchor, left: self.pic_passenger.rightAnchor, leftConstant: 4, right: self.rightAnchor, rightConstant: 32, heightConstant: 88)

        // search button
//        self.btn_search.layout(top: self.pic_passenger.bottomAnchor, left: self.leftAnchor, leftConstant: 32, right: self.rightAnchor, rightConstant: 32, heightConstant: 48)
        self.btn_search.layout(top: self.btn_stop_direct.bottomAnchor, topConstant: 16,
                               left: self.leftAnchor, leftConstant: 32,
                               right: self.rightAnchor, rightConstant: 32, heightConstant: 48)

        // add target
        self.btn_originCode.addTarget(self, action: #selector(clickedAirportCodeButton(_:)), for: .touchUpInside)
        self.btn_destinationCode.addTarget(self, action: #selector(clickedAirportCodeButton(_:)), for: .touchUpInside)
        self.btn_compare.addTarget(self, action: #selector(switchRoutes), for: .touchUpInside)
        self.seg_bound.addTarget(self, action: #selector(clickedBoundSegment), for: .valueChanged)
        self.slider_budget.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        self.btn_stop_direct.addTarget(self, action: #selector(checkBoxClicked(_:)), for: .touchUpInside)
        self.btn_stop_1stop.addTarget(self, action: #selector(checkBoxClicked(_:)), for: .touchUpInside)
        self.btn_stop_2stop.addTarget(self, action: #selector(checkBoxClicked(_:)), for: .touchUpInside)
        self.btn_search.addTarget(self, action: #selector(clickedSearchButton), for: .touchUpInside)
    }

    func clickedAirportCodeButton(_ sender: AnyObject) {

        var originOrDestination: String!

        if sender as! UIButton == self.btn_originCode {
            originOrDestination = "origin"
        } else {
            originOrDestination = "destination"
        }

        if ticketController.expandedCell == .Filter {

            let cv: SearchAirport = SearchAirport()

            // which button user clicked (origin or destination)
            cv.originOrDestination = originOrDestination

            // open SearchAirport
            ticketController.navigationController?.pushViewController(cv, animated: true)
            return
        }

        ticketController.expandedCell = .Filter
    }

    @objc fileprivate func clickedBoundSegment(sender: UISegmentedControl) {

        switch sender.selectedSegmentIndex {
        case 0:  // Round trip
            searchInfo.oneWay = false
            
        default: // One way
            searchInfo.oneWay = true
        }
        self.validate()
    }
    
    func switchRoutes(_ sender: AnyObject) {

        // when the compare arrow is clicked, switching the routes

        if ticketController.expandedCell == .Filter {

            let tmp_originCode: String = btn_originCode.currentTitle!
            let tmp_originCity: String = lab_originCity.text!
            let objects: [UIView] = [self.btn_originCode, self.btn_destinationCode, self.lab_originCity, self.lab_destinationCity]
            
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn,
                           animations: { objects.forEach { $0.alpha = 0 } }, // fade-out

                completion: { (_) -> Void in

                    // switch origin and destination
                    self.btn_originCode.setTitle(self.btn_destinationCode.currentTitle!, for: .normal)
                    self.btn_destinationCode.setTitle(tmp_originCode, for: .normal)
                    self.lab_originCity.text = self.lab_destinationCity.text!
                    self.lab_destinationCity.text = tmp_originCity

                    // fade-in
                    UIView.animate(withDuration: 0.2, animations: {
                        objects.forEach { $0.alpha = 1 }
                    })
            })
            return
        }
        ticketController.expandedCell = .Filter
    }
    
    func sliderChanged() {
        lab_budget.setupText(slider_budget.value)
    }
    
    func checkBoxClicked(_ sender: CheckBox) {
        
        // toggle
        sender.isChecked = !sender.isChecked
        self.validate()
    }

    func validate() {
        if self.btn_originCode.title(for: .normal) == "?"{
            searchButtonIsActive = false
            return
        }

        if self.btn_destinationCode.title(for: .normal) != "?" {
            if seg_bound.selectedSegmentIndex == 0 && (tf_depatureDate.text!.isEmpty || tf_returnDate.text!.isEmpty) {
                searchButtonIsActive = false
                return

            } else if seg_bound.selectedSegmentIndex == 1 && tf_depatureDate.text!.isEmpty {
                searchButtonIsActive = false
                return
            }
        }

        if !btn_stop_direct.isChecked && !btn_stop_1stop.isChecked && !btn_stop_2stop.isChecked {
            searchButtonIsActive = false
            return
        }

        self.searchButtonIsActive = true
    }
    
    func clickedSearchButton() {

        print("search button clicked")
        
        let stop: Int = {
            if btn_stop_direct.isChecked && btn_stop_1stop.isChecked && btn_stop_2stop.isChecked {
                return 2
            } else if btn_stop_direct.isChecked && btn_stop_1stop.isChecked {
                return 1
            } else {
                return 0
            }
        }()

        searchInfo.stop   = stop
        searchInfo.adult  = pic_passenger.selectedRow(inComponent: 0)
        searchInfo.child  = pic_passenger.selectedRow(inComponent: 1)
        searchInfo.infant = pic_passenger.selectedRow(inComponent: 2)
        searchInfo.cabin_class = pic_class.selectedRow(inComponent: 0)
//        minTripDuration: nil
//        maxTripDuration: nil
        searchInfo.budget = lab_budget.convertNumber()

        // save
        let searchInfo_data: Data = NSKeyedArchiver.archivedData(withRootObject: searchInfo)
        UserDefaults.standard.set(searchInfo_data, forKey: userDefaultKey_searchInfo)
        UserDefaults.standard.synchronize()

        // start searching
        ticketController.resultCell?.fetchTickets()
    }
}


// ----------------------------------------------------------
// delegate method of datePickers of departDate & returnDate
// ----------------------------------------------------------

extension FilterCell {
    
    func setupDatePicker() {
        
        // set placeholders
        tf_depatureDate.placeholder = DateUtils.dateToString(date: Date())
        tf_returnDate.placeholder   = DateUtils.dateToString(date: Date(timeInterval: 60*60*24*7, since: Date())) // 7 days later
        
        func setupToolBar(tag: Int) -> UIToolbar {
            let toolBarBtnDone  = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(tappedToolBarBtn(_:)))
                toolBarBtnDone.tag  = tag
            let toolBarBtnToday = UIBarButtonItem(title: "Today", style: .plain, target: self, action: #selector(tappedToolBarBtnToday(_:)))
                toolBarBtnToday.tag = tag
            let toolBarBtnClear = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(clickedToolBarBtnClear(_:)))
                toolBarBtnClear.tag = tag
            let flexibleSpace   = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 48.0))
                toolBar.layer.position = CGPoint(x: self.frame.size.width, y: self.frame.size.height - 24.0)
                toolBar.barStyle = .default
                toolBar.items = [toolBarBtnClear, toolBarBtnToday, flexibleSpace, toolBarBtnDone]
            return toolBar
        }
        
        func setupDatePicker(tag: Int) -> UIDatePicker {
            let datePicker: UIDatePicker = UIDatePicker()
            datePicker.addTarget(self, action: #selector(changedDateEvent(_:)), for: .valueChanged)
            datePicker.datePickerMode = UIDatePickerMode.date
            datePicker.tag = tag
            return datePicker
        }
        
        // setup depart datePicker
        toolBar_depart = setupToolBar(tag: 0)
        datePicker_depart = setupDatePicker(tag: 0)
        tf_depatureDate.inputView = datePicker_depart
        tf_depatureDate.inputAccessoryView = toolBar_depart
        
        // setup return datePicker
        toolBar_return = setupToolBar(tag: 1)
        datePicker_return = setupDatePicker(tag: 1)
        tf_returnDate.inputView = datePicker_return
        tf_returnDate.inputAccessoryView = toolBar_return
    }
    
    // Done btn clicked
    func tappedToolBarBtn(_ sender: UIBarButtonItem) {
        switch sender.tag { // hide the keyboard
        case 0 : tf_depatureDate.resignFirstResponder()
        default: tf_returnDate.resignFirstResponder()
        }
    }
    
    // Today btn clicked
    func tappedToolBarBtnToday(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 0 : datePicker_depart.date = Date()
        default: datePicker_return.date = Date()
        }
        changeLabelDate(date: Date(), sender.tag)
    }
    
    // Clear btn clicked
    func clickedToolBarBtnClear(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 0 : searchInfo.departDate = nil
        default: searchInfo.returnDate = nil
        }
    }
    
    // change datePicker
    func changedDateEvent(_ sender: AnyObject?) {
        if let tag: Int = sender?.tag {
            switch tag {
            case 0 :
                self.changeLabelDate(date: datePicker_depart.date, tag)

                func changeReturnDate() {
                    // add 7days since departDate
                    let dateIn7DaysFromDepartDate: Date = Date(timeInterval: 60*60*24*7, since: datePicker_depart.date)
                    self.changeLabelDate(date: dateIn7DaysFromDepartDate, 1)
                }

                // if returnDate is empty or the past from departDate, change returnDate automatically
                if tf_returnDate.text!.isEmpty {
                    changeReturnDate()

                } else if let returnDate = searchInfo.returnDate {
                    if datePicker_depart.date > returnDate {
                        changeReturnDate()
                    }
                }

            default: self.changeLabelDate(date: datePicker_return.date, tag)
            }
        }
    }
    
    func changeLabelDate(date: Date?, _ tag: Int) {
        switch tag {
        case 0 : searchInfo.departDate = date
        default: searchInfo.returnDate = date
        }
    }
}


//// ----------------------------------------------------------
//// delegate method of pickerViews of passeger & cabin calss
//// ----------------------------------------------------------
//
//extension FilterCell: UIPickerViewDelegate, UIPickerViewDataSource {
//    
//    func setupPickerView() { // called by didLoad method
//        
//        let allPickerViews: [UIPickerView] = [pic_passenger, pic_class]
//        
//        for pickerView in allPickerViews {
//            pickerView.dataSource = self
//            pickerView.delegate   = self
//
//            for subview in pickerView.subviews {
//                if subview.frame.origin.y != 0 {
//                    subview.isHidden = true
//                }
//            }
//        }
//    }
//    
//    // pickerView compornent
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        if pickerView == pic_class {
//            return 1
//        } else {
//            return 3
//        }
//    }
//    
//    // pickerView row
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        if pickerView == pic_class {
//            return data_classPicker.count
//        } else {
//            return data_passengerPicker.count
//        }
//    }
//    
//    // pickerView data
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if pickerView == pic_class {
//            return data_classPicker[row]
//        } else {
//            return data_passengerPicker[row]
//        }
//    }
//    
//    // did select
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        print(row)
//    }
//
//    // view of each row
//    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
//
//        let label: UILabel = UILabel(fontSize: 16, .white, textAlign: .center)
//        let v: UIView = UIView()
//            v.addSubview(label)
//        label.layout(centerY: v.centerYAnchor)
//
//        if pickerView == pic_class { // cabin class pickerView
//            label.text = data_classPicker[row]
//            label.layout(centerX: v.centerXAnchor)
//
//        } else { // passenger pickerView
//            label.text = data_passengerPicker[row]
//            label.layout(centerX: v.centerXAnchor, centerXConstant: 10)
//        }
//        return v
//    }
//    
//    // height of row
//    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
//        return 28
//    }
//}


// ----------------------------------------------------------
// delegate method of textfield of departDate & returnDate
// ----------------------------------------------------------

extension FilterCell: UITextFieldDelegate {
    
    func setDelegateTextField() {
        self.tf_depatureDate.delegate = self
        self.tf_returnDate.delegate   = self
    }
    
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //
    //        // Closing keyboard when user clicked outside of keyboard
    //        self.contentView.endEditing(true)
    //    }
}
