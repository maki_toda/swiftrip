//
//  VoiceSearchCell.swift
//  Swiftrip
//
//  Created by maki on 2017-05-30.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class VoiceSearchCell: BaseCollectionViewCell {

    let margin: CGFloat = 8
    let profileImageSize: CGFloat = 30

    var message: Message? {

        didSet {

            guard let message = message else { return }

            messageTextView.text = message.text as String!
            
            if let profileImage = message.sender.profileImage {
                profileImageView.image = profileImage
            }

            // message size
            let screenWidth: CGFloat = DeviceSize.screenWidth()
            let estimatedFrame: CGRect = estimateFrame(string: message.text)

            // message layout
            var messageTextView_x : CGFloat = 0
            var profileImageView_x: CGFloat?

            switch message.sender.senderType  {
            case .AI:
                profileImageView_x = margin
                messageTextView_x = profileImageSize + (margin * 3)
                textBubbleView.backgroundColor = .white

            case .user:
                if message.sender.profileImage != nil {
                    profileImageView_x = screenWidth - profileImageSize - margin
                    messageTextView_x  = screenWidth - estimatedFrame.width - profileImageSize - (margin * 4)
                } else {
                    messageTextView_x  = screenWidth - estimatedFrame.width - (margin * 3)
                }
                
                messageTextView.textColor = .white
                if message.text.isEqual(to: "…") {
                    textBubbleView.backgroundColor = UIColor(r: 0, g: 150, b: 210, a: 0.3)
                } else {
                    textBubbleView.backgroundColor = .sub
                }
            }

            if let _profileImageView_x: CGFloat = profileImageView_x {
                profileImageView.frame = CGRect(x: _profileImageView_x, y: estimatedFrame.height - 5, width: profileImageSize, height: profileImageSize)
            }
            messageTextView.frame = CGRect(x: messageTextView_x, y: 4, width: estimatedFrame.width + 16, height: estimatedFrame.height + 20)
        }
    }

    var messageTextView: MessageTextView = MessageTextView()
    
    let textBubbleView: UIView = {
        let o = UIView()
            o.layer.cornerRadius = 15
            o.layer.masksToBounds = true
            o.backgroundColor = .white
            o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()

    let profileImageView: UIImageView = {
        let o = UIImageView()
            o.contentMode = .scaleAspectFill
            o.layer.cornerRadius = 15
            o.layer.masksToBounds = true
        return o
    }()

    override func setupView() {
        super.setupView()
        
        contentView.addSubview(profileImageView)
        contentView.addSubview(textBubbleView)
        contentView.addSubview(messageTextView)
        
        textBubbleView.leftAnchor.constraint(equalTo: messageTextView.leftAnchor, constant: -(margin)).isActive = true
        textBubbleView.rightAnchor.constraint(equalTo: messageTextView.rightAnchor).isActive = true
        textBubbleView.topAnchor.constraint(equalTo: messageTextView.topAnchor, constant: -4).isActive = true
        textBubbleView.bottomAnchor.constraint(equalTo: messageTextView.bottomAnchor).isActive = true
    }

    func estimateFrame(string: NSString) -> CGRect {

        let size: CGSize = CGSize(width: DeviceSize.screenWidth() - (profileImageSize * 2) - (margin * 4), height: 1000)
        let options: NSStringDrawingOptions = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame: CGRect = string.boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont(name: fontName, size: 14)!], context: nil)
        return estimatedFrame
    }
}

class MessageTextView: UITextView {

    override var text: String! {
        didSet {
            if oldValue == "…" {
                self.textColor = UIColor.black
            }
        }
    }

    convenience init() {
        self.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.font = UIFont(name: fontName, size: 14)
        self.backgroundColor = UIColor.clear
        self.isUserInteractionEnabled = false
    }
}
