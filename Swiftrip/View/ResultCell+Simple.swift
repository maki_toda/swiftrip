//
//  ResultCell+Simple.swift
//  Swiftrip
//
//  Created by maki on 2017-05-23.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit
import SwiftyJSON

extension ResultCell {
    
    func initTicketForNoDestination(_ result: JSON, hasInbound: Bool) -> Ticket {
        
        /*
         
         Example result
         
         {
         "airline" : "VY",
         "destination" : "BCN",
         "departure_date" : "2017-05-03",
         "price" : "63.90",
         "return_date" : "2017-05-04"
         }
         
         */
        
        return Ticket(apiName: API_NAME.inspirationSearch,
                      itineraries: initItineraryForNoDestination(result, hasInbound: hasInbound),
                      deep_link: createDeeplink(result),
                      merchant: nil,
                      travel_class: nil,
                      fare_family: nil,
                      cabin_code: nil,
                      fare: Fare(price_per_adult: nil,
                                 price_per_child: nil,
                                 price_per_infant: nil,
                                 total_price: result["price"].numberValue,
                                 service_fees: nil,
                                 creditcard_fees: nil),
                      airline: result["airline"].string,
                      img: nil)
    }
    
    private func createDeeplink(_ result: JSON) -> String {
        var param : [String] = []
        param.append("marker=\(Setting.travelPayouts_marker)")
        if let origin_iata: String = searchInfo.origin?.airportCode {
            param.append("origin_iata=\(origin_iata)")
        }
        param.append("destination_iata=\(result["destination"].string!)")
        param.append("depart_date=\(result["departure_date"].string!)")
        if let return_date: String = result["return_date"].string { // ex. 2016-12-15
            param.append("return_date=\(return_date)")
        }
        if searchInfo.oneWay {
            param.append("oneway=1")
        } else {
            param.append("oneway=0")
        }
        if let adults: Int = searchInfo.adult {
            param.append("adults=\(adults)")
        }
        if let children: Int = searchInfo.child {
            param.append("children=\(children)")
        }
        if let infants: Int = searchInfo.infant {
            param.append("infants=\(infants)")
        }
        if let cabin_class: Int = searchInfo.cabin_class {
            param.append("trip_class=\(cabin_class)") // Economy: 0, Business: 1, First: 2
        }
        // Initiate search automatically (true: the search is loaded, false: form is filled out but search does not start)
        param.append("with_request=false")
        let url: String = "http://jetradar.com/searches/new?\(param.joined(separator: "&"))"
        return url
    }
    
    private func initItineraryForNoDestination(_ result: JSON, hasInbound: Bool) -> [Itinerary] {
        let flights : [Flight] = initFlightsForNoDestination(result, hasInbound: hasInbound)
        let outbound: Bound    = Bound(flights: [flights[0]], duration: nil)
        var inbound : Bound?   = nil
        if hasInbound {
            inbound = Bound(flights: [flights[1]], duration: nil)
        }
        return [Itinerary(outbound: outbound, inbound: inbound)]
    }
    
    private func initFlightsForNoDestination(_ result: JSON, hasInbound: Bool) -> [Flight] {
        var departure_date: String
        var origin        : Airport
        var destination   : Airport
        var flights: [Flight] = [Flight]()

        let airportOutbound: Airport = Airport(airportCode: searchInfo.origin!.airportCode,
                                               airportName: searchInfo.origin?.airportName,
                                               lat: searchInfo.origin?.location?.coordinate.latitude,
                                               lon: searchInfo.origin?.location?.coordinate.longitude,
                                               placeName: searchInfo.origin?.placeName,
                                               countryCode: searchInfo.origin?.countryCode,
                                               cityCode: searchInfo.origin?.cityCode,
                                               timeZone: searchInfo.origin?.timeZone,
                                               terminal: searchInfo.origin?.terminal,
                                               photoURL: searchInfo.origin?.photoURL)

        let airportInbound: Airport = Airport(airportCode: result["destination"].stringValue,
                                              airportName: nil,
                                              lat: nil,
                                              lon: nil,
                                              placeName: nil,
                                              countryCode: nil,
                                              cityCode: nil,
                                              timeZone: nil,
                                              terminal: nil,
                                              photoURL: nil)

        for index: Int in 0...1 {

            // flights[0] means departure flight
            if index == 0 {
                departure_date = result["departure_date"].stringValue
                origin         = airportOutbound
                destination    = airportInbound
                
            // flights[1] means return flight
            } else {
                if hasInbound {
                    departure_date = result["return_date"].stringValue
                    origin         = airportInbound
                    destination    = airportOutbound
                } else { continue }
            }
            
            flights.append(Flight(origin: origin,
                                  destination: destination,
                                  departs_at: departure_date.convertToDate(dateFormat: .YYYY_MM_DD),
                                  arrives_at: nil,
                                  booking_info: nil,
                                  operating_airline: result["airline"].string,
                                  marketing_airline: result["airline"].string,
                                  aircraft: nil,
                                  flight_number: nil))
        }
        return flights
    }
}
