//
//  BaseCell.swift
//  Swiftrip
//
//  Created by maki on 2017-05-20.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    func setupView() {
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BaseTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    func setupView() {
        self.selectionStyle = .none // to avoid changing color when cell was selected

        self.textLabel?.font = UIFont(name: fontName, size: 16)
        self.textLabel?.textColor = UIColor.darkGray
        self.detailTextLabel?.font = UIFont(name: fontName, size: 12)
        self.detailTextLabel?.textColor = UIColor.darkGray
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
