//
//  TicketCellHeader.swift
//  Swiftrip
//
//  Created by maki on 2017-04-15.
//  Copyright © 2017 maki. All rights reserved.
//

import UIKit

class TicketCellHeader: BaseCollectionViewCell {
    
    override func setupView() {
        self.backgroundColor = UIColor.clear
        self.layer.addSublayer(shadow)
//        self.layer.addSublayer(gradient)
        addSubview(imgView)
        addSubview(lab_city)
        
//        gradient.bounds = CGRect(x: 0.0, y: 0.0, width: self.frame.width - 32, height: 180)
//        gradient.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        
        shadow.bounds = CGRect(x: 0.0, y: 0.0, width: self.frame.width - 32, height: 180)
        shadow.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        shadow.zPosition = CGFloat(-1)
        
        // layout to back layer
        sendSubview(toBack: imgView)
        
        let items = ["lab_city": lab_city,
                     "imgView": imgView]
        
        items.forEach { $1.translatesAutoresizingMaskIntoConstraints = false }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-32-[lab_city]-32-|", options: .alignAllTop, metrics: nil, views: items))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[imgView]-16-|", options: .alignAllTop, metrics: nil, views: items))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lab_city]-16-|", options: .alignAllRight, metrics: nil, views: items))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[imgView]|", options: .alignAllRight, metrics: nil, views: items))
    }
    
    let lab_city: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont(name: label.font.fontName, size: 16)
        return label
    }()
    
    let imgView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.backgroundColor = UIColor.lightGray
        return image
    }()
    
    let gradient: CAGradientLayer = {
        let gradientTop    = UIColor(r: 255, g: 255, b: 255, a: 0)
        let gradientBottom = UIColor(r: 255, g: 255, b: 255, a: 1.0)
        let gradientColors: [CGColor] = [gradientTop.cgColor, gradientBottom.cgColor]
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientColors
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.8)
        return gradientLayer
    }()
    
    let shadow: CALayer = {
        let layer = CALayer()
        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        layer.shadowColor = UIColor(r: 186, g: 200, b: 224, a: 1).cgColor
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
        return layer
    }()
}
